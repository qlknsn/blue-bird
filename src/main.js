import Vue from "vue";
import App from "./App.vue";
import upperFirst from "lodash/upperFirst";
import camelCase from "lodash/camelCase";
import "video.js/dist/video-js.css";
import VueWorker from "vue-worker";
import {
  Carousel,
  CarouselItem,
  Timeline,
  TimelineItem,
  Icon,
  Progress,
  Steps,
  Step,
  Link,
  Tag,
  Loading
} from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
import ElementUI from "element-ui";
import router from "./router";
import store from "./store";
import "./registerServiceWorker";
import scroll from "vue-seamless-scroll";
import { VueAxios } from "./utils/requests";
import "./core/use";
import VueAwesomeSwiper from 'vue-awesome-swiper'
Vue.use(VueAwesomeSwiper)
import 'magic.css/dist/magic.min.css'
// import 'swiper/dist/css/swiper.css'
import global from './global'
Vue.prototype.GLOBAL = global


import md5 from 'js-md5';
Vue.prototype.$md5 = md5;

Vue.use(VueWorker);

Vue.use(VueAxios);
Vue.use(Progress); //进度条
Vue.use(Carousel); //轮播图
Vue.use(CarouselItem);
Vue.use(Timeline);
Vue.use(TimelineItem);
Vue.use(Icon);
Vue.use(Steps);
Vue.use(Step);

Vue.use(ElementUI);

Vue.use(Link);
Vue.use(Tag);
Vue.use(Loading);
Vue.use(scroll);

Vue.prototype.paginationTool = function(count, total) {
  if (total % count === 0) {
    return {
      page: total / count
    };
  }
};
Vue.prototype.diffObj = function(obj1, obj2) {
  var o1 = obj1 instanceof Object;
  var o2 = obj2 instanceof Object;
  if (!o1 || !o2) {
    /*  判断不是对象  */
    return obj1 === obj2;
  }

  if (Object.keys(obj1).length !== Object.keys(obj2).length) {
    return false;
  }

  for (var attr in obj1) {
    var t1 = obj1[attr] instanceof Object;
    var t2 = obj2[attr] instanceof Object;
    if (t1 && t2) {
      return Vue.prototype.diffObj(obj1[attr], obj2[attr]);
    } else if (obj1[attr] !== obj2[attr]) {
      return false;
    }
  }
  return true;
};

import $ from "jquery";
Vue.prototype.$ = $;

// Vue.prototype.DIALOG=true
// window.onload=function(){
//    Vue.prototype.DIALOG=false
// }
const requireComponent = require.context(
  "./components",
  true,
  /base[A-Z]\w+\.(vue|js)$/
);
requireComponent.keys().forEach(fileName => {
  const componentConfig = requireComponent(fileName);
  const componentName = upperFirst(
    camelCase(
      // 获取和目录深度无关的文件名
      fileName
        .split("/")
        .pop()
        .replace(/\.\w+$/, "")
    )
  );
  Vue.component(componentName, componentConfig.default || componentConfig);
});
Vue.config.productionTip = false;
// Vue.config.errorHander=(err,vm,info)=>{
//   console.log(err)
//   console.log(vm)
//   console.log(info)
// }
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");

import SockJS from "sockjs-client";
import Stomp from "stompjs";
const baseUrl = "10.2.0.11";
//socket连接地址
const base = "http://" + baseUrl + ":8092/ws/sockjs";
// const base = SOCKET_ADDRESS;

// const base2 = "ws://"+location.host+":"+location.port+"/ws/sockjs";

export default (name, back, arg) => {
  let use = base;
  let socket = new SockJS(use);
  let stompClient = Stomp.over(socket);
  stompClient.debug = function(str) {
    str;
  };
  let promise = new Promise((resolve, reject) => {
    let connect = () => {
      return resolve(stompClient);
    };
    let error = () => {
      stompClient.disconnect();
      back(arg);
      reject(name + "连接时发生了错误!");
    };
    stompClient.connect({}, connect, error);
  });
  return promise;
};

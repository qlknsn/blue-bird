const lxzRouter = [
  {
    path: "/liexiongzuo/streams",
    name: "liexiongzuoStreams",
    meta: {
      title: "猎熊座城市运行综合管理智慧平台"
    },
    component: () => import("@/components/streams.vue")
  },
  {
    path: "/liexiongzuo/turning/:screen",
    name: "liexiongzuoTurningScreen",
    meta: {
      title: "猎熊座城市运行综合管理智慧平台"
    },
    component: () => import("@/components/turningScreenLayout.vue")
  },
  {
    path: "/liexiongzuo/screen/:screen",
    name: "lxzCommonScreen",
    meta: {
      title: "猎熊座城市运行综合管理智慧平台"
    },
    component: () => import("@/components/commonScreenLayout.vue")
  },
  {
    path: "/liexiongzuo/video",
    name: "lxzVideo",
    component: () => import("@/videoTest.vue")
  },
  {
    path: "/liexiongzuo/login",
    name: "lxzLogin",
    meta: {
      title: "猎熊座城市运行综合管理智慧平台"
    },
    component: () => import("@/lxzLogin.vue")
  },
  {
    path: "/liexiongzuo/dashboard",
    name: "lxzDashboard",
    meta: {
      title: "猎熊座城市运行综合管理智慧平台"
    },
    component: () => import("@/views/lxz/dashboard.vue")
  },
  {
    path: "/liexiongzuo/videoLeft/:screen",
    name: "videoLeft",
    meta: {
      title: "左侧大屏"
    },
    // component: () => import('@/components/lxz/screen/testScreenLeft.vue')
    component: () => import("@/components/lxz/screen/screenLeft.vue")
  },
  {
    path: "/liexiongzuo/videoRight",
    name: "videoRight",
    meta: {
      title: "右侧大屏"
    },
    component: () => import("@/components/lxz/screen/screenRight.vue")
  }
];

export default lxzRouter;

const zhoujiaduRouter = [
  {
    path: "/zhoujiadu/turning/:screen",
    name: "zhoujiaduTurningScreen",
    meta: {
      title: "周家渡城市运行综合管理智慧平台"
    },
    component: () => import("@/components/turningScreenLayout.vue")
  },
  {
    path: "/zhoujiadu/screen/:screen",
    name: "zhoujiaduCommonScreen",
    meta: {
      title: "周家渡城市运行综合管理智慧平台"
    },
    component: () => import("@/components/commonScreenLayout.vue")
  },
  {
    path: "/zhoujiadu/login",
    name: "zhoujiaduLogin",
    meta: {
      title: "周家渡城市运行综合管理智慧平台"
    },
    component: () => import("@/zhoujiaduLogin.vue")
  },
  {
    path: "/zhoujiadu/dashboard",
    name: "zhoujiaduDashboard",
    meta: {
      title: "周家渡城市运行综合管理智慧平台"
    },
    component: () => import("@/views/zhoujiadu/dashboard.vue")
  },
  {
    path: "/zhoujiadu/videoLeft/:screen",
    name: "zhoujiaduLeftScreen",
    meta: {
      title: "轮询大屏"
    },
    component: () => import("@/components/zhoujiadu/screen/screenLeft.vue")
  }
];

export default zhoujiaduRouter;

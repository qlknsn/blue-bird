const sanlinOldStreet = [
  {
    path: "/sanlin/origin/login",
    name: "sanlinOldStreetLogin",
    meta: {
      title: "三林老街智能化管理平台"
    },
    component: () => import("@/views/sanlinOldStreet/login.vue")
  },
  {
    path: "/sanlin/origin/street",
    name: "sanlinOldStreet",
    meta: {
      title: "三林老街智能化管理平台"
    },
    component: () => import("@/views/sanlinOldStreet/oldStreetDashboard.vue")
  }
];

export default sanlinOldStreet;

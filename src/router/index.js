import Vue from "vue";
import axios from "axios";
import VueRouter from "vue-router";
import zhoujiaduRouter from "./zhoujiaduRouter";
import sanlinRouter from "./sanlinRouter";
import lxzRouter from "./lxzRouter";
import zhuqiaoRouter from "./zhuqiaoRouter";
import lingangRouter from "./lingangRouter";
import zhoupuRouter from "./zhoupuRouter";
import videoAggregationRouter from "./videoaggregationRouter";
import MaterialInventoryRouter from "./MaterialInventoryRouter";
import anZhiRouter from "./anZhiRouter";
import sanlinOldStreet from "./sanlinOldStreet";
Vue.use(VueRouter);

const routes = [
  ...zhoujiaduRouter,
  ...sanlinRouter,
  ...lxzRouter,
  ...zhuqiaoRouter,
  ...lingangRouter,
  ...videoAggregationRouter,
  ...zhoupuRouter,
  ...MaterialInventoryRouter,
  ...anZhiRouter,
  ...sanlinOldStreet
];

const router = new VueRouter({
  mode: "history",
  routes: [...routes]
});
router.beforeEach((to, from, next) => {
  window.document.title = to.meta.title || "";
  next();
  // if(to.path =='/lingang/bigscreen'&& !to.query.token){
  //   // console.log(111)
  //   axios.get('/json/videoAggregation.json').then(res=>{
  //     next(`/lingang/bigscreen?token=${res.data.token}`)
  //   })
  // }else{
  //   next();
  // }
});
export default router;

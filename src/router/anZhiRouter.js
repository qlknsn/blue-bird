const anZhiRouter = [
  {
    path: "/a/p",
    name: "anzhiPolling",
    meta: { title: "安智轮巡测试" },
    component: () => import("@/views/anzhi/polling.vue")
  }
];

export default anZhiRouter;

const lxzRouter = [
  {
    path: "/zhoupuanzhi/left",
    name: "zhoupuanzhiLeftScreen",
    meta: {
      title: "周浦镇运行综合管理智慧平台"
    },
    component: () => import("@/components/zhoupuanzhiLeftScreen.vue")
  },
  {
    path: "/zhoupuanzhi/right",
    name: "zhoupuanzhiRightScreen",
    meta: {
      title: "周浦镇运行综合管理智慧平台"
    },
    component: () => import("@/components/zhoupuanzhiRightScreen.vue")
  },
  {
    path: "/zhoupu/left",
    name: "zhoupuLeftScreen",
    meta: {
      title: "周浦镇运行综合管理智慧平台"
    },
    component: () => import("@/components/zhoupuLeftScreen.vue")
  },
  {
    path: "/zhoupu/right",
    name: "zhoupuRightScreen",
    meta: {
      title: "周浦镇运行综合管理智慧平台"
    },
    component: () => import("@/components/zhoupuRightScreen.vue")
  },
  {
    path: "/zhoupu/markevideo",
    name: "zhoupumarkevideo",
    meta: {
      title: "周浦镇视频标记工具"
    },
    component: () => import("@/components/videoMark.vue")
  },
];

export default lxzRouter;

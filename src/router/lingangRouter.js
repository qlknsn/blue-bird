const lingangRouter = [
  {
    path: "/lingang/livecast",
    name: "lingangLiveCast",
    meta: { title: "临港直播" },
    component: () => import("@/views/lingang/liveCasting.vue")
  },
  {
    path: "/lingang/bigscreen",
    name: "lingangBigScreen",
    meta: { title: "临港新片区社区综合管理平台" },
    component: () => import("@/views/lingang/bigScreen.vue")
  },
  {
    path: "/lingang/login",
    name: "lingangLogin",
    meta: { title: "临港新片区社区综合管理平台" },
    component: () => import("@/views/lingang/login.vue")
  },
];

export default lingangRouter;

const sanlinRouter = [
  {
    path: "/sanlin/screen/:screen",
    name: "sanlinCommonScreen",
    meta: {
      title: "三林镇城市运行综合管理智慧平台"
    },
    component: () => import("@/components/commonScreenLayout.vue")
  },
  {
    path: "/sanlin/turning/:screen",
    name: "sanlinTurningScreen",
    meta: {
      title: "三林镇城市运行综合管理智慧平台"
    },
    component: () => import("@/components/turningScreenLayout.vue")
  },
  {
    path: "/sanlin/fake/turning/:screen",
    name: "sanlinFakeTurningScreen",
    meta: {
      title: "三林镇城市运行综合管理智慧平台"
    },
    component: () => import("@/components/turningFakeScreenLayout.vue")
  },
  {
    path: "/sanlin/login",
    name: "sanlinLogin",
    meta: {
      title: "三林镇城市运行综合管理智慧平台"
    },
    component: () => import("@/sanlinLogin.vue")
  },
  {
    path: "/sanlin/dashboard",
    name: "sanlinDashboard",
    meta: {
      title: "三林镇城市运行综合管理智慧平台"
    },
    component: () => import("@/views/sanlin/dashboard.vue")
  },
  {
    path: "/sanlin/dashboard/v3",
    name: "sanlinDashboardV3",
    meta: {
      title: "三林镇城市运行综合管理智慧平台"
    },
    component: () => import("@/views/sanlin/dashboardv3.vue")
  },
  {
    path: "/sanlin/dashboard/v2",
    name: "sanlinDashboardV2",
    meta: {
      title: "三林镇城市运行综合管理智慧平台"
    },
    component: () => import("@/views/sanlin/dashboardv2.vue")
  },
  {
    path: "/sanlin/videoLeft/:screen",
    name: "sanlinLeftScreen",
    meta: {
      title: "左侧大屏"
    },
    component: () => import("@/components/sanlin/screen/screenLeft.vue")
  },
  {
    path: "/sanlin/videoRight",
    name: "sanlinRightScreen",
    meta: {
      title: "右侧大屏"
    },
    component: () => import("@/components/sanlin/screen/screenRight.vue")
  }
];

export default sanlinRouter;

const liqingchangRouter = [
  {
    path: "/lqc/kucun",
    name: "MaterialInventoryRouter",
    meta: { title: "材料库存" },
    component: () => import("@/views/liqingchang/MaterialInventory.vue")
  },
  {
    path: "/lqc/shengchan",
    name: "production",
    meta: { title: "生产情况" },
    component: () => import("@/views/liqingchang/production.vue")
  },
  {
    path: "/lqc/index",
    name: "liqingchangIndex",
    meta: { title: "沥青厂大屏" },
    component: () => import("@/views/liqingchang/liqingAll.vue")
  },
  {
    path: "/lqc/indexV2",
    name: "liqingchangTV2",
    meta: { title: "沥青厂大屏" },
    component: () => import("@/views/liqingchang/tv/liqingIndexV2.vue")
  },
  {
    path: "/lqc/pc/save",
    name: "lqcPcSave",
    meta: { title: "沥青厂大屏库存情况" },
    component: () => import("@/views/liqingchang/pc/save.vue")
  },
  {
    path: "/lqc/pc/product",
    name: "lqcPcProduction",
    meta: { title: "沥青厂大屏生产情况" },
    component: () => import("@/views/liqingchang/pc/prodution.vue")
  }
];

export default liqingchangRouter;

const sanlinRouter = [
  {
    path: "/zhuqiao/login",
    name: "zhuqiaoLogin",
    meta: {
      title: "祝桥镇城市运行综合管理智慧平台"
    },
    component: () => import("@/zhuqiaoLogin.vue")
  },
  {
    path: "/zhuqiao/turning/:screen",
    name: "zhuqiaoTurningScreen",
    meta: {
      title: "祝桥镇城市运行综合管理智慧平台"
    },
    component: () => import("@/components/turningScreenLayout.vue")
  },
  {
    path: "/zhuqiao/screen/:screen",
    name: "zhuqiaoCommonScreen",
    meta: {
      title: "祝桥轮询屏幕"
    },
    component: () => import("@/components/commonScreenLayout.vue")
  },
  {
    path: "/zhuqiao/dashboard",
    name: "zhuqiaoDashboard",
    meta: {
      title: "祝桥镇城市运行综合管理智慧平台"
    },
    component: () => import("@/views/zhuqiao/dashboard.vue"),
    children: [
      {
        path: "/zhuqiao/dashboard",
        name: "首页",
        component: () => import("@/components/zhuqiao/component/index.vue")
      },
      {
        path: "/zhuqiao/dashboard/roadcams",
        name: "人群观测布控",
        component: () => import("@/components/zhuqiao/component/roadcams.vue")
      },
      {
        path: "/zhuqiao/dashboard/peopleCounting",
        name: "人群密度",
        component: () =>
          import("@/components/zhuqiao/component/people-counting.vue")
      },
      {
        path: "/zhuqiao/dashboard/12345",
        name: "12345市民热线",
        component: () => import("@/components/zhuqiao/component/12345.vue")
      },
      {
        path: "/zhuqiao/dashboard/parking",
        name: "车辆违停",
        component: () => import("@/components/zhuqiao/component/parking.vue")
      }
      // {path: '/electrombile', name: '消防警报', component: electrombile},
      // {path: '/bracelet', name: '手环车辆', component: bracelet},
    ]
  },
  {
    path: "/zhuqiao/videoLeft",
    name: "zhuqiaoLeftScreen",
    meta: {
      title: "左侧大屏"
    },
    component: () => import("@/components/zhuqiao/screen/screenLeft.vue")
  },
  {
    path: "/zhuqiao/videoRight",
    name: "zhuqiaoRightScreen",
    meta: {
      title: "右侧大屏"
    },
    component: () => import("@/components/zhuqiao/screen/screenRight.vue")
  }
];

export default sanlinRouter;

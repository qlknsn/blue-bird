const videoAggregationRouter = [
  {
    path: "/videoaggregation/dashboard",
    name: "pudongBigScreen",
    meta: {
      title: "浦东城市大脑--街镇视频汇聚系统"
    },
    component: () => import("@/views/videoAggregation/dashboard.vue")
  }
];

export default videoAggregationRouter;

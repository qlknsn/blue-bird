import api from "./mapUrl";
import { axios } from "@/utils/requests";
import { param } from "jquery";

export function getDeviceList(params) {
  return axios({
    url: api.deviceList,
    params: params
  });
}
export function connectRandomVideo(params) {
  return axios({
    method: "post",
    url: `${api.connectRandomVideo}?action=${params.action}&protocol=${params.protocol}&streamType=${params.streamType}`,
    data: params.cameraId
  });
}

export function newConnectVideo(params) {
  return axios({
    method: "post",
    url: api.newConnectVideo,
    data: params
  });
}
export function connectVideo(params) {
  return axios({
    method: "post",
    url: api.connectVideo,
    data: params
  });
}

export function disconnectVideo(params) {
  return axios({
    method: "post",
    url: api.disconnectVideo,
    data: params
  });
}
export function lockvideo(params) {
  return axios({
    method: "post",
    url: `${api.lockvideo}?action=LOCK_SCREEN`,
    data: params
  });
}
export function unlockvideo(params) {
  return axios({
    method: "post",
    url: `${api.lockvideo}?action=UNLOCK_SCREEN`,
    data: params
  });
}

export function streamBext() {
  return axios({
    url: api.streamBext
  });
}

export function getCurrentWSvideoList(data) {
  return axios({
    url: api.getCurrentWSvideoList,
    params:data
  });
}
export function getScreens(data) {
  return axios({
    url: api.getScreens,
    params: data
  });
}
export function readDevicesTags(data) {//标签列表
  return axios({
    url: api.readDevicesTags,
    params: data
  });
}

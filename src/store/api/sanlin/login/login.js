import api from './loginUrl'
import {
    axios
} from '@/utils/requests'
// import config from '@/utils/config.js'

export function loginUser(data) {
    return axios({
        url: api.loginUser,
        method: 'post',
        data: data,
    })
}
export function sanlinLogin(params) {  //获取不达标小区与单位
    return axios({
        method:'post',
        url:api.sanlinLogin,
        data:params
    })
}
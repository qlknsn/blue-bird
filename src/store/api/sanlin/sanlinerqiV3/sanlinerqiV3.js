import api from './sanlinerqiUrlV3'
import { axios } from '@/utils/requests'
export function getline(params) {   //三率折现图
    return axios({
        method: 'get',
        url: api.getline,
        params: params
    })
}
export function getPointCount(params) {
    return axios({
        method: 'get',
        url: api.getPointCount,
        params: params
    })
}
export function getWeizhangList(params) {
    return axios({
        method: 'get',
        url: api.getWeizhangList,
        params: params
    })
}
export function getWeizhangPic(params) {
    return axios({
        method: 'get',
        url: api.getWeizhangPic,
        params: params
    })
}
export function getWeizhangRecord(params) {
    return axios({
        method: 'get',
        url: api.getWeizhangRecord,
        params: params
    })
}
export function getLeftMiddleInfoV3(params) {
    return axios({
        url: api.getLeftMiddleInfoV3,
        params: params
    })
}
export function getRenqunMiduList(params) {
    return axios({
        url: api.getRenqunMiduList,
        params: params
    })
}
export function getDropList(params) {
    return axios({
        url: api.getDropList,
        params: params
    })
}

export function getgarbagePie(params){
    return axios({
        method: 'get',
        url: api.getgarbagePie,
        params: params
    })
}

export function getBar(params) {    ////满意率、先行联系率、实际解决率
    return axios({
        method: 'get',
        url: api.getBar,
        params: params
    })
}
export function getrecycledList(params) {    //可回收服务列表
    return axios({
        method: 'get',
        url: api.getrecycledList,
        params: params
    })
}
export function getrecycledCount(params) {    //可回收服务列表
    return axios({
        method: 'get',
        url: api.getrecycledCount,
        params: params
    })
}
export function getgarbageRank(params) {    //可回收服务列表
    return axios({
        method: 'get',
        url: api.getgarbageRank,
        params: params
    })
}

export function qushiTu(params){    //趋势图
    return axios({
        method:'get',
        url:api.qushiTu,
        params:params
    })
}

export function noiseList(params){    //噪音检测设备列表
    return axios({
        method:'get',
        url:api.noiseList,
        params:params
    })
}
export function garbageLPlateList(params){    //垃圾板块详情列表
    return axios({
        method:'get',
        url:api.garbageLPlateList,
        params:params
    })
}
export function garbagePlateCount(params){    //垃圾板块详情统计
    return axios({
        method:'get',
        url:api.garbagePlateCount,
        params:params
    })
}
export function garbageRecycledList(params){    //垃圾板块详情统计
    return axios({
        method:'get',
        url:api.garbageRecycledList,
    })
}

export function noiseRecordList(params){    //噪音记录数据列表
    return axios({
        method:'post',
        url:api.noiseRecordList,
        params:params
    })
}

export function nosieLine(params){    //噪音记录数据列表
    return axios({
        method:'get',
        url:api.nosieLine,
        params:params
    })
}

export function gongdanfenxiPieChart(params){    //工单分析板块饼图
    return axios({
        method:'get',
        url:api.gongdanfenxiPieChart,
        params:params
    })
}
export function garbageStatic(params){    //工单分析板块饼图
    return axios({
        method:'post',
        url:api.garbageStatic,
        data:params
    })
}

export function baseData(params){    //基础数据
    return axios({
        method:'get',
        url:api.baseData,
        params:params
    })
}
export function getPeopleLineData(params) {
    return axios({
        method:'get',
        url:api.getPeopleLineData,
        params:params
    })
}
export function getnNoStandart(params) {  //获取不达标小区与单位
    return axios({
        method:'get',
        url:api.getnNoStandart,
        params:params
    })
}



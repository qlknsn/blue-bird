import { constant } from "lodash"

const api ={
        getWeizhangList: '/sanlinerqi/v1/car/devices',
        getWeizhangPic: '/sanlinerqi/v1/car/getLastInfo',
        getWeizhangRecord: '/sanlinerqi/v1/car/records',
        getLeftMiddleInfoV3: '/sanlinerqi/v1/population/graph/list',
        getRenqunMiduList: '/sanlinerqi/v1/population/list',
        getDropList: '/sanlinerqi/v1/config/dropdown',
        getPointCount: '/sanlinerqi/v1/car/pointStatistics',
        getline:'/sanlinerqi/v1/case/chart/line',
        // 垃圾板块饼状图
        getgarbagePie:'/sanlinerqi/v1/garbage/plate/pie',
        getBar:'/sanlinerqi/v1/case/graph/bar',  //满意率、先行联系率、实际解决率
        getrecycledList:'/sanlinerqi/v1/garbage/recycled/list',  //可回收物服务点列表
        getrecycledCount:'/sanlinerqi/v1/garbage/recycled/list/count',  //可回收物服务点统计
        getgarbageRank:'/sanlinerqi/v1/garbage/rank',  //干湿垃圾排行
        // qushiTu:'/sanlinerqi/v1/case/graph/singleline'
        qushiTu:'/sanlinerqi/v1/case/graph/singleline',  //趋势图
        noiseList:'/sanlinerqi/v1/noise/device/list',    //噪音检测列表
        garbageLPlateList:'/sanlinerqi/v1/garbage/plate/list',    //垃圾版块详情列表
        garbagePlateCount:'/sanlinerqi/v1/garbage/plate/list/count',    //垃圾版块详情列表
        garbageRecycledList:'/sanlinerqi/v1/garbage/recycled/list',    //垃圾版块详情列表

        noiseRecordList:'/sanlinerqi/v1/noise/records',  //噪音记录数据列表
        nosieLine:'/sanlinerqi/v1/noise/graph/line',  //噪音曲线图
        garbageStatic:'/sanlinerqi/v1/static/config/list',     //工单分析板块饼图
        gongdanfenxiPieChart:'/sanlinerqi/v1/case/chart/pie',     //工单分析板块饼图
        baseData:'/sanlinerqi/v1/static/config/baseData',
        getPeopleLineData: '/sanlinerqi/v1/population/graph/line',
        getnNoStandart:"/sanlinerqi/v1/garbage/notToStandard/search",
        
}

export default api

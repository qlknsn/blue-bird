import api from './sanlinechartsUrl'
import {
    axios
} from '@/utils/requests'
import {isNegativeZero} from "flipclock/src/js/Helpers";
// import config from '@/utils/config.js'

export function graphPie(params) {
    return axios({
        url: api.graphPie,
        params: params,
    })
}
export function graphBar(params) {
    return axios({
        url: api.graphBar,
        params: params,
    })
}
export function garbagePie(params) {
    return axios({
        url: api.garbagePie,
        params: params,
    })
}
export function garbageLine(params) {
    // let param = {
    //     period:params.period,
    //     districtId:params.districtId
    // }
    return axios({
        url: `${api.garbageLine}`,
        params: params,
    })
}
export function districtsXq(params) {
    return axios({
        url: api.districtsXq,
        params: params,
    })
}
export function getLastInfo(params) {
    return axios({
        url: api.getLastInfo,
        // method:'post',
        params: params,
    })
}
export function getLeftTopInfo(params) {
    return axios({
        url: api.getLeftTopInfo,
        params: params
    })
}
export function getLeftMiddleInfo(params) {
    return axios({
        url: api.getLeftMiddleInfo,
        params: params
    })
}
export function getPassWordList(params) {
    return axios({
        url: api.getPassWordList,
        params: params
    })

}
export function getWeikakouList(params) {
    return axios({
        url: api.getWeikakouList,
        params: params
    })
}
export function getWeikakouPoints(params) {
    return axios({
        url: api.getWeikakouPoints,
        params: params
    })
}
export function configList(params) {
    return axios({
        url: api.configList,
    })
}
export function getWeikakouDetail(params) {
    return axios({
        url: api.getWeikakouDetail,
        params: params
    })
}
export function getZhuapaiPoints(params) {
    return axios({
        url: api.getZhuapaiPoints,
        params: params
    })
}
export function getZhuapaiDetail(params) {
    return axios({
        url: api.getZhuapaiDetail,
        params: params
    })
}
export function getZaoyinBar(params) {
    return axios({
        url: api.getZaoyinBar,
        params: params
    })
}
export function getDropList(params) {
    return axios({
        url: api.getDropList + '/' + params.type
    })
}
export function getRenqunMiduList(params) {
    return axios({
        url: api.getRenqunMiduList,
        params: params
    })
}
export function getImagesList(params) {
    return axios({
        url: api.getImagesList,
        params: params
    })
}
export function getVoicePointList(params) {
    return axios({
        url: api.getVoicePointList,
        params: params
    })
}
export function getVoiceList(params) {
    return axios({
        method:'post',
        url: api.getVoiceList,
        params: params
    })
}
export function getVoiceDetail(params) {
    return axios({
        url: api.getVoiceDetail,
        params: params
    })
}
export function videoPollingList(params) {
    return axios({
        url: api.videoPollingList,
        params: params
    })
}
export function runTaskNow(params) {
    return axios({
        method: "post",
        url: `${api.delete}/${params.pollingId}`,
        params: { action: "PLAY_NOW" },
        headers: {
            "Content-Type": "application/json"
        }
    });
}
export function getPeopleHotEchart(params) {
    return axios({
        url: api.getPeopleHotEchart,
        params: params
    })
}
export function graphLine(params) {
    return axios({
        url: api.graphLine,
        params: params
    })
}

export function garbageAvgList(params) {
    return axios({
        url: api.garbageAvgList,
        params:params
    })
}

// 获取平均清运量排行
export function getAvgRank(params) {
    return axios({
        url: api.getAvgRank,
        params: params
    })
}
// 获取清运量排行
export function getRank(params) {
    return axios({
        url: api.getRank,
        params: params
    })
}
// 派单柱状图
export function dispatchBar(params) {
    return axios({
        url: api.dispatchBar,
        params: params
    })
}
// 派单饼状图
export function dispatchPie(params) {
    return axios({
        url: api.dispatchPie,
        params: params
    })
}
// 获取垃圾分类的趋势图
export function getBarbageLine(params) {
  return axios({
    url: api.getBarbageLine,
    params: params
  })
}
export function getVoiceLiveUrl(params) {
    return axios({
        url: api.getLiveUrl,
        params: params
    })
}

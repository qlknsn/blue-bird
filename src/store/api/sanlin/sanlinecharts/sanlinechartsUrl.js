const api = {
    // 登录接口
    graphPie: '/seaweed/v1/case/graph/pie',//环状图
    graphBar: '/seaweed/v1/case/graph/bar',//条形图
    garbagePie:'/seaweed/v1/garbage/graph/pie',
    garbageLine:'/seaweed/v1/garbage/graph/line',//折线图
    districtsXq:'/seaweed/v1/garbage/districts',//获取小区列表
    getLastInfo:'/seaweed/v1/car/getLastInfo',//获取最新的违停图片
    getLeftTopInfo: '/micro-bayonet-statistics/v1/face/countInfo', // 微卡口模块统计信息接口
    getLeftMiddleInfo: '/seaweed/v1/population/graph/bar', // 实时人口密度柱状图
    getPassWordList: '/seaweed/v1/face/dynamicPassword', // 获取动态密码
    getWeikakouList: '/micro-bayonet-statistics/v1/face/warnling/list', // '获取抓拍列表
    getWeikakouPoints: '/micro-bayonet-statistics/v1/face/dept/list', // 微卡口撒点
    configList: '/seaweed/v1/config/list', // 配置管理
    getWeikakouDetail: '/micro-bayonet-statistics/v1/face/dept/detail', // 微卡口详情
    getZhuapaiPoints: '/micro-bayonet-statistics/v1/face/point/list', // 抓拍撒点
    getZhuapaiDetail: '/micro-bayonet-statistics/v1/face/point/detail', // 抓拍撒点详情
    getZaoyinBar: '/seaweed/v1/noise/graph/bar', // '噪音柱状图'
    getDropList: '/seaweed/v1/config/dropdown', // '下拉框列表'
    getRenqunMiduList: '/seaweed/v1/population/list', // 告警列表
    getImagesList: '/micro-bayonet-statistics/v1/face/dept/images', // 抓拍图片列表
    getVoicePointList: '/seaweed/v1/noise/device/list', // 设备列表 噪音检测的撒点
    getVoiceList: '/seaweed/v1/noise/list', // 设备列表 噪音检测的列表
    getVoiceDetail: '/seaweed/v1/noise/device/detail', // 设备详情
    videoPollingList: "/vis/v1/streaming/polling", // 一键轮询
    delete: "/vis/v1/streaming/polling",
    runTaskNow: "/vis/v1/streaming/polling/", // 一键切换
    getPeopleHotEchart: "/seaweed/v1/population/heatMap", // "人群密度热力图"
    graphLine: "/seaweed/v1/case/graph/singleline", // "趋势图"
    garbageAvgList:'/seaweed/v1/garbage/avgList',
    getAvgRank: "/seaweed/v1/garbage/graph/avgRank", // 平均清运量排行
    getRank: "/seaweed/v1/garbage/graph/rank", // 清运量排行
    dispatchBar:"/seaweed/v1/case/graph/dispatchBar",//派单柱状图
    dispatchPie:"/seaweed/v1/case/graph/dispatchPie",//派单饼状图
    getBarbageLine: "/seaweed/v1/garbage/graph/singleline", // 获取垃圾分类的趋势图
    getLiveUrl: "/seaweed/v1/noise/liveUrl" // 获取视频流
}
// seaweed
export default api

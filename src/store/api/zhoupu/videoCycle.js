import api from './videoCycleUrl'
import {
    axios
} from '@/utils/requests'
// import config from '@/utils/config.js'

export function closeStream(data) {
    return axios({
        url: api.closeStream,
        params: data,
    })
}
export function changeStream(data) {
    return axios({
        url: api.changeStream,
        params: data,
    })
}
export function fetchStream(data) {
    return axios({
        url: api.fetchStream,
        params: data,
    })
}
export function markStream(data) {
    return axios({
        url: api.markVideos,
        params: data,
    })
}
export function markVideos(data) {
    return axios({
        url: api.markVideos,
        data: data,
        method:'post'
    })
}

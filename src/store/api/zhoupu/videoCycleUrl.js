const api = {
    // 登录接口
    closeStream: "/videoLoopTask/closeStream",
    changeStream: "/videoLoopTask/changeStream",
    fetchStream: "/videoLoopTask/fetchStream",
    markStream: "/videoLoopTask/markStream",
    markVideos: "/markVideo/markVideo",
  };
  
  export default api;
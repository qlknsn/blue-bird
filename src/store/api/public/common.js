import api from "./commonUrl";
import { axios } from "@/utils/requests";

export function markCameraDamaged(data) {
  return axios({
    url: api.markCameraDamaged,
    method: "POST",
    data: data
  });
}

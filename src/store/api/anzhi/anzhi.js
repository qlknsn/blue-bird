import api from "./anzhiUrl";
import { axios } from "@/utils/requests";

export function changeVideo(data) {
  //标签列表
  return axios({
    url: api.changeVideo,
    params: data
  });
}

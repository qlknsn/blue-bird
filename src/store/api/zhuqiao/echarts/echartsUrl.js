const api = {
    // 获取echarts接口
    getFacePushCount:'/zq/api/suspect/faceRecognizeRecord/getFacePushCount',
    hourlyTotalPopulation:'/zq/api/wifi/hourlyTotalPopulation',
    getFaceCamScore:'/zq/api/suspect/faceRecognizeRecord/getFaceCamScore',
    getCaseSource:'/zq/api/governmentData/getCaseSource',
    getCountByYesterdayHour:'/zq/api/car/getCountByYesterdayHour',
    yesterdayXiuTanAllNum:'/zq/api/wifi/yesterdayTotalCount',

    phoneBrandStatistic:'/zq/api/wifi/timeBrand',
    yesterdayPeopleNumPerHour:'/zq/api/wifi/hourlyTotalPopulation',
    lastWeekXiuTanPeopleNum:'/zq/api/wifi/weeklyTotalPopulation',

    lastWeekXiuTanCarNum:'/zq/api/car/getCountByYesterdayHour',
    getPercentageByPrevious:'/zq/api/car/getPercentageByPrevious',
    getCountByLastSevenDays:'/zq/api/car/getCountByLastSevenDays',
    getPercentageByCarNumber:'/zq/api/car/getPercentageByCarNumber',
    getCountByCam:'/zq/api/car/getCountByCam',

    getCaseStatus:'/zq/api/governmentData/getCaseStatus',
    getCaseType:'/zq/api/governmentData/getCaseType',
    // getCaseSource:'/api/governmentData/getCaseStatus'

    getFaceDBRatio:'/zq/api/suspect/faceRecognizeRecord/getFaceDBRatio',
    getFaceCamAccuracy:'/zq/api/suspect/faceRecognizeRecord/getFaceCamAccuracy',
    getFaceCamPushRatio:'/zq/api/suspect/faceRecognizeRecord/getFaceCamPushRatio',
    getFaceCamWeeklyPush:'/zq/api/suspect/faceRecognizeRecord/getFaceCamWeeklyPush',
  };
  
  export default api;
import api from "./echartsUrl";
import { axios } from "@/utils/requests";

export function getFacePushCount(params) {
  return axios({
    url: api.getFacePushCount,
    params: params
  });
}
export function hourlyTotalPopulation(params) {
  return axios({
    url: api.hourlyTotalPopulation,
    params: params
  });
}
export function getFaceCamScore(params) {
  return axios({
    url: api.getFaceCamScore,
    params: params
  });
}
export function getCaseSource(params) {
  return axios({
    url: api.getCaseSource,
    params: params
  });
}
export function getCountByYesterdayHour(params) {
  return axios({
    url: api.getCountByYesterdayHour,
    params: params
  });
}
export function yesterdayXiuTanAllNum(params) {
  return axios({
    url: api.yesterdayXiuTanAllNum,
    params: params
  });
}
export function phoneBrandStatistic(params) {
  return axios({
    url: api.phoneBrandStatistic,
    params: params
  });
}
export function yesterdayPeopleNumPerHour(params) {
  return axios({
    url: api.yesterdayPeopleNumPerHour,
    params: params
  });
}
export function lastWeekXiuTanPeopleNum(params) {
  return axios({
    url: api.lastWeekXiuTanPeopleNum,
    params: params
  });
}
export function lastWeekXiuTanCarNum(params) {
  return axios({
    url: api.lastWeekXiuTanCarNum,
    params: params
  });
}
export function getPercentageByPrevious(params) {
  return axios({
    url: api.getPercentageByPrevious,
    params: params
  });
}
export function getCountByLastSevenDays(params) {
  return axios({
    url: api.getCountByLastSevenDays,
    params: params
  });
}
export function getPercentageByCarNumber(params) {
  return axios({
    url: api.getPercentageByCarNumber,
    params: params
  });
}
export function getCountByCam(params) {
  return axios({
    url: api.getCountByCam,
    params: params
  });
}
export function getCaseStatus(params) {
  return axios({
    url: api.getCaseStatus,
    params: params
  });
}
export function getCaseType(params) {
  return axios({
    url: api.getCaseType,
    params: params
  });
}
export function getFaceDBRatio(params) {
  return axios({
    url: api.getFaceDBRatio,
    params: params
  });
}
export function getFaceCamAccuracy(params) {
  return axios({
    url: api.getFaceCamAccuracy,
    params: params
  });
}
export function getFaceCamPushRatio(params) {
  return axios({
    url: api.getFaceCamPushRatio,
    params: params
  });
}
export function getFaceCamWeeklyPush(params) {
  return axios({
    url: api.getFaceCamWeeklyPush,
    params: params
  });
}
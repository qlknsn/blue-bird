import api from './loginUrl'
import {
    axios
} from '@/utils/requests'
// import config from '@/utils/config.js'

export function loginUsers(data) {
    return axios({
        url: api.loginUser,
        method: 'post',
        data: data,
    })
}

export function accountLogin(data) {
    return axios({
        url: api.accountLogin,
        method: 'post',
        data: data,
    })
}
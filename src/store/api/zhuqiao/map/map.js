
import api from "./mapUrl";
import { axios } from "@/utils/requests";



export function getDeviceList(params) {
  return axios({
    url: api.deviceList,
    params: params
  });
}

export function connectVideo(params) {
  return axios({
    method: "post",
    url: api.connectVideo,
    data: params
  });
}
export function disconnectVideo(params) {
  return axios({
    method: "post",
    url: api.disconnectVideo,
    data: params
  });
}
export function lockvideo(params) {
  return axios({
    method: "post",
    url: `${api.lockvideo}?action=LOCK_SCREEN`,
    data: params
  });
}
export function unlockvideo(params) {
  return axios({
    method: "post",
    url: `${api.lockvideo}?action=UNLOCK_SCREEN`,
    data: params
  });
}

export function streamBext() {
  return axios({
    url: api.streamBext
  });
}

export function getCurrentWSvideoList(data) {
  // console.log(data)
  return axios({
    url: api.getCurrentWSvideoList,
    params:data
  });
}

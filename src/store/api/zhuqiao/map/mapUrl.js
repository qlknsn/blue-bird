const api = {
  // 获取地图上的接口
  deviceList: "/vis/v1/devices", //大屏撒点以及区域搜索通用接口
  connectVideo: "/vis/v1/streaming/connect", //连接某个视频
  disconnectVideo: "/vis/v1/streaming/disconnect", //断开视频链接
  lockvideo: "/vis/v1/streaming/play", //断开视频链接
  streamBext: "/vis/v1/streaming/next", //视频轮询
  // streamPolling :'/vis/v1/streaming/polling',
  getCurrentWSvideoList: "/vis/v1/streaming/play" //读取当前轮屏的视屏url
};

export default api;


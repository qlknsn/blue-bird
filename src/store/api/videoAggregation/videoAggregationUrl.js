const api = {
    // 视频接入统计
    camerasSummary: "/viis/v1/report/cameras/summary",
    // 相机列表
    devicesCamera: "/viis/v1/devices/camera",
    // 调用方排名
    leadboardBySource:'/viis/v1/report/streaming/leadboard/bySource',
    // 实时被调用量
    realtimeByDistrict:'/viis/v1/report/streaming/summary',
    // 调用量分析
    searchByCamera:'/viis/v1/report/streaming/search/byCamera',
    // 调用量分析非全部
    leadboardByCamera:'/viis/v1/report/streaming/leadboard/byCamera',
    // 调用情况
    usageSummary:'/viis/v1/report/usage/summary',
    // 被调用场景分析
    searchByTag:'/viis/v1/report/streaming/search/byTag',
    // 获取授权信息
    getTitle: '/viis/v1/account/authorize',
    // 相机撒点接口
    cameraPoint: '/viis/v1/devices/camera/point',
    //可调用点位列表
    available:"/viis/v1/report/usage/summary/camera/available",
    //居村实时分析
    villageAnalyseList:'/viis/v1/report/streaming/summary/search/byPage'
  };
  
  export default api;
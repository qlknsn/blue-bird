import api from './videoAggregationUrl'
import {
    axios
} from '@/utils/requests'
import { param } from 'jquery'
// import config from '@/utils/config.js'

export function camerasSummary(params) {
    return axios({
        url: api.camerasSummary,
        params: params,
    })
}
export function devicesCamera(params) {
    return axios({
        url: api.devicesCamera,
        params: params,
    })
}
export function leadboardBySource(params) {
    return axios({
        url: api.leadboardBySource,
        params: params,
    })
}
export function searchByCamera(params) {
    return axios({
        method:'post',
        url: `${api.searchByCamera}?count=${params.count}&start=${params.start}&timeUnit=${params.timeUnit}`,
        data: params.urns,
    })
}
export function leadboardByCamera(params) {
    return axios({
        url: api.leadboardByCamera,
        params: params,
    })
}
export function realtimeByDistrict(params) {
    return axios({
        url: api.realtimeByDistrict,
        params: params,
    })
}
export function usageSummary(params) {
    return axios({
        url: api.usageSummary,
        params: params,
    })
}
export function searchByTag(params) {
    return axios({
        method:'post',
        url: `${api.searchByTag}?timeUnit=${params.timeUnit}`,
        data: params.urns,
    })
}
export function getAllTitle(params) {
    return axios({
        method:'post',
        url: api.getTitle,
        data: params,
    })
}
export function cameraPoint(params) {
    return axios({
        url: api.cameraPoint,
        params: params,
    })
}
export function available(params) {
    return axios({
        url: api.available,
        params: params,
        method:'get'
    })
}
export function villageAnalyseList(params) {
    return axios({
        url: api.villageAnalyseList,
        params: params,
        method:'get'
    })
}

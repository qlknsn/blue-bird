import api from "./basicInfoUrl";
import { axios } from "@/utils/requests";
import Vue from 'vue';
// import config from '@/utils/config.js'

export function commpanyList(data) {
  return axios({
    url: api.commpanyList,
    params: data
  });
}
export function videoList(data) {
  return axios({
    url: api.videoList,
    params: data
  });
}
export function basicDataPool(data) {
  // Object.assign(data,{districtId:Vue.ls.get("districtId")})
  
  return axios({
    url: api.basicDataPool,
    params: data
  });
}
export function SynergyPage(data) {
  return axios({
    url: api.SynergyPage,
    params: data
  });
}
export function companyPage(data) {
  return axios({
    url: api.companyPage,
    params: data
  });
}
export function watchPage(data) {
  return axios({
    url: api.watchPage,
    params: data
  });
}
export function findPageList(data) {
  return axios({
    url: api.findPageList,
    params: data
  });
}
export function findSensorDeviceList(data) {
  return axios({
    url: api.findSensorDeviceList,
    params: data
  });
}
export function lingangDevices(data) {
  return axios({
    url: api.lingangDevices,
    params: data
  });
}
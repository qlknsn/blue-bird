const api = {
    // 登录接口
    commpanyList: "/lingangresultful/companys/commpanyList",
    videoList:'/lingangresultful/basicdata/ploatformCamera/list',
    basicDataPool:'/lingangresultful/basicdata/basicData/findBasicData',
    SynergyPage:'/lingangresultful/basicdata/basicData/SynergyPage',
    companyPage:'/lingangresultful/basicdata/basicData/companyPage',
    watchPage:'/lingangresultful/basicdata/watch/watchPage',
    findPageList:'/lingangresultful/basicdata/faceTemp/findPageList',
    findSensorDeviceList:'/lingangresultful/basicdata/sensor/findSensorDevicePage',
    lingangDevices:'/getCamera/v1/devices'
  };
  
  export default api;
import api from "../flow/flowUrl"
import {axios} from "@/utils/requests"
export function meetingsList(params){
    return axios({
        method:'get',
        url:api.meetingsList,
        params:params
    })
}

export function eventList(params){
    return axios({
        method:'get',
        url:api.eventList,
        params:params
    })
}

export function meetingDetail(params){
    return axios({
        method:'get',
        url:api.meetingDetail,
        params:params
    })
}

export function orderInfo(params){
    return axios({
        method:'get',
        url:api.orderInfo,
        params:params
    })
}

export function taskInfo(params){
    return axios({
        method:'get',
        url:api.taskInfo,
        params:params
    })
}
const api = {
    meetingsList:'/lingangresultful/task/v4/districtmeetings/listMeetings',
    eventList:"/lingangresultful/task/occurrenceDynamicProcess/list",
    meetingDetail:'/lingangresultful/task/v4/districtmeetings/detail',
    orderInfo:'/lingangresultful/basicdata/districtWorkOrder/info',
    taskInfo:"/lingangresultful/task/v4/taskinfo",

}
export default api
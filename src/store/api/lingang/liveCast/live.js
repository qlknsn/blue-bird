import api from "./liveUrl";
import { axios } from "@/utils/requests";
// import config from '@/utils/config.js'

export function getLiveUrl(data) {
  return axios({
    url: api.getLiveUrl,
    params: data
  });
}
export function getLingangUrl(params) {
  return axios({
    url:api.getLiveUrl,
    params: params
  });
}

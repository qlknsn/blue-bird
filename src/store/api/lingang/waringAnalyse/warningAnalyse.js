
import api from '../waringAnalyse/warningAnalyseUrl'
import {axios} from "@/utils/requests"

export function warnAnalyse(params){
    return axios({
        method:'get',
        url:api.warnAnalyse,
        params:params
    })
}
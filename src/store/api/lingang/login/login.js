import api from './loginUrl'
import {axios} from "@/utils/requests";
export function lgLogin(data) {
  return axios({
    url: api.lgLogin,
    params: data
  })
}
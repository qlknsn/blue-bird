const api ={
    cityState:'/lingangresultful/basicdata/metro/findMetroData',
    findMetroPoint:'/lingangresultful/basicdata/metro/findMetroPoint',
    findMetroPointInfo:'/lingangresultful/basicdata/metro/findMetroPointInfo',
    findMetroPointList:'/lingangresultful/basicdata/metro/findMetroPointList'
}
export default api
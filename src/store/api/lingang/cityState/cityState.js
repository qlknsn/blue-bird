import api from '../cityState/cityStateUrl'
import {axios} from "@/utils/requests"
export function cityState(params){
    return axios({
        method:'get',
        url:api.cityState,
        params:params
    })
}
export function findMetroPoint(params){
    return axios({
        method:'post',
        url:api.findMetroPoint+'?infoatname='+params.infoatname+"&"+"times="+params.times+"&"+"districtId=" +params.districtId,
    }) 
}

export function findMetroPointInfo(params){
    return axios({
        method:'post',
        url:api.findMetroPointInfo+'?keyWord='+params.keyWord+"&"+"id="+params.id+"&"+"types="+params.types+"&"+"infoatname=WFJZ",
    }) 
}

export function findMetroPointList(params){
    return axios({
        method:'post',
        // url:api.findMetroPointList+'?keyWord='+params.keyWord+"&"+"id="+params.id+"&"+"types="+params.types+"&"+"infoatname=WFJZ",
        url:api.findMetroPointList,
        params:params
    }) 
}
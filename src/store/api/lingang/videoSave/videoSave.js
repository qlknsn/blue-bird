import api from "./videoSaveUrl";
import { axios } from "@/utils/requests";

export function lgGetVideoList(data) {
  return axios({
    url: api.lgGetVideoList,
    params: data
  });
}
export function lgGetMessageList(data) {
  return axios({
    url: api.lgGetMessageList,
    params: data
  });
}
export function lgVideoTimeSet(data) {
  return axios({
    url: api.lgVideoTimeSet,
    params: data
  })
}

const api = {
  // 视频保障接口
  lgGetVideoList: "/lingangresultful/basicdata/ploatformCamera/list",
  lgGetMessageList: '/lingangresultful/basicdata/notice/noticePage',
  lgVideoTimeSet: '/lingangresultful/basicdata/ploatformCamera/updatePlayChangeTime',
};

export default api;

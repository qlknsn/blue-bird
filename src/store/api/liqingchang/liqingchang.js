import api from "./liqingchangUrl";
import { axios } from "@/utils/requests";

export function getliqingEchart(data) {
  return axios({
    url: api.getliqingEchart,
    method: "get",
    params: data
  });
}
export function getshiliaoEchart(data) {
  return axios({
    url: api.getshiliaoEchart,
    method: "get",
    params: data
  });
}
export function getProdutionInfo(data) {
  return axios({
    url: api.getProdutionInfo,
    method: "get",
    params: data
  });
}
export function getSearchProdution(data) {
  return axios({
    url: api.getSearchProdution,
    method: "get",
    params: data
  });
}
export function saveRecord(data) {
  return axios({
    url: api.saveRecord,
    method: "post",
    params: data
  });
}
export function getTrueProduct(data) {
  return axios({
    url: api.getTrueProduct,
    method: "get",
    params: data
  });
}
export function getWeather(data) {
  return axios({
    url: api.getWeather,
    method: "get",
    params: data
  });
}
export function getElectricity(data) {
  return axios({
    url: api.getElectricity,
    method: "get",
    params: data
  });
}
export function getNaturalGas(data) {
  return axios({
    url: api.getNaturalGas,
    method: "get",
    params: data
  });
}

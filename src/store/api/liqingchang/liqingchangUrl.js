const api = {
  getliqingEchart: "/asp/v1/asphalt/getAsphaltStock",
  getshiliaoEchart: "/asp/v1/stone/getStoneStock",
  getProdutionInfo: "/asp/v1/standard/getProducedSum",
  getSearchProdution: "/asp/v1/standard/getProject",
  saveRecord: "/asp/v1/standard/saveStandardObject",
  getTrueProduct: "/asp/v1/blendPlan/date",
  getWeather: "/asp/v1/common/weather",
  getElectricity: "/asp/v1/common/electricity",
  getNaturalGas: "/asp/v1/common/naturalGas"
};
export default api;

const api = {
  // 基础信息
  getPersonnelCount: "/getstreetvideo/oldStreet/v1/personnel/getPersonnelCount",
  // 老街景点
  getOldStreetCount:
    "/getstreetvideo/oldStreet/v1/otherScenic/getOldStreetCount",
  // 人流量统计
  countStatistics: "/getstreetvideo/oldStreet/v1/camera/countStatistics",
  // 人流\客流量折线图
  lineStatistics: "/getstreetvideo/oldStreet/v1/camera/lineStatistics",
  //老街景点撒点
  getScenicCoordinateInfo:
    "/getstreetvideo/oldStreet/v1/otherScenic/getScenicCoordinateInfo",
  //老街景点详情
  getOtherScenicOne:
    "/getstreetvideo/oldStreet/v1/otherScenic/getOtherScenicOne",
  // //智能告警 - 汇总统计
  getWarncountStatistics: "/getstreetvideo/seaweed/v1/task/countStatistics",
  // //智能告警 - 大屏撒点
  getTaskCoordinateInfo:
    "/getstreetvideo/seaweed/v1/task/getTaskCoordinateInfo",
  // //问题投诉数量
  getProblemCount: "/getstreetvideo/oldStreet/v1/question/getProblemCount",
  //智能设备数量
  getCountDevice: "/getstreetvideo/oldStreet/v1/point/countDevice",
  //智能设备撒点
  getListDevice: "/getstreetvideo/oldStreet/v1/point/listDevice",
  // //智能告警页面详情统计
  getTaskDetailsInfo: "/getstreetvideo/seaweed/v1/task/getTaskDetailsInfo",
  // 获取摄像机配置
  getDefaultConfig: "/getstreetvideo/oldStreet/v1/camera/getDefaultConfig",
  // 获取摄像机树形结构
  findTree: "/getstreetvideo/oldStreet/v1/camera/findTree",
  // 请求推流
  connectStream: "/getstreetvideo/oldStreet/v1/camera/connect",
  // 结束推流
  disConnectStream: "/getstreetvideo/oldStreet/v1/camera/disConnect",
  // 保存设备配置
  saveDefaultConfig: "/getstreetvideo/oldStreet/v1/camera/saveDefaultConfig",
  // 获取所有探头撒点
  getallcameralist: "/getstreetvideo/oldStreet/v1/camera/list",
  // 获取任务详情
  gettaskInfo: "/getstreetvideo/seaweed/v1/task/info"
};
export default api;

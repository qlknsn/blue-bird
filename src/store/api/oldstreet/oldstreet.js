import api from "./oldstreetUrl";
// import { axios } from "@/utils/requests";
import axios from "axios";

export function getPersonnelCount(data) {
  return axios({
    url: api.getPersonnelCount,
    params: data
  });
}
export function getOldStreetCount(data) {
  return axios({
    url: api.getOldStreetCount,
    params: data
  });
}
export function countStatistics(data) {
  return axios({
    url: api.countStatistics,
    params: data
  });
}
export function lineStatistics(data) {
  return axios({
    url: api.lineStatistics,
    params: data
  });
}
export function getScenicCoordinateInfo(data) {
  return axios({
    url: api.getScenicCoordinateInfo,
    params: data
  });
}
export function getOtherScenicOne(data) {
  return axios({
    url: api.getOtherScenicOne,
    params: data
  });
}
export function getWarncountStatistics(data) {
  return axios({
    url: api.getWarncountStatistics,
    params: data
  });
}
export function getTaskCoordinateInfo(data) {
  return axios({
    url: api.getTaskCoordinateInfo,
    params: data
  });
}
export function getProblemCount(data) {
  return axios({
    url: api.getProblemCount,
    params: data
  });
}
export function getTaskDetailsInfo(data) {
  return axios({
    url: api.getTaskDetailsInfo,
    params: data
  });
}
export function getDefaultConfig(data) {
  return axios({
    url: api.getDefaultConfig,
    params: data
  });
}
export function findTree(data) {
  return axios({
    url: api.findTree,
    params: data
  });
}
export function connectStream(data) {
  return axios({
    url: api.connectStream,
    params: data
  });
}
export function disConnectStream(data) {
  return axios({
    url: api.disConnectStream,
    params: data
  });
}
export function saveDefaultConfig(data) {
  return axios({
    url: api.saveDefaultConfig,
    data: data,
    method:'post'
  });
}
export function getallcameralist(data) {
  return axios({
    url: api.getallcameralist,
    params: data,
  });
}
export function gettaskInfo(data) {
  return axios({
    url: api.gettaskInfo,
    params: data,
  });
}
export function getCountDevice(data) {
  return axios({
    url: api.gettaskInfo,
    params: data,
    method:"get"
  });
}
export function getListDevice(data) {
  return axios({
    url: api.getListDevice,
    params: data,
    method:"get"
  });
}
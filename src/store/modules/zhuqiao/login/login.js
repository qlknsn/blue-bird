import { loginUsers,accountLogin } from "@/store/api/zhuqiao/login/login";
// import Vue from "vue";
// import router from '@/router'
// import { LOGIN_USER } from "@/store/mutation-types.js";
import router from "@/router/index";
const state = {
  
};
const actions = {
  loginUsers({ commit }, data) {
    commit;
    let p = loginUsers(data);
    p.then(res => {
      if (res.errcode == 0) {
        router.push('/zhuqiao/dashboard')
      }
    });
  },
  accountLogin({ commit }, data) {
    commit;
    let p = accountLogin(data);
    p.then(res => {
      console.log(res)
    });
  }
};

const mutations = {
  
};

export default {
  state,
  actions,
  mutations
};

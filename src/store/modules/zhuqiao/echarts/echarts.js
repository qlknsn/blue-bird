import { 
    getFacePushCount,
    hourlyTotalPopulation,
    getFaceCamScore,
    getCaseSource,
    getCountByYesterdayHour,
    yesterdayXiuTanAllNum,
    phoneBrandStatistic,
    yesterdayPeopleNumPerHour,
    lastWeekXiuTanPeopleNum,
    lastWeekXiuTanCarNum,
    getPercentageByPrevious,
    getCountByLastSevenDays,
    getPercentageByCarNumber,
    getCountByCam,
    getCaseStatus,
    getCaseType,
    getFaceDBRatio,
    getFaceCamAccuracy,
    getFaceCamPushRatio,
    getFaceCamWeeklyPush
 } from "@/store/api/zhuqiao/echarts/echarts";
// import Vue from "vue";
// import router from '@/router'
// import { LOGIN_USER } from "@/store/mutation-types.js";
// import router from "@/router/index";
const state = {
    dealAllNum:0,
    hourpeoLiuliang:{},
    facecamScore:{},
    todayanjian:[],
    yesterdayCarweizhang:[],
    yesterdayXiutanallnum:0,
    yesterdayXiutanCishu:0,
    phoneStatistic:[],
    yesterdayPeoperhour:[],
    lastWeekXiuTanPeople:[],
    lastWeekXiuTanCar:[],
    getPercentAgeprevious:[],
    getCountLastsevenday:[],
    getPercentCarnumber:[],
    getCountcam:[],
    caseStatus:[],
    caseType:[],
    faceDbratio:{},
    faceCamaccuracy:{},
    faceCampushRatio:{},
    faceCamweeklypush:{}
};
const actions = {
    getFacePushCount({commit}){
        commit;
        return getFacePushCount()
        // let p = getFacePushCount()
        // p.then(res=>{
        //     commit('GET_FACE_PUSH_COUNT',res.data)
        // })
    },
    hourlyTotalPopulation({commit}){
        commit;
        return hourlyTotalPopulation()
        // let p = hourlyTotalPopulation()
        // p.then(res=>{
        //     commit('HOURLYTOTAL_POPULATION',res.data)
        // })
    },
    getFaceCamScore({commit}){
        commit;
        return getFaceCamScore()
        // let p = getFaceCamScore()
        // p.then(res=>{
        //     commit('GET_FACECAM_SCORE',res.data)
        // })
    },
    getCaseSource({commit},params){
        commit;
        return getCaseSource(params)
        // let p = getCaseSource(params)
        // p.then(res=>{
        //     commit('TODAY_ANJIAN_STATISTIC',res.data)
        // })
    },
    getCountByYesterdayHour({commit},params){
        commit;
        return getCountByYesterdayHour(params)
        // let p = getCountByYesterdayHour(params)
        // p.then(res=>{
        //     commit('GETCOUNT_YESTERDAYHOUR',res)
        // })
    },
    yesterdayXiuTanAllNum({commit},params){
        commit;
        return yesterdayXiuTanAllNum(params);
        // let p = yesterdayXiuTanAllNum(params)
        // p.then(res=>{
        //     if(params.type ==0){
        //         commit('YESTERDAY_XIUTANALLNUM',res)
        //     }
        //     if(params.type ==1){
        //         commit('YESTERDAY_XIUTANCI',res)
        //     }  
        // })
    },
    phoneBrandStatistic({commit},params){
        commit;
        return phoneBrandStatistic(params)
        // let p = phoneBrandStatistic(params)
        // p.then(res=>{
        //     commit('PHONE_BRANDSTATISTIC',res)
        // })
    },
    yesterdayPeopleNumPerHour({commit},params){
        commit;
        return yesterdayPeopleNumPerHour(params)
        // let p = yesterdayPeopleNumPerHour(params)
        // p.then(res=>{
        //     commit('YESTERDAY_PEOPLENUMPERHOUR',res)
        // })
    },
    lastWeekXiuTanPeopleNum({commit},params){
        commit;
        return lastWeekXiuTanPeopleNum(params)
        // let p = lastWeekXiuTanPeopleNum(params)
        // p.then(res=>{
        //     commit('LASTWEEKXIUTAN_PEOPLENUM',res)
        // })
    },
    lastWeekXiuTanCarNum({commit},params){
        commit;
        return lastWeekXiuTanCarNum(params)
        // let p = lastWeekXiuTanCarNum(params)
        // p.then(res=>{
        //     commit('LASTWEEKXIUTAN_CARNUM',res)
        // })
    },
    getPercentageByPrevious({commit},params){
        commit;
        return getPercentageByPrevious(params)
        // let p = getPercentageByPrevious(params)
        // p.then(res=>{
        //     commit('GETPERCENT_AGEBYPREVIOUS',res)
        // })
    },
    getCountByLastSevenDays({commit},params){
        commit;
        return getCountByLastSevenDays(params)
        // let p = getCountByLastSevenDays(params)
        // p.then(res=>{
        //     commit('GETCOUNT_LASTSEVENDAYS',res)
        // })
    },
    getPercentageByCarNumber({commit},params){
        commit;
        return getPercentageByCarNumber(params)
        // let p = getPercentageByCarNumber(params)
        // p.then(res=>{
        //     commit('GETPERCENT_CARNUMBER',res)
        // })
    },
    getCountByCam({commit},params){
        commit;
        return getCountByCam(params) 
        // let p = getCountByCam(params)
        // p.then(res=>{
        //     commit('GETCOUNT_CAM',res)
        // })
    },
    getCaseStatus({commit},params){
        let p = getCaseStatus(params)
        p.then(res=>{
            commit('GETCASE_STATUS',res)
        })
    },
    getCaseType({commit},params){
        commit;
        return getCaseType(params)
        // let p = getCaseType(params)
        // p.then(res=>{
        //     commit('GETCASE_TYPE',res)
        // })
    },
    getFaceDBRatio({commit},params){
        commit;
        return getFaceDBRatio(params)
        // let p = getFaceDBRatio(params)
        // p.then(res=>{
        //     let arr = [];
        //     let arr2 = [];
        //     res.data.forEach(item => {
        //         let it = {
        //             name: item.name,
        //             value: item.count
        //         }
        //         arr.push(it)
        //         arr2.push(it.name)
        //     })
        //     commit('GETFACE_DBRATIO',{data:arr,name:arr2})
        // })
    },
    getFaceCamAccuracy({commit},params){
        commit;
        return getFaceCamAccuracy(params)
    },
    getFaceCamPushRatio({commit},params){
        commit;
        return getFaceCamPushRatio(params)
        // let p = getFaceCamPushRatio(params)
        // p.then(res=>{
        //     let arr=[];
        //     let arr2=[];
        //     res.data.sort((a,b)=>{
        //         return b.count - a.count
        //     })
        //     res.data.slice(0,10).forEach(item=>{
        //         let it={
        //             name:item.camName,
        //             value:item.count
        //         }
        //         arr.push(it)
        //         arr2.push(it.name)
        //     })
        //     console.log(res.data.slice(0,10))
        //     // this.data = [...arr]
        //     commit('GETFACE_CAMPUSHRATIO',{value:arr,name:arr2})
        // })
    },
    getFaceCamWeeklyPush({commit},params){
        commit;
        return getFaceCamWeeklyPush(params)
        // let p = getFaceCamWeeklyPush(params)
        // p.then(res=>{
        //     res;
            
        //     commit('GETFACE_CAMWEEKLYPUSH',{value:arr,name:arr2})
        // })
    },
};

const mutations = {
    GET_FACE_PUSH_COUNT(state,res){
        state.dealAllNum = res
    },
    HOURLYTOTAL_POPULATION(state,res){
        state.hourpeoLiuliang = res
    },
    GET_FACECAM_SCORE(state,res){
        state.facecamScore = res
    },
    TODAY_ANJIAN_STATISTIC(state,res){
        state.todayanjian = res
    },
    GETCOUNT_YESTERDAYHOUR(state,res){
        state.yesterdayCarweizhang = res.data
    },
    YESTERDAY_XIUTANALLNUM(state,res){
        state.yesterdayXiutanallnum = res.data
    },
    YESTERDAY_XIUTANCI(state,res){
        state.yesterdayXiutanCishu = res.data
    },
    PHONE_BRANDSTATISTIC(state,res){
        state.phoneStatistic = res.data
    },
    YESTERDAY_PEOPLENUMPERHOUR(state,res){
        state.yesterdayPeoperhour = res.data
    },
    LASTWEEKXIUTAN_PEOPLENUM(state,res){
        state.lastWeekXiuTanPeople = res.data
    },
    LASTWEEKXIUTAN_CARNUM(state,res){
        state.lastWeekXiuTanCar = res.data
    },
    GETPERCENT_AGEBYPREVIOUS(state,res){
        state.getPercentAgeprevious = res.data
    },
    GETCOUNT_LASTSEVENDAYS(state,res){
        state.getCountLastsevenday = res.data 
    },
    GETPERCENT_CARNUMBER(state,res){
        state.getPercentCarnumber =res.data
    },
    GETCOUNT_CAM(state,res){
        state.getCountcam = res.data
    },
    GETCASE_STATUS(state,res){
        state.caseStatus = res.data
    },
    GETCASE_TYPE(state,res){
        state.caseType = res.data
    },
    GETFACE_DBRATIO(state,res){
        state.faceDbratio = res
    },
    GETFACE_CAMACCURACY(state,res){
        state.faceCamaccuracy = res
    },
    GETFACE_CAMPUSHRATIO(state,res){
        state.faceCampushRatio = res
    },
    GETFACE_CAMWEEKLYPUSH(state,res){
        state.faceCamweeklypush = res
    }
};

export default {
  state,
  actions,
  mutations
};
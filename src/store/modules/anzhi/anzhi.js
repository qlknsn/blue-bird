import { changeVideo } from "@/store/api/anzhi/anzhi";

const state = {
  videoList: []
};
const actions = {
  changeVideo({ commit }, data) {
    let p = changeVideo(data);
    p.then(res => {
      res;
      commit;
    });
  }
};
const mutations = {
  SET_ANZHI_VIDEO_LIST(state, data) {
    if (state.videoList.length < 9) {
      console.log("....");
      state.videoList.push(data);
      state.videoList.push("1");
      state.videoList.pop();
    } else {
      if (data.data.position < 9) {
        state.videoList.splice(data.data.position, 1, data);
      }
    }
  }
};

export default {
  state,
  actions,
  mutations
};

import {
  getliqingEchart,
  getshiliaoEchart,
  getProdutionInfo,
  getSearchProdution,
  saveRecord,
  getTrueProduct,
  getWeather,
  getNaturalGas,
  getElectricity
} from "../../api/liqingchang/liqingchang";
const state = {
  liqingList: [],
  shiliaoList: [],
  produtionInfo: {},
  produtionProgress: [],
  produtionInfoEchart: [],
  trueProductList: [],
  weatherInfo: {},
  naturalGas: {},
  electricity: {}
};
const actions = {
  getliqingEchart({ commit }, params) {
    let p = getliqingEchart(params);
    p.then(res => {
      commit("GET_LIQINGCHANG_LIST", res);
    });
  },
  getshiliaoEchart({ commit }, params) {
    let p = getshiliaoEchart(params);
    p.then(res => {
      commit("GET_SHILIAO_LIST", res);
    });
  },
  getProdutionInfo({ commit }, params) {
    let p = getProdutionInfo(params);
    p.then(res => {
      commit("GET_PRODUTION_INFO", res);
    });
  },
  getSearchProdution({ commit }, params) {
    let p = getSearchProdution(params);
    p.then(res => {
      commit("GET_PRODUTION_PROGRESS", res);
    });
  },
  saveRecord({ commit }, params) {
    let p = saveRecord(params);
    p.then(res => {
      console.log("保存称重记录", res);
    });
  },
  getTrueProduct({ commit }, params) {
    let p = getTrueProduct(params);
    p.then(res => {
      commit("GET_TRUE_PRODUCT", res);
    });
  },
  getWeather({ commit }, params) {
    let p = getWeather(params);
    p.then(res => {
      commit("GET_WEATHER_INFO", res);
    });
  },
  getNaturalGas({ commit }, params) {
    let p = getNaturalGas(params);
    p.then(res => {
      commit("GET_NATURAL_GAS", res);
    });
  },
  getElectricity({ commit }, params) {
    let p = getElectricity(params);
    p.then(res => {
      commit("GET_ELECTRICITY", res);
    });
  }
};
const mutations = {
  GET_LIQINGCHANG_LIST(state, res) {
    state.liqingList = res.results;
  },
  GET_SHILIAO_LIST(state, res) {
    state.shiliaoList = res.results;
  },
  GET_PRODUTION_INFO(state, res) {
    state.produtionInfo = res.result;
    state.produtionInfoEchart = res.result.yearSum;
  },
  GET_PRODUTION_PROGRESS(state, res) {
    state.produtionProgress = res.results;
  },
  GET_TRUE_PRODUCT(state, res) {
    state.trueProductList = res.results;
  },
  GET_WEATHER_INFO(state, res) {
    state.weatherInfo = res.results[0];
  },
  GET_NATURAL_GAS(state, res) {
    state.naturalGas = res.result;
  },
  GET_ELECTRICITY(state, res) {
    state.electricity = res.result;
  }
};
export default {
  state,
  actions,
  mutations
};

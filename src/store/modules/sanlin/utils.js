import Vue from 'vue';
import AMap from "AMap";
import '@/store/index'


import {
    ADD_MARKERS,
    CLEAR_MARKERS,
    SET_CURRENT_VIDEO,
    CLEAR_MARKERS_2
}
from '@/store/mutation-types.js'
import store from '../..';

const state = {
    mapTypefunction: { //地图点类别
        // getottff
        // illegalparking
        // smartDevice
        // selfQuarantine
        // convict
        // returnFromAbroad
        // company
        // illegalPark
        '1': function (params) {
            store.dispatch('getSignsAlarm', params)
        },
        '2': function (params) {
            store.dispatch('getottff', params)
        },
        '3': function (params) {
            store.dispatch('illegalparking', params)
        },
        '4': function (params) {
            store.dispatch('smartDevice', params)
        },
        '5': function (params) {
            store.dispatch('selfQuarantine', params)
        },
        '6': function (params) {
            store.dispatch('getZhongdianDetail', params)
        },
        '7': function (params) {
            store.dispatch('returnFromAbroad', params)
        },
        '8': function (params) {
            store.dispatch('getQiyeDanwei', params)
        },
        '9': function (params) {
            store.dispatch('illegalPark', params)
        },
        '41': params => {
            store.dispatch('getDangYuanDetail', params)
        },
        '42': params => {
            store.dispatch('getZhongdianDetail', params)
        },
        '43': params => {
            store.dispatch('getGuanai', params)
        },
        '44': params => {
            store.dispatch('getGuanliLiliang', params)
        },
        '50': params => {
            store.dispatch('getCamera', params)
        },
        '51': params => {
            store.dispatch('getQiyeDanwei', params)
        },
        '52': params => {
            store.dispatch('getMinsu', params)
        },
        'fake-52': params => {
            store.dispatch('firstLoadEvent', params)
        },
        'fake-44': params => {
            store.dispatch('goundPowerPoint', params)
        },
        'fake-61': params => {
            store.dispatch('hedao', params)
        },
        'fake-64': params => {
            store.dispatch('gonggongsheshi', params)
        },
        'fake-93': params => {
            store.dispatch('weiting', params)
        },
        "fake-ga":params => {
            store.dispatch('guanai', params)
        },
        "fake-zhongdian":params =>{
            store.dispatch("zhongdianevent",params)
        },
        "fake-gaojing":params => {
            store.dispatch('geliguancha', params)
        },
        // "fake-zhongdian":params => {
        //     store.dispatch('zhongdian', params)
        // },
        '53': params => {
            store.dispatch('getCanyin', params)
        },
        '54': params => {
            store.dispatch('getSmallMart', params)
        },
        '60': params => {
            store.dispatch('governanceElementsDetail', params)
        },
        '61': params => {
            store.dispatch('getHedao', params)
        },
        '62': params => {
            store.dispatch('getQiaoliang', params)
        },
        '63': params => {
            store.dispatch('getDaolu', params)
        },
        '64': params => {
            store.dispatch('getPublicFacility', params)
        },
        '65': params => {
            store.dispatch('getNongdi', params)
        },
        '30': params => {
            store.dispatch('getCunmin', params)
        },
        '31': params => {
            store.dispatch('getXiaoqu', params)
        },
        '32': params => {
            store.dispatch('getChuzu', params)
        },
        '70': params => {
            store.dispatch('occurrenceDetail', params) //动态流程
        },
        '80': params => {
            store.dispatch('autonomyDetail', params) //动态流程
        },
        '90': params => {
            store.dispatch('getCunminzuDetail', params)
        },
        '100': params => {
            store.dispatch('getFangWuDetail', params)
        },
        '110': params => {
            store.dispatch('getZhibanRenyuan', params)
        },
        '120': params => {
            store.dispatch('getZiyouliliang', params)
        },
        '130': params => {
            store.dispatch('getLianqinliandong', params)
        },
        '999': params => {
            store.dispatch('zhogndianrenyuan', params)
        }
    },
    currentVideoId: 'tts',
    currentVideoSrc: 'rtmp://139.224.69.150:1935/live/01',
    videoList: [{
            videoId: "class1",
            videoSrc: 'rtmp://139.224.69.150:1935/live/09'
        },
        {
            videoId: 'class12',
            videoSrc: 'rtmp://139.224.69.150:1935/live/07'
        },
        {
            videoId: 'class13',
            videoSrc: 'rtmp://139.224.69.150:1935/live/03'
        },
        {
            videoId: 'class14',
            videoSrc: 'rtmp://139.224.69.150:1935/live/04'
        },
    ],
    markerList: []
}

const actions = {}
const mutations = {
    [SET_CURRENT_VIDEO](state, data) {
        state.currentVideoSrc = data.currentVideoSrc
    },
    [CLEAR_MARKERS](state) {
        state.markerList.forEach(marker => {
            
             Vue.prototype.globalMap.remove(marker)
  
        })
        store.dispatch('clearDialog')
    },
    [CLEAR_MARKERS_2](state, type) {

        switch (type) {
            case 'jrsx':
                state.markerList.forEach(marker => {
                    if (marker.w.data.type === 'fake-52'||marker.w.data.type === 'fake-61'||marker.w.data.type === 'fake-64'||
                    marker.w.data.type === 'fake-93'||marker.w.data.type == "fake-ga"||
                    marker.w.data.type == "fake-gaojing" ||marker.w.data.type == "fake-zhongdian") {
                        Vue.prototype.globalMap.remove(marker)
                    }

                })
                break;
            case 'zzll':
                state.markerList.forEach(marker => {
                    if (marker.w.data.type === 'fake-44') {
                        Vue.prototype.globalMap.remove(marker)
                    }

                })
                break;
            case 'xtll':
                state.markerList.forEach(marker => {
                    if (marker.w.data.type === 130) {
                        Vue.prototype.globalMap.remove(marker)
                    }

                })
                break;
        }
        // if(type==='zzll'){
        //     state.markerList.forEach(marker => {
        //         if(marker.w.data.type==='fake-44'){
        //             Vue.prototype.globalMap.remove(marker)
        //         }

        //     })
        // }

        // state.markerList.forEach(marker => {
        //     Vue.prototype.globalMap.remove(marker)
        // })
        store.dispatch('clearDialog')
    },
    [ADD_MARKERS](state, data) {
        data.forEach(item => {
            var marker = null
            if (item.iconUrl.indexOf('http') !== -1) {
                marker = new AMap.Marker({
                    position: new AMap.LngLat(item.lng, item.lat), // 经纬度对象，也可以是经纬度构成的一维数组[116.39, 39.9]
                    icon: item.iconUrl,
                    data: item
                });
            } else {
                marker = new AMap.Marker({
                    position: new AMap.LngLat(item.lng, item.lat), // 经纬度对象，也可以是经纬度构成的一维数组[116.39, 39.9]
                    icon: process.env.VUE_APP_SECRET + item.iconUrl,
                    data: item
                });
            }
            store.commit('clear4typevisible')
            if (item.type.toString() == '41') {
                store.commit('setdangyuanvisible', true)
            }
            if (item.type.toString() == '52') {
                store.commit('setminsuvisible', true)
            }
            if (item.type.toString() == '43') {
                store.commit('setguanaivisible', true)
            }
            if (item.type.toString() == '6') {
                store.commit('setzhongdianvisible', true)
            }
            if (item.type == '999') {
                store.commit('setzhongdianvisible', true)
            }
            if (item.type.toString() == '44') {
                store.commit('setguanlipowervisible', true)
            }
            marker.on('click', function (ev) {
                // 触发事件的对象
                // var target = ev.target;

                // // 触发事件的地理坐标，AMap.LngLat 类型
                // var lnglat = ev.lnglat;

                // // 触发事件的像素坐标，AMap.Pixel 类型
                // var pixel = ev.pixel;

                // // 触发事件类型
                // var type = ev.type;
                ev;
                if (this.w.data.type == 2 || this.w.data.type == 3 || this.w.data.type == 5 || this.w.data.type == 9 ||
                    this.w.data.type == 60 || this.w.data.type == 70 || this.w.data.type == 80 || this.w.data.type == "fake-52" ||
                    this.w.data.type == "fake-44" || this.w.data.type == "999" || this.w.data.type == "fake-64" ||
                    this.w.data.type == "fake-61" || this.w.data.type == "fake-93"||this.w.data.type == "fake-ga"||
                    this.w.data.type == "fake-gaojing"||this.w.data.type == "fake-zhongdian") {
                    let singleFunction = state.mapTypefunction[this.w.data.type]
                    singleFunction(this.w.data)
                } else {
                    let singleFunction = state.mapTypefunction[this.w.data.type]
                    singleFunction({
                        id: this.w.data.id
                    })
                }

            });
            Vue.prototype.globalMap.add(marker);
            state.markerList.push(marker)
        })
    }
}


export default {
    state,
    actions,
    mutations
}
import { loginUser,sanlinLogin } from "@/store/api/sanlin/login/login";
import Vue from "vue";
// import router from '@/router'
import { LOGIN_USER } from "@/store/mutation-types.js";
import router from "@/router/index";
const state = {
  eventMap: new Map([
    [
      "sanlin",
      () => {
        router.push("/sanlin/dashboard/v3");
      }
    ],
    [
      "zhoujiadu",
      () => {
        router.push("/zhoujiadu/dashboard");
      }
    ],
    [
      "lxz",
      () => {
        router.push("/liexiongzuo/dashboard");
      }
    ]
  ])
};
const actions = {
  loginUser({ commit }, data) {
    let p = loginUser(data);
    p.then(res => {
      commit(LOGIN_USER, res);
    });
  },
  sanlinLogin({ commit }, data) {
    commit;
    let p = sanlinLogin(data);
    return p;
  },
};

const mutations = {
  [LOGIN_USER](...arg) {
    let [
      state,
      {
        auth: { token },
        properties: { zone }
      }
    ] = [...arg];
    state;
    Vue.ls.set("token", token);
    Vue.ls.set("zone", zone);
    let district = Vue.ls.get("district");
    if (token && district) {
      state.eventMap.get(district)();
    } else {
      alert("系统错误");
    }
  }
};

export default {
  state,
  actions,
  mutations
};

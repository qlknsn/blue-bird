import {
  graphPie,
  graphBar,
  garbagePie,
  garbageLine,
  districtsXq,
  getLastInfo,
  getLeftTopInfo,
  getLeftMiddleInfo,
  getPassWordList,
  getWeikakouList,
  getWeikakouPoints,
  configList,
  getWeikakouDetail,
  getZhuapaiPoints,
  getZhuapaiDetail,
  getZaoyinBar,
  getDropList,
  getRenqunMiduList,
  getImagesList,
  getVoicePointList,
  getVoiceList,
  getVoiceDetail,
  videoPollingList,
  runTaskNow,
  getPeopleHotEchart,
  graphLine,
  garbageAvgList,
  getAvgRank,
  getRank,
  dispatchBar,
  dispatchPie,
  getBarbageLine,
  getVoiceLiveUrl
} from "@/store/api/sanlin/sanlinecharts/sanlinecharts";
//   import router from "@/router/index";
import Vue from "vue";
import store from "@/store/index";
import AMap from "AMap";
import { Message } from "element-ui";
import {
  CLEAR_MARKERS,
  GET_DEVICE_LIST,
  RUN_TASK_NOW,
  GETPEOPLEHOTECHART,
  GET_VOICE_LIVE_URL
} from "@/store/mutation-types";

// import { Store } from "vuex";
// import vueConfig from "../../../../../vue.config";
import { WSconnect } from "@/utils/websockt";
// import {GET_LIVE_URL} from "../../../mutation-types";
const state = {
  xiaoquList: [],
  lastPicInfo: [],
  popContainerVisible: false,
  zaoyinTableVisible: false,
  leftTopInfo: [],
  fourArr: [],
  oneArr: [{ count: "", activeCount: "", type: "" }],
  warnPeoplepopVisible: false, //重点人员信息
  weikakoupopVisible: false, //微卡口
  locuspopVisible: false, //轨迹
  escapePeoplepopVisible: false, //在逃
  deviceProbingVisible: false, //设备检测
  deviceProbingpicVisible: false, //设备检测pic
  leftMiddleInfo: {},
  leftEchartVisit: false,
  zhuapaipopVisible: false,
  passwordList: [],
  weikakouList: [],
  weikakouTableVisible: false,
  weikakouPeopleType: "",
  weikakouPeopleTypeName: "",
  pageData: {},
  weikakouPoints: [],
  configLists: {},
  weikakouMarkerList: [],
  weikakouDetail: { deptName: "", cameras: [] },
  zhuapaiPoints: [],
  renkoumiduTableVisible: false,
  zhuapaiDetail: {
    warningPush: {}
  },
  zaoyinBar: {},
  dropList: [],
  renqunmiduList: {},
  renqunmidupagination: {},
  rightEchartVisit: false,
  imageList: [],
  imageListPagination: {},
  voiceList: [],
  voicePagination: {},
  voiceDetail: {},
  videopollingList: {},
  hotPointsList: [],
  carDetail: {},
  voiceTuliVisitble: false,
  heatmap:null,
  lockTimes:0,
  gongdanTrendvisible:false,
  garbageTrendvisible: false,
  videoBoxvisible: false,
  graphLinedata:{
    xAxis:[],
    data:{}
  },
  garbagelsitVisible:false,
  garbageavglist:[],
  garbagelistTotal:0,
  avgRankData: {},
  rankData: {},
  peopleMiduPopVisible: false,
  peopleMiduData: {},
  garbageLineData: {},
  peopleMiduList: [],
  voiceLiveNode: { metadata: { url: "" } }
};
const actions = {
  // GARBAGE_AVGLIST
  garbageAvgList({commit}, params){
    let p = garbageAvgList(params)
    p.then(res=>{
      commit('GARBAGE_AVGLIST',res)
    })
  },
  dispatchBar({commit},params){
    commit;
    return dispatchBar(params)
  },
  dispatchPie({commit},params){
    commit;
    return dispatchPie(params)
  },
  getBarbageLine({commit}, params){
    let p = getBarbageLine(params)
    p.then(res=>{
      commit('GETBARBAGELINE',res)
    })
  },
  getAvgRank({commit}, params){
    let p1 = getAvgRank(params)
    let p2 = getRank(params)
    Promise.all([p1, p2]).then((result) => {
      commit('GETRANKDATA',result)
      commit('GARBAGE_LIST_VISIBLE',true)
    }).catch((error) => {
      console.log(error)
    })
  },
  graphLine({commit},params){
    let p = graphLine(params)
    p.then(res=>{
      commit('GRAPH_LINE',res)
    })
  },
  graphPie({ commit }, params) {
    commit;
    return graphPie(params);
  },
  graphBar({ commit }, params) {
    commit;
    return graphBar(params);
  },
  garbagePie({ commit }, params) {
    commit;
    return garbagePie(params);
  },
  garbageLine({ commit }, params) {
    commit;
    return garbageLine(params);
  },
  districtsXq({ commit }, params) {
    let p = districtsXq(params);
    p.then(res => {
      commit("DISTRICTSXQ", res);
    });
  },
  getLastInfo({ commit }, params) {
    let p = getLastInfo(params);
    p.then(res => {
      commit("GETLASTINFO", res);
    });
  },
  getLeftTopInfo({ commit }, params) {
    let p = getLeftTopInfo(params);
    p.then(res => {
      commit("GETLEFTTOPINFO", res);
    });
  },
  getLeftMiddleInfo({ commit }, params) {
    state.leftEchartVisit = false;
    let p = getLeftMiddleInfo(params);
    p.then(res => {
      commit("GETLEFTMIDDLEINFO", res);
      state.leftEchartVisit = true;
    });
  },
  getPassWordList({ commit }, params) {
    let p = getPassWordList(params);
    p.then(res => {
      commit("GETPASSWORDLIST", res);
    });
  },
  getWeikakouList({ commit }, params) {
    commit("WEIKAKOUTABLEVISIBLE", false);
    let p = getWeikakouList(params);
    p.then(res => {
      commit("GETWEIKAKOULIST", res);
      commit("ZAOYINTABLEVISIBLE", false);
      commit("WEIKAKOUTABLEVISIBLE", true);
      commit("RENKOUMIDUTABLEVISIBLE", false);
      commit('SAVE_TIEM')
    });
  },
  getWeikakouPoints({ commit }, params) {
    let p = getWeikakouPoints(params);
    p.then(res => {
      commit("GETWEIKAKOUPOINTS", res);
    });
  },
  configList({ commit }, params) {
    let p = configList(params);
    p.then(res => {
      commit("CONFIGLIST", res);
      store.dispatch("getWeizhangPic", {});
    });
  },
  getWeikakouDetail({ commit }, params) {
    let p = getWeikakouDetail(params);
    p.then(res => {
      commit("GETWEIKAKOUDETAIL", res);
    });
  },
  getZhuapaiPoints({ commit }, params) {
    let iconUrl = "123";
    switch (state.weikakouPeopleType) {
      case 3604:
        iconUrl = "/img/icons/points/zhongdianrenyuan@2x.png";
        break;
      case 3605:
        iconUrl = "/img/icons/points/gaoweihujidi@2x.png";
        break;
      case 3606:
        iconUrl = "/img/icons/points/zaitaorenyuan@2x.png";
        break;
      case 3607:
        iconUrl = "/img/icons/points/xidurenyuan@2x.png";
        break;
      default:
        break;
    }
    let p = getZhuapaiPoints(params);
    p.then(res => {
      res.iconUrl = iconUrl;
      commit("GETZHUAPAIPOINTS", res);
    });
  },
  getZhuapaiDetail({ commit }, params) {
    let p = getZhuapaiDetail(params);
    p.then(res => {
      commit("GETZHUAPAIDETAIL", res);
    });
  },
  getZaoyinBar({ commit }, params) {
    state.rightEchartVisit = false;
    let p = getZaoyinBar(params);
    p.then(res => {
      commit("GETZAOYINBAR", res);
      state.rightEchartVisit = true;
    });
  },
  getDropList({ commit }, params) {
    let p = getDropList(params);
    p.then(res => {
      commit("GETDROPLIST", res);
    });
  },
  getRenqunMiduList({ commit }, params) {
    let p = getRenqunMiduList(params);
    p.then(res => {
      commit("GETRENQUNMIDULIST", res);
      commit("ZAOYINTABLEVISIBLE", false);
      commit("WEIKAKOUTABLEVISIBLE", false);
      commit("RENKOUMIDUTABLEVISIBLE", true);
    });
  },
  getImagesList({ commit }, params) {
    let p = getImagesList(params);
    p.then(res => {
      commit("GETIMAGELIST", res);
    });
  },
  getVoicePointList({ commit }, params) {
    let p = getVoicePointList(params);
    p.then(res => {
      commit("GETVOICEPOINTLIST", res);
    });
  },
  getVoiceList({ commit }, params) {
    let p = getVoiceList(params);
    p.then(res => {
      commit("GETVOICELIST", res);
      commit("ZAOYINTABLEVISIBLE", true);
      commit("WEIKAKOUTABLEVISIBLE", false);
      commit("RENKOUMIDUTABLEVISIBLE", false);
    });
  },
  getVoiceDetail({ commit }, params) {
    let p = getVoiceDetail(params);
    p.then(res => {
      commit("GETVOICEDETAIL", res);
    });
  },
  videoPollingList({ commit }, params) {
    let p = videoPollingList(params);
    p.then(res => {
      commit("VIDEOPOLLINGLIST", res);
    });
  },
  runTaskNow({ commit }, params) {
    //立即执行某个轮屏任务
    let p = runTaskNow(params);
    p.then(res => {
      commit(RUN_TASK_NOW, res);
    });
  },
  getPeopleHotEchart({ commit }, params) {
    let p = getPeopleHotEchart(params);
    p.then(res => {
      // console.log(res.result[0])
      setTimeout(() => {
        Vue.prototype.globalAMap.setZoomAndCenter(14,[res.result[0].lng,res.result[0].lat])
      }, 2000);
      commit("GETPEOPLEHOTECHART", res);
    });
  },
  getVoiceLiveUrl({commit}, params) {
    let p = getVoiceLiveUrl(params)
    p.then(res => {
      commit(GET_VOICE_LIVE_URL, res);
    })
  }
};

const mutations = {
  GAT_PEOPLE_DATALIST(state, res) {
    state.peopleMiduList = res
  },
  GETBARBAGELINE(state, res){
    state.garbageLineData = res.result
  },
  DISTRICTSXQ(state, res) {
    res.result.unshift({ id: "", name: "全部" });
    state.xiaoquList = res.result;
  },
  GETCARDETAIL(state, res) {
    state.carDetail = res;
  },
  GETLASTINFO(state, res) {
    state.lastPicInfo = res.result.slice(0,2);
    // console.log('违章停车', state.lastPicInfo.slice(0,2))
  },
  CHANGEPOPvISIBLE(state, res) {
    state.popContainerVisible = res;
  },
  ZAOYINTABLEVISIBLE(state, res) {
    state.zaoyinTableVisible = res;
  },
  WARNPEOPLEPOPVISIBLE(state, res) {
    state.warnPeoplepopVisible = res;
  },
  WEIKAKOUPOPVISIBLE(state, res) {
    state.weikakoupopVisible = res;
  },
  LOCUSPOPVISIBLE(state, res) {
    state.locuspopVisible = res;
  },
  ESCAPEPEOPLEPOPVISIBLE(state, res) {
    state.escapePeoplepopVisible = res;
  },
  DEVICEPROBINGVISIBLE(state, res) {
    state.deviceProbingVisible = res;
  },
  DEVICEPROBINGPICVISIBLE(state, res) {
    state.deviceProbingpicVisible = res;
  },
  GETLEFTTOPINFO(state, res) {
    state.leftTopInfo = res.results;
    const fourArr = state.leftTopInfo.slice(0, 4);
    fourArr.forEach(item => {
      state.fourArr.push(item);
    });
    state.oneArr = state.leftTopInfo.slice(4, 5);
  },
  GETLEFTMIDDLEINFO(state, res) {
    state.leftMiddleInfo = res.result;
  },
  ZHUAPAIPOPVISIBLE(state, res) {
    state.zhuapaipopVisible = res;
  },
  GETPASSWORDLIST(state, res) {
    state.passwordList = res.results;
  },
  GETWEIKAKOULIST(state, res) {
    state.weikakouList = res.results;
    state.pageData = res.pagination;
  },
  WEIKAKOUTABLEVISIBLE(state, res) {
    state.weikakouTableVisible = res;
  },
  WEIKAKOUPEOPLETYPE(state, res) {
    state.weikakouPeopleType = res;
  },
  WEIKAKOUPEOPLETYPENAME(state, res) {
    state.weikakouPeopleTypeName = res;
  },
  GETWEIKAKOUPOINTS(state, res) {
    store.commit("CLEARMARKERS");
    res.results.map(item => {
      if (item.center) {
        let marker = null;
        marker = new AMap.Marker({
          position: new AMap.LngLat(item.center[0], item.center[1]),
          extData: item,
          icon: "/img/icons/points/weikakou@2x.png",
          size: 20
        });
        marker.on("click", e => {
          // 这里实现点击操作
          // console.log(e)
          let deptid = e.target.w.extData.id;
          store.dispatch("getWeikakouDetail", { deptId: deptid });
          store.commit("WEIKAKOUPOPVISIBLE", true);
        });
        state.weikakouMarkerList.push(marker);
      }
    });
    Vue.prototype.globalAMap.add(state.weikakouMarkerList);
    // state.weikakouPoints = res
  },
  RENKOUMIDUTABLEVISIBLE(state, res) {
    state.renkoumiduTableVisible = res;
  },
  CONFIGLIST(state, res) {
    state.configLists = res.result;
    localStorage.websocketUrl = state.configLists.websocketServer
    WSconnect(state.configLists.websocketServer);
  },
  GETWEIKAKOUDETAIL(state, res) {
    state.weikakouDetail = res.result;
  },
  GETZHUAPAIPOINTS(state, res) {
    store.commit("CLEARMARKERS");
    state.zhuapaiPointsPage = res.pagination;
    state.zhuapaiPoints = res.results;
    let iconIndex = 1;
    res.results.map(item => {
      if (item.camlnglat) {
        // 点标记显示内容，HTML要素字符串
        var markerContent =
          "" +
          '<div class="custom-content-marker">' +
          '   <img src="' +
          `${res.iconUrl}` +
          '">' +
          '   <div class="close-btn">' +
          `${iconIndex++}` +
          "</div>" +
          "</div>";

        let marker = new AMap.Marker({
          position: new AMap.LngLat(item.camlnglat[0], item.camlnglat[1]),
          content: markerContent, // todo
          offset: new AMap.Pixel(-13, -30), // todo
          extData: item,
          size: 20
          // icon: res.iconUrl
        });

        marker.on("click", e => {
          console.log(e);
          // marker.moveAlong(lineArr, 200);
          let id = e.target.w.extData.id;
          store.dispatch("getZhuapaiDetail", { id: id });
          store.commit("WARNPEOPLEPOPVISIBLE", true);
        });
        state.weikakouMarkerList.push(marker);
      }
    });
    Vue.prototype.globalAMap.add(state.weikakouMarkerList);
  },
  GETZHUAPAIDETAIL(state, res) {
    state.zhuapaiDetail = res.result;
  },
  GETZAOYINBAR(state, res) {
    state.zaoyinBar = res.result;
  },
  GETDROPLIST(state, res) {
    state.dropList = res.result;
  },
  GETRENQUNMIDULIST(state, res) {
    state.renqunmiduList = res.results;
    state.renqunmidupagination = res.pagination;
  },
  GETIMAGELIST(state, res) {
    state.imageList = res.results;
    state.imageListPagination = res.pagination;
    // 展示三级的弹窗
    store.commit("ZHUAPAIPOPVISIBLE", true);
  },
  GETVOICEPOINTLIST(state, res) {
    store.commit("CLEARMARKERS");
    state.voiceList = res.results;
    res.results.map(item => {
      if (item.lat) {
        let imgUrl = "";
        if (item.state === 1) {
          imgUrl = "/img/icons/points/zaoyin_normal@2x.png";
        } else {
          imgUrl = "/img/icons/points/zaoyin_unnormal@2x.png";
        }
        let marker = new AMap.Marker({
          position: new AMap.LngLat(item.lng, item.lat),
          extData: item,
          icon: imgUrl,
          size: 20
        });
        marker.on("click", e => {
          let deviceUrn = e.target.w.extData.deviceUrn;
          store.dispatch("getVoiceDetail", { deviceUrn: deviceUrn });
          store.commit("DEVICEPROBINGVISIBLE", true);
        });
        state.weikakouMarkerList.push(marker);
      }
    });
    Vue.prototype.globalAMap.add(state.weikakouMarkerList);
  },
  GETVOICELIST(state, res) {
    state.voiceList = res.results;
    state.voicePagination = res.pagination;
  },
  GETVOICEDETAIL(state, res) {
    state.voiceDetail = res.result;
  },
  CLEARMARKERS(state) {
    Vue.prototype.globalAMap.remove(state.weikakouMarkerList);
    state.weikakouMarkerList = [];
    store.commit("CLEAR_CLUSTERER");
  },
  VIDEOPOLLINGLIST(state, res) {
    let index = 0;
    let arr = [];
    while(index<res.results.length){
      arr.push(res.results.slice(index,index+=9))
    }
    // console.log(arr)
    state.videopollingList = arr;
  },
  [RUN_TASK_NOW](...arg) {
    let [, { msg }] = [...arg];
    if (arg.status === 1) {
      Message.success('设置成功');
    } else {
      Message.warning(msg);
    }
  },
  [GETPEOPLEHOTECHART](state, res) {
    state.hotPointsList = res.result;
    setTimeout(() => {
      var heatmap;
      Vue.prototype.globalAMap.plugin(["AMap.Heatmap"], function() {
        //初始化heatmap对象
        state.heatmap = new AMap.Heatmap(Vue.prototype.globalAMap, {
          radius: 25, //给定半径
          opacity: [0, 0.8]
        });
        //设置数据集：该数据为北京部分“公园”数据
        state.heatmap.setDataSet({
          data: res.result,
          max: 100
        });
      });
    }, 2500);
  },
  CLEARHOTECHART(state){
    state.heatmap.setDataSet({
      data: [],
      max: 100
    });
    state.heatmap.hide()
  },
  VIOCE_TULI_VISITBLE (state, res) {
    state.voiceTuliVisitble = res
  },
  GONGDAN_TREND_VISITBLE (state, res) {
    state.gongdanTrendvisible = res
  },
  GARBAGE_TREND_VISIBLE (state, res) {
    state.garbageTrendvisible = res
  },
  VIDEO_BOX_VISIBLE (state, res) {
    state.videoBoxvisible = res
  },
  CHANGE_URL (state, res) {
    state.voiceLiveNode.metadata.url = res
  },
  LOCK_TIME(state){
    let lockTime = Vue.ls.get('lock_time')
    state.lockTimes = lockTime
    let timestamp = new Date().getTime()
    if(lockTime){
      return timestamp-lockTime
    }
  },
  SAVE_TIEM(state){
    let timestamp = new Date().getTime()
    Vue.ls.set('lock_time',timestamp)
  },
  GRAPH_LINE(state,res){
    state.graphLinedata = res.result
  },
  GETRANKDATA(state,res){
    let [avgRankData, rankData] = res
    state.avgRankData = avgRankData.result
    state.rankData = rankData.result
  },
  GARBAGE_LIST_VISIBLE(state,res){
    state.garbagelsitVisible = res
  },
  GARBAGE_AVGLIST(...arg){
    let [state,{results,pagination:{total}}] = [...arg]
    state.garbagelistTotal = total;
    state.garbageavglist = results
  },
  [GET_VOICE_LIVE_URL](...arg) {
    let [state, { results }] = [...arg];
    state.voiceLiveNode = results[0];
  }
};

export default {
  state,
  actions,
  mutations
};

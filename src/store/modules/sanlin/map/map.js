import {
  getCurrentWSvideoList,
  getDeviceList,
  getScreens,
  connectVideo,
  disconnectVideo,
  lockvideo,
  streamBext, //视频轮询
  unlockvideo,
  readDevicesTags,
  connectRandomVideo,
  newConnectVideo
} from "@/store/api/sanlin/map/map";
import router from "@/router/index";
import {
  ADD_DEVICE_LIST,
  CONNECT_VIDEO,
  HIDE_VIDEO_VISIBLE,
  DIS_CONNECT_VIDEO,
  CLEAR_MARKERS,
  POLLING_CAMERA_LIST,
  SET_CURRENT_VIDEO_LIST,
  MAP_MOVED,
  GET_DEVICE_LIST,
  GET_CURRENT_WS_VIDEO_LIST,
  GET_SCREENS,
  CLEAR_CLUSTERER
} from "@/store/mutation-types.js";
import Vue from "vue";
import AMap from "AMap";
import { selectdeviceIcon } from "@/utils/deviceIcon";
import store from "@/store/index";
// import { param } from "jquery";
const state = {
  cluster: null,
  pollingVideoList: [],
  segList: [],
  screenList: [],
  deviceListPagination: {
    start: 0,
    count: 15000,
    page: 0,
    total: 0,
    props: "_sys_position,_sys_location,_sys_nickname,_sys_detectionType"
    // tags:'',
    // subscribed:true
  },
  zjd_0_0: "",
  zjd_1_0: "",
  zjd_2_0: "",
  zjd_0_1: "",
  zjd_1_1: "",
  zjd_2_1: "",
  zjd_0_2: "",
  zjd_1_2: "",
  zjd_2_2: "",
  leftScreenSrc: [],
  leftScreen1: "",
  leftScreen2: "",
  leftScreen3: "",
  rightScreen1: "",
  rightScreen2: "",
  rightScreen3: "",
  devicelist: [],
  fakeMarkerList: [],
  markerList: [],
  videoRtsp: {}, //当前播放的rtsp流
  videoVisible: false, //控制视频弹出层显示和隐藏
  currentUrn: "",
  cameraList: [],
  cameraListRight: [],
  cameraListLeft: [],
  lockList: [
    {
      locked: false,
      urn: ``,
      screenSpot: {
        coordinate: {
          x: 0,
          y: 0,
          z: 0
        },
        screen: {
          organization: "bh",
          group: "screen",
          id: "310115130058A"
        },
        // layout: "LEFT_TOP",
        segment: "ALL"
      },
      schedule: {
        lifeCycle: "TILL_CANCELLATION"
      }
    },
    {
      locked: false,
      urn: ``,
      screenSpot: {
        coordinate: {
          x: 0,
          y: 1,
          z: 0
        },
        screen: {
          organization: "bh",
          group: "screen",
          id: "310115130058A"
        },
        // layout: "LEFT_TOP",
        segment: "ALL"
      },
      schedule: {
        lifeCycle: "TILL_CANCELLATION"
      }
    },
    {
      locked: false,
      urn: ``,
      screenSpot: {
        coordinate: {
          x: 0,
          y: 2,
          z: 0
        },
        screen: {
          organization: "bh",
          group: "screen",
          id: "310115130058A"
        },
        // layout: "LEFT_TOP",
        segment: "ALL"
      },
      schedule: {
        lifeCycle: "TILL_CANCELLATION"
      }
    },
    {
      locked: false,
      urn: ``,
      screenSpot: {
        coordinate: {
          x: 1,
          y: 0,
          z: 0
        },
        screen: {
          organization: "bh",
          group: "screen",
          id: "310115130058A"
        },
        // layout: "LEFT_TOP",
        segment: "ALL"
      },
      schedule: {
        lifeCycle: "TILL_CANCELLATION"
      }
    },
    {
      locked: false,
      urn: ``,
      screenSpot: {
        coordinate: {
          x: 1,
          y: 1,
          z: 0
        },
        screen: {
          organization: "bh",
          group: "screen",
          id: "310115130058A"
        },
        // layout: "LEFT_TOP",
        segment: "ALL"
      },
      schedule: {
        lifeCycle: "TILL_CANCELLATION"
      }
    },
    {
      locked: false,
      urn: ``,
      screenSpot: {
        coordinate: {
          x: 1,
          y: 2,
          z: 0
        },
        screen: {
          organization: "bh",
          group: "screen",
          id: "310115130058A"
        },
        // layout: "LEFT_TOP",
        segment: "ALL"
      },
      schedule: {
        lifeCycle: "TILL_CANCELLATION"
      }
    }
  ],
  devicesTagsList: [],
  checkedTags: "",
  videoIdlist: [
    "c3a664f86f0e42abbcddeb04240475ad",
    "d9dd23d27359443f93cffa47c3859b9f",
    "7a78270220a940b18c52985dea1be2cf",
    "22fcd3f3d14d490f9b87fd9ad16af5a7",
    "2eb26f680d634e22ba82d538fda6bc2d",
    "9d9f2a5cbd5e4d03941a2cf3252bbf53",
    "4df47e7f9e9a4921a70ac657bc41e2dd",
    "b1793a918108418f8d85fb9d774cc049",
    "c857a0c99a36489fac2186aa1be7b338",
    "ab18bc9e081447fd8af8394bf2aeb78e",
    "8dc7b1fb6e884994b948ee3621b29556",
    "4e608128a2de45c6b5da8c611284e326",
    "95ac82ec31a34eb0bd98b82bd9a03300",
    "cc8711b2bc7949bbbc4b0a56ad73383b",
    "3d3b6de135f1402c911994031fc12e9a",
    "d733d1a8fa8242f4ac169230e9b91115",
    "33cfa2ab696e40a1ae8112819df145a3",
    "dbccb9dc8ebd4018abd2ad7e3b9ab207",
    "d208091f81ee4b6a97ab58c9122d8d0b",
    "b5ebf9cef3714b229e714754a9f4f36c",
    "e0126d8c59d644e19f9066154a68bf0a",
    "f44d54ae87a440b7a39a3adf1bf806c3",
    "4a1adf31cb3c475dabf60b14d83c9f1b",
    "bd093f2080b64535afb4bbe719f6d4fc",
    "e1ecfa6889cd437283a8db0821e182b1",
    "51a47ab38b4044639eec600d2c2091af",
    "421721c23a83431d87e1539fb76f24d5",
    "0c263c03209340c2acac66862d20a5e5",
    "c8cbdc6555c84b9c827c97dcfd9753bb",
    "76714be03a2d49359240d71a01cfdd49",
    "818816ae078549ebb987b0a0f0781c50",
    "b9513dee73d642818c20d599239e9e23",
    "573fde010130488cb626657a11551258",
    "067dee58f21843c696db33df10ccf204",
    "bf858db02bfe4b779aeccc4d7beb0cb0",
    "b2d18ac83fe845f4ac4b3d92caa70c0f",
    "799882c3071848f5aa40b4d5ab73ad39",
    "2889109ae6004e0a9b850e648b6589f1",
    "e97bb76c59734b1c9adf9f48ac5f5823",
    "789b2552fd5041bf9a793f1955e8c021",
    "b94ca0f83cdc4e2083234c4c6d5d82c9",
    "2f0a027956684ac1a57a253eedbd0291",
    "a4dbca4f12d9422894caf0e44fd7205d",
    "acfe6e4f26c8485f9386d8ea54966ff1",
    "4d942b28b66b4b3f91e90b9356d7538c",
    "a5f0b566be3d46ffb7372ccb0d44097b",
    "bae6ceb86ad4465d937b9d68f67e65fd",
    "807ec2482cdd49e1a8e377bba5c4bf06",
    "63f1c4e7cf6547dabcf79cd503623627",
    "420d49e206814591ac7a85a2bfe7c91d",
    "aa5987261a96432e8d9c4474fa782555",
    "d3885e9380dd40f0b03b223909b3d469",
    "e84a5385de60494b8ae40e3cb3af7f82",
    "6b951e2519464f86a3247ae76438d2d2",
    "2eb06ce743ce4280ad18add7cf7f8d90",
    "3da409aca8f14d509bc51b7f196ccc2c",
    "a6bd8eb7a30b44b0a92435a24237a3b7",
    "67a48a0b63294dc492ab145a646669ef",
    "a0bf2dcaf9434351b798534358c32b4a",
    "54284cf86adb418f8e71175e00ddb8b8",
    "9f3f4acf04744a0b9673170f320a44ed",
    "0140b7ec62474b9590231340da6ea981",
    "db3764294bba4bbdb3d447a8e8f3aeb0",
    "11528acf918f45fb92a00513b49ece14",
    "61e7573346164f30a8ff1d5f50378cad",
    "14d0a722326d4eb39b66157284b5cdb9",
    "eec7b9de33f749928e05cbdce123b733",
    "40da5a90a93a41e58b33785651e0c2cf",
    "e58f845f7d594523b24866f24d64bcf2",
    "4f2c015283964e809ad1edd48b9f6334",
    "4c194188313046deabbfd5c02e0e6c14",
    "685c43853a6f4e8ba9522d31203d9312",
    "8e9d654af9694e32bf0a0cb4676d5dbf",
    "1a164cdf0c10440ebdcb5dafea5328b6",
    "ed48eb71b56c46eeb2b2d2045100dd5b",
    "ed7fadf97501494a9a0352fa577d2907",
    "e15f3db2242c4861a2a222d529d47220",
    "eb37dc79b57f49a88bd719289f60c8a2",
    "053ea76662c049b693484ab493878a66",
    "0637b2724e944d43a7943f6fef6efc41",
    "0c6f6532df7e46f2ba987348da2162a5",
    "a9ee1fa183464f378a8f5569d80eefb6",
    "711b65e51ec14f91bf3ff7d29d402372",
    "2fc59d475364477f9927b63059e6ead6",
    "393fd16b5d714e159182201fa947ff48",
    "b3b3b12c48034b5c85bdf56ea6a21fd8",
    "837e058db05841ae9d29f3d11704f14e",
    "8f07aa98d04d495b911707c76b8958c7",
    "bdb0dd0363e44c6999c4338e21720eec",
    "34b2b4c7732f41b5a0342ab2e6710615",
    "1520b605a24e4ee196d096a607c15164",
    "136639f28fee4fef8aa63add17d53cc8",
    "2cdcb0242f2d429b94c6fc3573f6d502",
    "d32c6f5c48744db3bacada824751364c",
    "38077884b2b349c5a8453b03e13d2619",
    "63fb4aab37a348d9b2cbe87f15a009c7",
    "b005bf61bfb04d9d8f9ea930fc897bed",
    "ec7dfeb272f548cf9acabd9536b3a6d0",
    "ab8ba6dd227b40bf8d73a63b4c3fd1be"
  ],
  sx: 0,
  sy: 0,
  videoconcatparams: {},
  Cameralastpagecount: 100,
  videomarkerList: [],
  openInfowindow: []
};
const actions = {
  newConnectVideo({ commit }, data) {
    let p = newConnectVideo(data);
    p.then(res => {
      commit("NEW_CONNECT_VIDEO", res);
    });
  },
  getScreens({ commit }, data) {
    let p = getScreens(data);
    p.then(res => {
      commit("GET_SCREENS", res);
    });
  },
  getCurrentWSvideoList({ commit }, data) {
    let p = getCurrentWSvideoList(data);
    p.then(res => {
      commit("GET_CURRENT_WS_VIDEO_LIST", res);
    });
  },
  getDeviceList({ commit }, params) {
    let p = getDeviceList(params);
    p.then(res => {
      commit(GET_DEVICE_LIST, res);
    });
  },
  // 存贮所有场景数据
  getSignDeviceList({ commit }, [params, name]) {
    let p = getDeviceList(params);
    p.then(({ results }) => {
      commit;
      // 存在localstorage中，localstorage不能存贮Map对象
      let localObj = {};
      let obj = Vue.ls.get("signDevice");

      if (obj) {
        if (obj[name] === undefined) {
          obj[name] = results;
          Vue.ls.set("signDevice", obj);
        } else {
          delete obj[name];

          Vue.ls.set("signDevice", obj);
        }
      } else {
        localObj[params.tags] = results;
        Vue.ls.set("signDevice", localObj);
      }
    });
  },
  connectVideo({ commit }, params) {
    let p = connectVideo(params);
    p.then(res => {
      // commit(CONNECT_VIDEO, res);
    });
  },
  connectVideos({ commit }, params) {
    commit;
    let p = connectVideo(params);
    p.then(res => {
      res;
    });
  },
  connectRandomVideo({ commit }, params) {
    let p = connectRandomVideo(params);
    p.then(res => {
      if (params.action == "CONNECT") {
        commit("CONNECT_RANDOM_VIDEO", res);
      }
    });
  },
  disconnectVideo({ commit }, params) {
    commit;
    let p = disconnectVideo(params);
    p.then(res => {
      res;
    });
  },
  lockvideo({ commit }, params) {
    commit;
    let p = lockvideo(params);
    p.then(res => {
      if (res.errors.length == 0) {
        Vue.prototype.$message({
          type: "success",
          message: "锁定成功"
        });
        store.commit("HIDE_CONTROL_VISIABLE");
      }
    });
  },
  unlockvideo({ commit }, params) {
    commit;
    let p = unlockvideo(params);
    p.then(res => {
      if (res.errors.length == 0) {
        store.commit("HIDE_CONTROL_VISIABLE");
        Vue.prototype.$message({
          type: "success",
          message: "取消锁定成功"
        });
      }
    });
  },
  streamBext({ commit }) {
    let p = streamBext();
    p.then(res => {
      commit(POLLING_CAMERA_LIST, res.results);
    });
  },
  // 读取标签列表
  readDevicesTags({ commit }, data) {
    let p = readDevicesTags(data);
    p.then(res => {
      commit("READ_DEVICES_TAGS", res);
    });
  }
};

const mutations = {
  NEW_CONNECT_VIDEO(state, res) {
    let errors = res.errors;
    if (errors.length > 0) {
      alert("视频流拉取失败");
    } else {
      state.videoRtsp = res.results;
      state.videoVisible = true;
    }
  },
  [CLEAR_CLUSTERER](state) {
    state.cluster.clearMarkers();
  },
  [GET_SCREENS](...arg) {
    let [state, { results }] = [...arg];
    state.screenList = results;
    let screenUrn = sessionStorage.getItem("screen");
    if (screenUrn) {
      let screenId = screenUrn.split(":").pop();
      results.forEach(screen => {
        if (screen.urn.id == screenId) {
          state.segList = screen.descriptor.properties.segConfig;
        }
      });
    }
  },
  [GET_CURRENT_WS_VIDEO_LIST](...arg) {
    let [state, { results }] = [...arg];
    state.pollingVideoList = results;
  },
  [GET_DEVICE_LIST](...arg) {
    let [state, { pagination, results }] = [...arg];
    state;
    let path = router.currentRoute.fullPath;
    // state.Cameralastpagecount = pagination.count
    if (path.indexOf("lingang") !== -1) {
      store.commit("CLEAR_LINGANG_MARKER");
      state.videomarkerList = [];
    } else if (path.indexOf("sanlin") !== -1) {
      store.commit("CLEAR_LINGANG_MARKER");
      state.videomarkerList = [];
    }
    Object.assign(state.deviceListPagination, pagination);
    let styleObjectList = [
      // DAMAGED,维修状态的摄像头
      {
        url: selectdeviceIcon("INACTIVE"),
        anchor: new AMap.Pixel(0, 1),
        size: new AMap.Size(23, 34)
      },
      // GENERAL,
      {
        url: selectdeviceIcon("GENERAL"),
        anchor: new AMap.Pixel(0, 1),
        size: new AMap.Size(23, 34)
      },
      // VEHICLE,
      {
        url: selectdeviceIcon("VEHICLE"),
        anchor: new AMap.Pixel(0, 1),
        size: new AMap.Size(23, 34)
      },
      // VIOLATION,
      {
        url: selectdeviceIcon("VIOLATION"),
        anchor: new AMap.Pixel(0, 1),
        size: new AMap.Size(23, 34)
      },
      // FACIAL,
      {
        url: selectdeviceIcon("FACIAL"),
        anchor: new AMap.Pixel(0, 1),
        size: new AMap.Size(23, 34)
      }
      //
    ];
    var dataList = [];
    var markerList = [];
    var innerMarker;

    results.forEach(item => {
      // console.log(item)
      if (path.indexOf("videoAggregation") !== -1) {
        item.type = "videoAggregation";
      } else if (path.indexOf("lingang") !== -1) {
        item.type = "lingang";
      }
      if (item.properties?._sys_detectionType) {
        item;
      } else {
        item.properties._sys_detectionType = "CAMERAGROUP";
      }
      // console.log(item)
      var data = null;
      // 撒点逻辑
      if (item.status == "INACTIVE") {
        item;
      } else {
        if (item?.position?.longitude && item?.position?.longitude !== "null") {
          innerMarker = new AMap.Marker({
            position: new AMap.LngLat(
              item.position.longitude,
              item.position.latitude
            ),
            itemData: item,
            icon: selectdeviceIcon(item.properties._sys_detectionType),
            title: item.location
          });

          innerMarker.on("click", function(e) {
            let d = e.target.w.itemData;
            if (d.type == "videoAggregation") {
              // state.openInfowindow.push(d)
              store.commit("CONNECT_RANDOMVIDEO", d);
              // store.commit('SET_VIDEO_MP4_VISIBLE',true)
              // store.commit('SET_VIDEO_RANDOM_VISIBLE',true)
              // let index = Math.floor(Math.random()*99)
              // console.log($refs['videoAMap'])
              // const infoWindow = new AMap.InfoWindow({
              //     // 使用自定义窗体
              //     isCustom: true,
              //     // 只有当组件渲染完毕后，通过$el才能拿到原生的dom对象
              //     content: `<div>信息窗口</div>`,
              //     // 设置定位偏移量
              //     offset: new AMap.Pixel(-9, -60)
              // })

              // infoWindow.open(Vue.prototype.globalAMap, e.target.getPosition())
            } else if (d.type == "lingang") {
              store.commit("GET_LOCATION", d);
              store.commit("CAMERA_LINGANG_RANDOMVIDEO", d);
            } else {
              state.currentUrn = `urn:${d.urn.organization}:${d.urn.group}:${d.urn.id}`;
              // store.dispatch("connectVideo", [state.currentUrn]);
              store.dispatch("newConnectVideo", [state.currentUrn]);
            }
          });
          markerList.push(innerMarker);
          state.videomarkerList.push(innerMarker);
          switch (item.properties._sys_detectionType) {
            case "GENERAL":
              data = {
                lnglat: [item.position.longitude, item.position.latitude],
                name: item.location,
                id: item.urn.organization + item.urn.group + item.urn.id,
                style: 1,
                content: item
              };
              break;
            case "VEHICLE":
              data = {
                lnglat: [item.position.longitude, item.position.latitude],
                name: item.location,
                id: item.urn.organization + item.urn.group + item.urn.id,
                style: 2,
                content: item
              };
              break;
            case "VIOLATION":
              data = {
                lnglat: [item.position.longitude, item.position.latitude],
                name: item.location,
                id: item.urn.organization + item.urn.group + item.urn.id,
                style: 3,
                content: item
              };
              break;
            case "FACIAL":
              data = {
                lnglat: [item.position.longitude, item.position.latitude],
                name: item.location,
                id: item.urn.organization + item.urn.group + item.urn.id,
                style: 4,
                content: item
              };
              break;
            default:
              break;
          }
          dataList.push(data);
        } else {
          // 没有点位信息
          // console.log(item)
        }
      }
    });
    if (state.cluster) {
      store.commit(CLEAR_MARKERS);
    }
    try {
      Vue.prototype.globalAMap.plugin(["AMap.MarkerClusterer"], function() {
        let sts = [
          {
            url:
              "https://bh-frontend.oss-cn-shanghai.aliyuncs.com/videoaggregation/%E7%BB%84%206.png",
            size: new AMap.Size(40, 48),
            offset: new AMap.Pixel(-16, -16)
          },
          {
            url:
              "https://bh-frontend.oss-cn-shanghai.aliyuncs.com/videoaggregation/%E7%BB%84%206%20%E6%8B%B7%E8%B4%9D%204.png",
            size: new AMap.Size(40, 48),
            offset: new AMap.Pixel(-16, -16)
          },
          {
            url:
              "https://bh-frontend.oss-cn-shanghai.aliyuncs.com/videoaggregation/%E7%BB%84%206%20%E6%8B%B7%E8%B4%9D%205.png",
            size: new AMap.Size(40, 48),
            offset: new AMap.Pixel(-18, -18)
          },
          {
            url:
              "https://bh-frontend.oss-cn-shanghai.aliyuncs.com/videoaggregation/%E7%BB%84%206%20%E6%8B%B7%E8%B4%9D%206.png",
            size: new AMap.Size(40, 48),
            offset: new AMap.Pixel(-24, -24)
          }
        ];
        let yagnshi = {};
        if (path.indexOf("videoAggregation") !== -1) {
          yagnshi = {
            styles: sts,
            gridSize: 30
          };
        } else {
          yagnshi = {
            gridSize: 30
          };
        }
        state.cluster = new AMap.MarkerClusterer(
          Vue.prototype.globalAMap,
          state.videomarkerList,
          yagnshi
        );
        // 点击聚合点的回调
        state.cluster.on("click", function(e) {
          store.commit("SHOW_INFO_WINDOW", e);
        });
      });
    } catch (e) {
      Vue.prototype.$message({
        type: "error",
        message: "网络出错，请刷新重试"
      });
    }
    state.fakeMarkerList = markerList;
    // Vue.prototype.globalAMap.add(state.videomarkerList)
    styleObjectList;
  },
  CONNECT_RANDOMVIDEO(state, d) {
    state.currentUrn = `urn:${d.urn.organization}:${d.urn.group}:${d.urn.id}`;
    let videoId = `urn:${d.urn.organization}:${d.urn.group}:${d.urn.id}`;
    state.videoconcatparams = {
      action: "CONNECT",
      cameraId: [{ streamNodeUrn: videoId }],
      protocol: "hls",
      streamType: 0
    };
    store.dispatch("connectRandomVideo", state.videoconcatparams);
  },
  CAMERA_LIST_RANDOMVIDEO(state, res) {
    state.videoconcatparams = {
      action: "CONNECT",
      cameraId: [{ streamNodeUrn: res.category.id }],
      protocol: "hls",
      streamType: 0
    };
    store.dispatch("connectRandomVideo", state.videoconcatparams);
  },
  CAMERA_LINGANG_RANDOMVIDEO(state, d) {
    state.currentUrn = `urn:${d.urn.organization}:${d.urn.group}:${d.urn.id}`;
    state.videoconcatparams = {
      // action:'CONNECT',
      cameraId: d.urn.id,
      protocol: "hls",
      streamType: 0
    };
    store.dispatch("getLingangUrl", state.videoconcatparams);
  },
  [MAP_MOVED](state) {
    state;
    // let start = state.deviceListPagination.start + state.deviceListPagination.count
    // if (start <= state.deviceListPagination.total) {
    // Object.assign(state.deviceListPagination, {
    //     start: start,
    //     count: state.deviceListPagination.count
    // })
  },
  [SET_CURRENT_VIDEO_LIST](state, data) {
    state;
    let pollingIndex = -1;
    state.pollingVideoList.forEach((polling, index) => {
      if (
        polling.screenSpot.segment == data.screenSpot.segment &&
        polling.screenSpot.coordinate.x == data.screenSpot.coordinate.x &&
        polling.screenSpot.coordinate.y == data.screenSpot.coordinate.y
      ) {
        pollingIndex = index;
      }
    });
    if (pollingIndex > -1) {
      state.pollingVideoList.splice(pollingIndex, 1, data);
    }
    let currentPath = router.currentRoute.fullPath;
    // let zindex = -1;
    // let videoObj;
    // state.pollingVideoList.forEach((video, index) => {
    //   if (Vue.prototype.diffObj(video.screenSpot, data.screenSpot)) {
    //     zindex = index;
    //     videoObj = data;
    //   }
    // });
    // if (zindex > -1) {
    //   console.log("替换成功...");
    //   state.pollingVideoList.splice(zindex, 1, videoObj);
    //   state.pollingVideoList.push("1");
    //   state.pollingVideoList.pop();
    // } else {
    //   // console.table([data]);
    // }
    if (
      currentPath.indexOf("zhoujiadu") > -1 ||
      currentPath.indexOf("liexiongzuo") > -1
    ) {
      // if (data.screenSpot.coordinate.y == 0) {
      //   if (data.screenSpot.coordinate.x == 0) {
      //     state.zjd_0_0 = data.metadata.url;
      //     console.log("(0,0): " + state.zjd_0_0);
      //   }
      //   if (data.screenSpot.coordinate.x == 1) {
      //     state.zjd_1_0 = data.metadata.url;
      //     console.log("(1,0): " + state.zjd_1_0);
      //   }
      //   if (data.screenSpot.coordinate.x == 2) {
      //     state.zjd_2_0 = data.metadata.url;
      //     console.log("(2,0): " + state.zjd_2_0);
      //   }
      // }
      // if (data.screenSpot.coordinate.y == 1) {
      //   if (data.screenSpot.coordinate.x == 0) {
      //     state.zjd_0_1 = data.metadata.url;
      //     console.log("(0,1): " + state.zjd_0_1);
      //   }
      //   if (data.screenSpot.coordinate.x == 1) {
      //     state.zjd_1_1 = data.metadata.url;
      //     console.log("(1,1): " + state.zjd_1_1);
      //   }
      //   if (data.screenSpot.coordinate.x == 2) {
      //     state.zjd_2_1 = data.metadata.url;
      //     console.log("(2,1): " + state.zjd_2_1);
      //   }
      // }
      // if (data.screenSpot.coordinate.y == 2) {
      //   if (data.screenSpot.coordinate.x == 0) {
      //     state.zjd_0_2 = data.metadata.url;
      //     console.log("(0,2): " + state.zjd_2_1);
      //   }
      //   if (data.screenSpot.coordinate.x == 1) {
      //     state.zjd_1_2 = data.metadata.url;
      //     console.log("(1,2): " + state.zjd_2_1);
      //   }
      //   if (data.screenSpot.coordinate.x == 2) {
      //     state.zjd_2_2 = data.metadata.url;
      //     console.log("(2,2): " + state.zjd_2_1);
      //   }
      // }
    }
    // if (currentPath.indexOf("sanlin") > -1) {
    //   if (data.screenSpot.coordinate.x == 0) {
    //     if (data.screenSpot.coordinate.y == 0) {
    //       state.leftScreenSrc[0] = data.metadata.url;
    //       state.leftScreen1 = data.metadata.url;
    //       console.log("sanlin:(0,0): " + data.metadata.url);
    //     }
    //     if (data.screenSpot.coordinate.y == 1) {
    //       state.leftScreenSrc[1] = data.metadata.url;
    //       state.leftScreen2 = data.metadata.url;
    //       console.log("sanlin:(0,1): " + data.metadata.url);
    //     }
    //     if (data.screenSpot.coordinate.y == 2) {
    //       state.leftScreenSrc[2] = data.metadata.url;
    //       state.leftScreen3 = data.metadata.url;
    //       console.log("sanlin:(0,2): " + data.metadata.url);
    //     }
    //   }
    //   if (data.screenSpot.coordinate.x == 1) {
    //     if (data.screenSpot.coordinate.y == 0) {
    //       state.leftScreenSrc[0] = data.metadata.url;
    //       state.rightScreen1 = data.metadata.url;
    //     }
    //     if (data.screenSpot.coordinate.y == 1) {
    //       state.leftScreenSrc[1] = data.metadata.url;
    //       state.rightScreen2 = data.metadata.url;
    //       console.log("sanlin:(1,1): " + data.metadata.url);
    //     }
    //     if (data.screenSpot.coordinate.y == 2) {
    //       state.leftScreenSrc[2] = data.metadata.url;
    //       state.rightScreen3 = data.metadata.url;
    //       console.log("sanlin:(1,2): " + data.metadata.url);
    //     }
    //   }
    // }
    if (currentPath.indexOf("liexiongzuo") > -1) {
      // 猎熊座逻辑
      // if (data.screenSpot.coordinate.x == 0) {
      //   if (data.screenSpot.coordinate.y == 0) {
      //     state.leftScreenSrc[0] = data.metadata.url;
      //     state.leftScreen1 = data.metadata.url;
      //     console.log("lxz:(0,0): " + data.metadata.url);
      //   }
      //   if (data.screenSpot.coordinate.y == 1) {
      //     state.leftScreenSrc[1] = data.metadata.url;
      //     state.leftScreen2 = data.metadata.url;
      //     console.log("lxz:(0,1): " + data.metadata.url);
      //   }
      //   if (data.screenSpot.coordinate.y == 2) {
      //     state.leftScreenSrc[2] = data.metadata.url;
      //     state.leftScreen3 = data.metadata.url;
      //     console.log("lxz:(0,2): " + data.metadata.url);
      //   }
      // }
      // if (data.screenSpot.coordinate.x == 1) {
      //   if (data.screenSpot.coordinate.y == 0) {
      //     state.leftScreenSrc[0] = data.metadata.url;
      //     state.rightScreen1 = data.metadata.url;
      //     console.log("lxz:(1,0): " + data.metadata.url);
      //   }
      //   if (data.screenSpot.coordinate.y == 1) {
      //     state.leftScreenSrc[1] = data.metadata.url;
      //     state.rightScreen2 = data.metadata.url;
      //     console.log("lxz:(1,1): " + data.metadata.url);
      //   }
      //   if (data.screenSpot.coordinate.y == 2) {
      //     state.leftScreenSrc[2] = data.metadata.url;
      //     state.rightScreen3 = data.metadata.url;
      //     console.log("lxz:(1,2): " + data.metadata.url);
      //   }
      // }
    }
  },
  CHNAGE_LOCK(state, res) {
    state.lockList.forEach((item, index) => {
      if (
        item.screenSpot.coordinate.x == res.screenSpot.coordinate.x &&
        item.screenSpot.coordinate.y == res.screenSpot.coordinate.y
      ) {
        state.lockList[index] = res;
      }
    });
  },
  [POLLING_CAMERA_LIST](state, res) {
    state.cameraListRight = res.filter(item => {
      return item.screenSpot.coordinate.x == 1;
    });
    state.cameraListLeft = res.filter(item => {
      return item.screenSpot.coordinate.x == 0;
    });
  },
  SET_CAMERA() {
    state.cameraListRight = [
      {
        // streamNode:{
        //   urn:{
        //     group: "camera",
        //     id: "31011513013761",
        //     organization: "bh",
        //   }
        // },
        // metadata:{
        //     url:'rtmp://192.168.199.164:1935/live/n316017883'
        // }
      },
      {
        // streamNode:{
        //   urn:{
        //     group: "camera",
        //     id: "31011513013764",
        //     organization: "bh",
        //   }
        // },
        // metadata:{
        //     url:'rtmp://192.168.199.164:1935/live/n316017880'
        // }
      },
      {
        // streamNode:{
        //   urn:{
        //     group: "camera",
        //     id: "31011513013763",
        //     organization: "bh",
        //   }
        // },
        // metadata:{
        //     url:'rtmp://192.168.199.164:1935/live/n316017881'
        // }
      }
    ];
    state.cameraListLeft = [
      {
        // streamNode:{
        //   urn:{
        //     group: "camera",
        //     id: "31011513013771",
        //     organization: "bh",
        //   }
        // },
        // metadata:{
        //     url:'rtmp://192.168.199.164:1935/live/n316017852'
        // }
      },
      {
        // streamNode:{
        //   urn:{
        //     group: "camera",
        //     id: "31011513013754",
        //     organization: "bh",
        //   }
        // },
        // metadata:{
        //     url:'rtmp://192.168.199.164:1935/live/n316017911'
        // }
      },
      {
        // streamNode:{
        //   urn:{
        //     group: "camera",
        //     id: "31011513013770",
        //     organization: "bh",
        //   }
        // },
        // metadata:{
        //     url:'rtmp://192.168.199.164:1935/live/n316017853'
        // }
      }
    ];
  },
  // CHANGE_RTSP(state,res){
  //     state.cameraListRight[0].metadata.url = res.metadata.url
  //     localStorage.removeItem('upVideo')
  // },
  [ADD_DEVICE_LIST](state, res) {
    console.time("sort");

    // console.log(res)
    // var timer = null
    state.deviceListPagination = res.pagination;
    Object.assign(state.deviceListPagination, res.pagination);
    // var massMarks = null
    var styleObjectList = [];
    res.results.forEach((item, index) => {
      index;
      // let marker = null
      let styleObject = null;
      selectdeviceIcon;
      if (item.position && item.position.longitude && item.position.latitude) {
        // 维修的摄像头不进行撒点、
        if (item.status == "INACTIVE") {
          //维修的摄像头
          styleObject = {
            url: selectdeviceIcon(item.status),
            size: new AMap.Size(46, 68)
          };
          styleObjectList.push(styleObject);
          // marker = new AMap.Marker({ //item.position.longitude
          //     position: new AMap.LngLat(item.position.longitude, item.position.latitude), //
          //     // icon: new AMap.Icon({
          //     //     image: selectdeviceIcon(item.status),
          //     //     size: new AMap.Size(46, 68), //图标大小
          //     //     imageSize: new AMap.Size(23, 34)
          //     // }),
          //     extData: `urn:${item.urn.organization}:${item.urn.group}:${item.urn.id}`
          // })
        } else {
          //除了维修的摄像头
          if (item.properties._sys_detectionType) {
            // marker = new AMap.Marker({ //item.position.longitude
            //     position: new AMap.LngLat(item.position.longitude, item.position.latitude), //
            //     // icon: new AMap.Icon({
            //     //     image: selectdeviceIcon(item.properties._sys_detectionType),
            //     //     size: new AMap.Size(46, 68), //图标大小
            //     //     imageSize: new AMap.Size(23, 34)
            //     // }),
            //     extData: `urn:${item.urn.organization}:${item.urn.group}:${item.urn.id}`
            // })
            styleObject = {
              url: selectdeviceIcon(item.properties._sys_detectionType),
              size: new AMap.Size(46, 68)
            };
            styleObjectList.push(styleObject);
          } else {
            //        不考虑没有icon类型的情况
            console.log("no icon.");
          }
          if (item.status == "INACTIVE ") {
            alert("此点是维修设备,暂时无法播放");
          } else {
            // marker.on('click', (res) => {
            //     state.currentUrn = `urn:${item.urn.organization}:${item.urn.group}:${item.urn.id}`
            //     res;
            // })
            // marker.on('dblclick', () => { //点击坐标的双击事件
            //     clearTimeout(timer)
            //     // store.commit('SHOW_CONTROL_VISIABLE')
            // })
          }
          console.log(index);
          // Vue.prototype.globalAMap.add(marker)
          // state.markerList.push(marker)
        }
      }

      Vue.prototype.globalAMap.plugin(["AMap.MarkerClusterer"], function() {
        var cluster = new AMap.MarkerClusterer(
          Vue.prototype.globalAMap,
          markerList,
          {
            gridSize: 40,
            zoomOnClick: false
          }
        );
        // 点击聚合点的回调
        cluster.on("click", function(e) {
          if (Vue.prototype.globalAMap.getZoom() == 18) {
            let lnglatLocalList = e.markers;
            let obj = {
              position: lnglatLocalList[0].Ce.position,
              markers: lnglatLocalList
            };
            store.commit("SHOW_INFO_WINDOW", obj);
          } else {
            store.commit("ZOOM_IN_GLOBAL");
          }
        });
      });
    });
    // console.log(styleObjectList);
    console.timeEnd("sort");
  },
  [CLEAR_MARKERS](state) {
    if (state.cluster) {
      state.cluster.clearMarkers();
    }

    // Vue.prototype.globalAMap.remove(state.weikakouMarkerList)
    // state.weikakouMarkerList = []
  },
  [CONNECT_VIDEO](state, res) {
    let errors = res.errors;
    if (errors.length > 0) {
      alert("视频流拉取失败");
    } else {
      state.videoRtsp = res.results;
      state.videoVisible = true;
    }
  },
  CONNECT_RANDOM_VIDEO(state, res) {
    let errors = res.error;
    if (!errors == null) {
      alert("视频流拉取失败");
    } else {
      state.videoRtsp = res.results;
      console.log(res.results);
      state.videoVisible = true;
    }
  },
  CONNECT_LINGANG_VIDEO(state, res) {
    let errors = res.error;
    if (!errors == null) {
      alert("视频流拉取失败");
    } else {
      console.log(res);
      store.commit("CONTROL_DIALOG", true);
      store.commit("SET_DIALOG_NAME", "video");
      store.commit("SET_DIALOG_INFO", { id: 1, url: res.result.metadata.url });
      store.commit("SET_DIALOG_TITLE", "监控画面");
      // console.log(res)
      // state.videoRtsp = res.result;
      // state.videoVisible = true;
    }
  },
  [HIDE_VIDEO_VISIBLE](state) {
    store.dispatch("disconnectVideo", [state.currentUrn]);
    state.currentUrn = "";
    state.videoVisible = false;
    state.videoRtsp = "";
  },
  HIDE_VIDEO_RANDOM_VISIBLE(state) {
    state.videoVisible = false;
    state.videoRtsp = "";
  },
  DIS_CONNECT_VIDEO() {},
  READ_DEVICES_TAGS(state, res) {
    state.devicesTagsList = [];
    res.results.forEach(item => {
      state.devicesTagsList.push({
        signType: "info",
        name: item
      });
    });
    // console.log(state.devicesTagsList);
  },
  CHANGE_TAGS_TYPE(state, res) {
    if (state.devicesTagsList[res].signType == "info") {
      state.devicesTagsList[res].signType = "primary"; //变成选中状态
      state.checkedTags = state.devicesTagsList[res].name; //选中之后调用的接口参数
      Object.assign(state.deviceListPagination, {
        tags: state.devicesTagsList[res].name.value
      });
    } else {
      state.devicesTagsList[res].signType = "info";
      // 清除当前点击的场

      return;
    }
    let checked = state.devicesTagsList.filter(item => {
      return item.signType == "primary";
    });
    if (checked) {
      console.log("这里还有标签");
    }
    // console.log(state.deviceListPagination);
    // checked.forEach(item=>[
    //   state.checkedTags += (item.name+',')
    // ])

    // console.log(state.checkedTags)
    // if(state.checkedTags){
    //   state.deviceListPagination={
    //     start: 0,
    //     count: 5000,
    //     page: 0,
    //     total: 0,
    //     tags:state.checkedTags
    //   }
    //   // state.deviceListPagination.tags =
    // }else{
    //   state.deviceListPagination={
    //     start: 0,
    //     count: 5000,
    //     page: 0,
    //     total: 0,
    //   }
    // }
  },
  SET_SX(state, res) {
    state.sx = res;
  },
  SET_SY(state, res) {
    state.sy = res;
  }
};

export default {
  state,
  actions,
  mutations
};

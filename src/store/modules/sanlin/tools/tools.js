import {
  INIT_WEBSOCKET,
  INIT_AMAP,
  SHOW_INFO_WINDOW,
  ZOOM_IN_GLOBAL,
  ZOOM_OUT_GLOBAL,
  WEIXING_AMAP,
  HIDE_WEIXING_MAP,
  SHOW_BASE_MAP,
  SHOW_HEISE_MAP,
  SHOW_CONTROL_VISIABLE,
  HIDE_CONTROL_VISIABLE,
  INIT_PAGE
} from "@/store/mutation-types";
import store from "@/store/index";
import Vue from "vue";
import AMap from "AMap";
import AMapUI from "AMapUI";
import { WSconnect } from "@/utils/socket";
import { selectdeviceIcon } from "@/utils/deviceIcon";
import router from "@/router/index";

const state = {
  mapLoadingComplete: false,
  globalAMap: null,
  satelliteLayer: null,
  baseLayer: null,
  controlLRvisiable: false,
  cameraListleft: [],
  videoIdlist: [
    "c3a664f86f0e42abbcddeb04240475ad",
    "d9dd23d27359443f93cffa47c3859b9f",
    "7a78270220a940b18c52985dea1be2cf",
    "22fcd3f3d14d490f9b87fd9ad16af5a7",
    "2eb26f680d634e22ba82d538fda6bc2d",
    "9d9f2a5cbd5e4d03941a2cf3252bbf53",
    "4df47e7f9e9a4921a70ac657bc41e2dd",
    "b1793a918108418f8d85fb9d774cc049",
    "c857a0c99a36489fac2186aa1be7b338",
    "ab18bc9e081447fd8af8394bf2aeb78e",
    "8dc7b1fb6e884994b948ee3621b29556",
    "4e608128a2de45c6b5da8c611284e326",
    "95ac82ec31a34eb0bd98b82bd9a03300",
    "cc8711b2bc7949bbbc4b0a56ad73383b",
    "3d3b6de135f1402c911994031fc12e9a",
    "d733d1a8fa8242f4ac169230e9b91115",
    "33cfa2ab696e40a1ae8112819df145a3",
    "dbccb9dc8ebd4018abd2ad7e3b9ab207",
    "d208091f81ee4b6a97ab58c9122d8d0b",
    "b5ebf9cef3714b229e714754a9f4f36c",
    "e0126d8c59d644e19f9066154a68bf0a",
    "f44d54ae87a440b7a39a3adf1bf806c3",
    "4a1adf31cb3c475dabf60b14d83c9f1b",
    "bd093f2080b64535afb4bbe719f6d4fc",
    "e1ecfa6889cd437283a8db0821e182b1",
    "51a47ab38b4044639eec600d2c2091af",
    "421721c23a83431d87e1539fb76f24d5",
    "0c263c03209340c2acac66862d20a5e5",
    "c8cbdc6555c84b9c827c97dcfd9753bb",
    "76714be03a2d49359240d71a01cfdd49",
    "818816ae078549ebb987b0a0f0781c50",
    "b9513dee73d642818c20d599239e9e23",
    "573fde010130488cb626657a11551258",
    "067dee58f21843c696db33df10ccf204",
    "bf858db02bfe4b779aeccc4d7beb0cb0",
    "b2d18ac83fe845f4ac4b3d92caa70c0f",
    "799882c3071848f5aa40b4d5ab73ad39",
    "2889109ae6004e0a9b850e648b6589f1",
    "e97bb76c59734b1c9adf9f48ac5f5823",
    "789b2552fd5041bf9a793f1955e8c021",
    "b94ca0f83cdc4e2083234c4c6d5d82c9",
    "2f0a027956684ac1a57a253eedbd0291",
    "a4dbca4f12d9422894caf0e44fd7205d",
    "acfe6e4f26c8485f9386d8ea54966ff1",
    "4d942b28b66b4b3f91e90b9356d7538c",
    "a5f0b566be3d46ffb7372ccb0d44097b",
    "bae6ceb86ad4465d937b9d68f67e65fd",
    "807ec2482cdd49e1a8e377bba5c4bf06",
    "63f1c4e7cf6547dabcf79cd503623627",
    "420d49e206814591ac7a85a2bfe7c91d",
    "aa5987261a96432e8d9c4474fa782555",
    "d3885e9380dd40f0b03b223909b3d469",
    "e84a5385de60494b8ae40e3cb3af7f82",
    "6b951e2519464f86a3247ae76438d2d2",
    "2eb06ce743ce4280ad18add7cf7f8d90",
    "3da409aca8f14d509bc51b7f196ccc2c",
    "a6bd8eb7a30b44b0a92435a24237a3b7",
    "67a48a0b63294dc492ab145a646669ef",
    "a0bf2dcaf9434351b798534358c32b4a",
    "54284cf86adb418f8e71175e00ddb8b8",
    "9f3f4acf04744a0b9673170f320a44ed",
    "0140b7ec62474b9590231340da6ea981",
    "db3764294bba4bbdb3d447a8e8f3aeb0",
    "11528acf918f45fb92a00513b49ece14",
    "61e7573346164f30a8ff1d5f50378cad",
    "14d0a722326d4eb39b66157284b5cdb9",
    "eec7b9de33f749928e05cbdce123b733",
    "40da5a90a93a41e58b33785651e0c2cf",
    "e58f845f7d594523b24866f24d64bcf2",
    "4f2c015283964e809ad1edd48b9f6334",
    "4c194188313046deabbfd5c02e0e6c14",
    "685c43853a6f4e8ba9522d31203d9312",
    "8e9d654af9694e32bf0a0cb4676d5dbf",
    "1a164cdf0c10440ebdcb5dafea5328b6",
    "ed48eb71b56c46eeb2b2d2045100dd5b",
    "ed7fadf97501494a9a0352fa577d2907",
    "e15f3db2242c4861a2a222d529d47220",
    "eb37dc79b57f49a88bd719289f60c8a2",
    "053ea76662c049b693484ab493878a66",
    "0637b2724e944d43a7943f6fef6efc41",
    "0c6f6532df7e46f2ba987348da2162a5",
    "a9ee1fa183464f378a8f5569d80eefb6",
    "711b65e51ec14f91bf3ff7d29d402372",
    "2fc59d475364477f9927b63059e6ead6",
    "393fd16b5d714e159182201fa947ff48",
    "b3b3b12c48034b5c85bdf56ea6a21fd8",
    "837e058db05841ae9d29f3d11704f14e",
    "8f07aa98d04d495b911707c76b8958c7",
    "bdb0dd0363e44c6999c4338e21720eec",
    "34b2b4c7732f41b5a0342ab2e6710615",
    "1520b605a24e4ee196d096a607c15164",
    "136639f28fee4fef8aa63add17d53cc8",
    "2cdcb0242f2d429b94c6fc3573f6d502",
    "d32c6f5c48744db3bacada824751364c",
    "38077884b2b349c5a8453b03e13d2619",
    "63fb4aab37a348d9b2cbe87f15a009c7",
    "b005bf61bfb04d9d8f9ea930fc897bed",
    "ec7dfeb272f548cf9acabd9536b3a6d0",
    "ab8ba6dd227b40bf8d73a63b4c3fd1be"
  ]
};
const mutations = {
  [INIT_PAGE](state, PointSimplifier) {
    state;
    //创建组件实例
    var pointSimplifierIns = new PointSimplifier({
      map: Vue.prototype.globalAMap, //关联的map
      compareDataItem: function(a, b, aIndex, bIndex) {
        //数据源中靠后的元素优先，index大的排到前面去
        return aIndex > bIndex ? -1 : 1;
      },
      getPosition: function(dataItem) {
        //返回数据项的经纬度，AMap.LngLat实例或者经纬度数组
        return dataItem.position;
      },
      getHoverTitle: function(dataItem, idx) {
        //返回数据项的Title信息，鼠标hover时显示
        return "序号: " + idx;
      },
      renderOptions: {
        //点的样式
        pointStyle: {
          fillStyle: "blue" //蓝色填充
        }
      }
    });
    //仅作示意
    function createPoints(center, num) {
      var data = [];
      for (var i = 0, len = num; i < len; i++) {
        data.push({
          position: [
            center.getLng() + (Math.random() > 0.5 ? 1 : -1) * Math.random(),
            center.getLat() + (Math.random() > 0.5 ? 1 : -1) * Math.random()
          ]
        });
      }
      return data;
    }
    //随机创建一批点，仅作示意
    var data = createPoints(Vue.prototype.globalAMap.getCenter(), 100000);

    //设置数据源，data需要是一个数组
    pointSimplifierIns.setData(data);

    //监听事件
    pointSimplifierIns.on("pointClick pointMouseover pointMouseout", function(
      e,
      record
    ) {
      console.log(e.type, record);
    });
  },
  [INIT_WEBSOCKET](state, res) {
    state;
    res;
    WSconnect();
  },
  CAMERA_LEFT(state, res) {
    state.cameraListleft = res;
  },
  // 加载卫星地图
  [WEIXING_AMAP]() {
    Vue.prototype.globalAMap.add(state.satelliteLayer);
  },
  [HIDE_WEIXING_MAP]() {
    Vue.prototype.globalAMap.remove(state.satelliteLayer);
  },
  [SHOW_BASE_MAP]() {
    store.commit(HIDE_WEIXING_MAP);
    Vue.prototype.globalAMap.setMapStyle("");
  },
  [SHOW_HEISE_MAP]() {
    store.commit(HIDE_WEIXING_MAP);
    Vue.prototype.globalAMap.setMapStyle(
      "amap://styles/a2c5b896c39c2bfc85d98d1e8af8229f"
    );
    state.globalAMap.indoorMap.show();
  },
  // 初始化地图
  [INIT_AMAP]() {
    state.satelliteLayer = new AMap.TileLayer.Satellite();
    let route = router.currentRoute;
    let path = route.fullPath;
    let mapCenter = null;
    let mapStyle = null;
    // "amap://styles/a2c5b896c39c2bfc85d98d1e8af8229f"
    if (path.indexOf("sanlin") > -1) {
      mapCenter = [121.511594, 31.143172];
      mapStyle = "amap://styles/70534310bb6607653ebcd3a08a693e7b";
    } else if (path.indexOf("zhoujiadu") > -1) {
      mapCenter = [121.51394426822662, 31.174530545333983];
      mapStyle = "amap://styles/a2c5b896c39c2bfc85d98d1e8af8229f";
    } else if (path.indexOf("liexiongzuo") > -1) {
      mapCenter = [121.575186, 31.090246];
      mapStyle = "amap://styles/a2c5b896c39c2bfc85d98d1e8af8229f";
    } else if (path.indexOf("zhuqiao") > -1) {
      mapCenter = [121.759256, 31.103012];
      mapStyle = "amap://styles/a2c5b896c39c2bfc85d98d1e8af8229f";
    }else if (path.indexOf("origin") > -1) {
      mapCenter = [121.759256, 31.103012];
      mapStyle = "amap://styles/70534310bb6607653ebcd3a08a693e7b";
    } else {
      mapStyle = "amap://styles/55c955c7059fb5ccc4d407f7e755dd4d";
    }
    // AMapUI.loadUI(["control/BasicControl"], function(BasicControl) {
    state.globalAMap = new AMap.Map("map-container", {
      mapStyle: mapStyle, //设置地图的显示样式
      zoom: 18, //级别
      // center: [121.569542, 31.114507], //中心点坐标(万达)
      center: mapCenter, //中心点坐标
      // pitch: 75, // 地图俯仰角度，有效范围 0 度- 83 度
      viewMode: "3D", //使用3D视图,
      resizeEnable: true
    });
    var markerList = [];
    Vue.prototype.globalAMap = state.globalAMap;
    // console.log(Vue.prototype.globalAMap);
    Vue.prototype.globalAMap.on("complete", function() {
      console.log("complete...");
      state.mapLoadingComplete = true;
    });
    Vue.prototype.globalAMap.on("zoomchange", () => {
      // console.log('缩放')
    });
    Vue.prototype.globalAMap.on("dragging", () => {
      // console.log('拖拽')
    });
    state.globalAMap.plugin(["AMap.MarkerClusterer"], function() {
      var cluster = new AMap.MarkerClusterer(state.globalAMap, markerList, {
        gridSize: 20,
        zoomOnClick: false
      });
      // 点击聚合点的回调;
      cluster.on("click", function(e) {
        let lnglatLocalList = e.markers;
        let obj = {
          position: lnglatLocalList[0].Ce.position,
          markers: lnglatLocalList
        };
        store.commit("SHOW_INFO_WINDOW", obj);
      });
    });
    // setTimeout(() => {
    //   state.basicControlZoom = new BasicControl.Zoom({
    //     position: {
    //       top: "-100px",
    //       right: "-100px"
    //     }
    //   });
    //   state.basicControlZoom.addTo(state.globalAMap);
    // }, 2000);
    // });
  },
  POLY_GON() {
    console.log(".......");
    let userInfo = Vue.ls.get("userInfo");
    let res = JSON.parse(userInfo);
    const arr = res.pointArr.split(";");
    let path = [];
    console.log(arr);
    arr.forEach(item => {
      let num = [],
        nums = [];
      num = item.split(",");
      nums = num.map(a => {
        return Number.parseFloat(a);
      });
      path.push(nums);
    });

    var polygon = new AMap.Polygon({
      path: path,
      strokeColor: "#01de81",
      strokeWeight: 4,
      strokeOpacity: 1,
      fillOpacity: 0,
      fillColor: "#ccc",
      zIndex: 50
    });

    Vue.prototype.globalAMap.add(polygon);
    // 缩放地图到合适的视野级别
    Vue.prototype.globalAMap.setFitView([polygon]);
    // Vue.prototype.globalAMap.setZoomAndCenter(18, path[0]);
  },
  POLY_VIDEO_GON(state, item) {
    state;
    var polygon = new AMap.Polygon({
      path: item,
      strokeColor: "#17E7E5",
      strokeWeight: 4,
      strokeOpacity: 1,
      fillOpacity: 0.1,
      fillColor: "#ccc",
      zIndex: 50
    });

    Vue.prototype.globalAMap.add(polygon);
    // 缩放地图到合适的视野级别
    Vue.prototype.globalAMap.setFitView([polygon]);
  },
  [ZOOM_OUT_GLOBAL](state) {
    state.basicControlZoom.zoomOut();
  },
  [ZOOM_IN_GLOBAL](state) {
    state.basicControlZoom.zoomIn();
  },
  [SHOW_INFO_WINDOW](state, data) {
    // let that = this
    const contents = Vue.extend({
      template: `
      <div class='infoBox' :style='limitCss'>
      <ul>
      <li v-for="item in markerList"><img @click="handleConnectVideo(item)" :src="item|getImgUrl"></img><span style='color:#00edfb'>{{item.w.itemData.location || item.w.itemData.nickname.split("_")[0]}}</span></li>
      </ul>
      </div>
            `,
      data() {
        return {
          markerList: data.markers,
          limitCss: {
            "max-height": "300px"
          }
        };
      },
      mounted() {},
      filters: {
        getImgUrl: function(val) {
          let d = val.w.itemData;
          console.log(val);
          return selectdeviceIcon(d.properties._sys_detectionType);
        }
      },
      methods: {
        handleConnectVideo(item) {
          console.log(item);
          let d = item.w.itemData;
          console.log(d);
          // let path = router.currentRoute.fullPath;
          if (d.type == "videoAggregation") {
            // store.commit('SET_VIDEO_MP4_VISIBLE',true)
            // let index = Math.floor(Math.random()*99)
            // let videoId = state.videoIdlist[index]
            // let params = {
            //   cameraId:videoId,
            //   protocol:'hls',
            //   streamType:0
            // }
            // store.dispatch('connectRandomVideo',params)
            store.commit("CONNECT_RANDOMVIDEO", d);
          } else if (d.type == "lingang") {
            // console.log(d);
            store.commit("GET_LOCATION", d);
            store.commit("CAMERA_LINGANG_RANDOMVIDEO", d);
          } else {
            var currentUrn = `urn:${d.urn.organization}:${d.urn.group}:${d.urn.id}`;
            // store.dispatch("connectVideo", [currentUrn]);
            store.dispatch("newConnectVideo", [currentUrn]);
          }
        }
      },
      computed: {
        url: function() {
          return "http://baidu.com";
        }
      }
    });
    const con = new contents().$mount();
    // const contents = `
    //     <div>
    //         ${data.markers.map(item=>`
    //             <div onclick="${openvideo()}">
    //                 <img src="${item.Ce.icon.w.image}">
    //             </div>
    //         `).join(' ')}
    //     </div>
    // `
    var infoWindow = new AMap.InfoWindow({
      // isCustom: true,
      content: con.$el,
      position: data.position,
      offset: new AMap.Pixel(0, -20)
    });
    infoWindow.open(state.globalAMap, data.markers[0].getPosition());
  },

  CREATE_POINT_DIV() {},
  [SHOW_CONTROL_VISIABLE]() {
    state.controlLRvisiable = true;
  },
  [HIDE_CONTROL_VISIABLE]() {
    state.controlLRvisiable = false;
  }
};

export default {
  state,
  mutations
};

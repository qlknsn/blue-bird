import {
    // SET_YAOSU_VISIBLE,
    // SET_VIDEO_POPUP_VISIBLE,
    // SET_VIDEO_CONTAINER_VISIBLE,
    // SET_BASIC_DATA_FACILITY,
    // SET_BASIC_DATA_COMPANY,
    // SET_BASIC_DATA_PEOPLE_VISIBLE,
    // SET_BASIC_DATA_VILLAGE_POPUP,
    // SET_BASIC_DATA_VISIBLE,
    // SET_BASIC_DATA_CUNMINZU,
    // SET_GUARD_POWER_VISIBLE,
} from '@/store/mutation-types'
// import store from '@/store/index'
const state = {
    basicDataVisible: true,
    guardPowerVisible: false,
    basicDataVillagePopupVisible: false,
    basicDataPeopleVisible: false,
    basicDataCompanyVisible: false,
    basicDataFacilityVisible: false,
    videoContainerVisible: true,
    videoPopupVisible: false,
    yaosuVisible: false,
    basicDataCunminzuVisible: false,
    dangyuanVisible: false,
    zhongdianVisible: false,
    guanaiVisible: false,
    minsuVisible: false,
    guanlipowerVisible: false,
    jinrirenwuVisible:false,//今日任务图例
    duiwuVisible:false,//队伍的图例
    zizhiVisible:false,//自治队伍
    liangweichengyuanVisible:false,//两委成员
    zizhililiangVisible:false,//自治力量左上角
    xietongliliangVisible:false,//协同力量左上角
    qushituPopVisible:false,
    checkStateVisible:false,
    cyclePointVisible:false,
    garbageRankVisible:false
}
const actions = {}
const mutations = {
    clear4typevisible(state) {
        state.dangyuanVisible = false
        state.zhongdianVisible = false
        state.guanaiVisible = false
        state.minsuVisible = false
        state.guanlipowerVisible = false
        state.jinrirenwuVisible = false
        state.duiwuVisible = false
        state.zizhiVisible = false
        state.liangweichengyuanVisible=false
        state.zizhililiangVisible=false
        state.xietongliliangVisible = false
    },
    setdangyuanvisible(state, data) {

        state.dangyuanVisible = data
    },
    setzhongdianvisible(state, data) {
        state.zhongdianVisible = data
    },
    setguanaivisible(state, data) {

        state.guanaiVisible = data
    },
    // settaskvisible(state,data){
    //     state.guanlipowerVisible =  data
    // },
    setminsuvisible(state, data) {
        // store.commit('clear4typevisible')
        state.minsuVisible = data
    },
    setjinrirenwuVisible(state,data){
        state.jinrirenwuVisible = data
    },
    setduiwuVisible(state,data){

        state.duiwuVisible = data
    },
    setzizhiVisible(state,data){

        state.zizhiVisible = data
    },
    setliangweichengyuanVisible(state,data){
        state.liangweichengyuanVisible = data
    },
    setzizhililiangVisible(state,data){
        state.zizhililiangVisible = data
    },
    setxietongliliangVisible(state,data){
        state.xietongliliangVisible = data
    },
    // [SET_BASIC_DATA_CUNMINZU](state, data) {
    //     state.basicDataCunminzuVisible = data
    // },
    // [SET_YAOSU_VISIBLE](state, data) {
    //     state.yaosuVisible = data
    // },
    // [SET_VIDEO_POPUP_VISIBLE](state, data) {
    //     state.videoPopupVisible = data
    // },
    // [SET_BASIC_DATA_PEOPLE_VISIBLE](state, data) {
    //     state.basicDataPeopleVisible = data
    // },
    // [SET_BASIC_DATA_VISIBLE](state, data) {
    //     state.basicDataVisible = data
    // },
    // [SET_GUARD_POWER_VISIBLE](state, data) {
    //     state.guardPowerVisible = data
    // },
    // [SET_BASIC_DATA_VILLAGE_POPUP](state, data) {
    //     state.basicDataVillagePopupVisible = data
    // },
    // [SET_BASIC_DATA_COMPANY](state, data) {
    //     state.basicDataCompanyVisible = data
    // },
    // [SET_BASIC_DATA_FACILITY](state, data) {
    //     state.basicDataFacilityVisible = data
    // },
    // [SET_VIDEO_CONTAINER_VISIBLE](state, data) {
    //     state.videoContainerVisible = data
    // },
    SET_QUSHITU_POP_VISIBLE(state,res){
        state.qushituPopVisible = res;
    },
    CHECK_STATE_VISIBLE(state,res){
        state.checkStateVisible = res
    },
    CYCLE_POINT_VISIBLE(state,res){
        state.cyclePointVisible = res
    },
    GARBAGE_RANK_VISIBLE(state,res){
        state.garbageRankVisible = res
    },
}


export default {
    state,
    actions,
    mutations
}

import {
    getPointCount,
    getWeizhangPic,
    getWeizhangRecord,
    getLeftMiddleInfoV3,
    getRenqunMiduList,
    getline,
    getBar,
    getgarbagePie,
    noiseList,
    noiseRecordList,
    getrecycledList,
    getrecycledCount,
    getgarbageRank,
    qushiTu,
    garbageLPlateList,
    garbagePlateCount,
    garbageRecycledList,
    nosieLine,
    gongdanfenxiPieChart,
    garbageStatic,
    baseData,
    getPeopleLineData,
    getnNoStandart
} from '../../../api/sanlin/sanlinerqiV3/sanlinerqiV3'

const state = {
    weizhangPointList: [],
    popContainerVisible: false,
    renkoumiduTableVisible: false,
    nostandardVisible:false,
    carDetail: {},
    leftMiddleInfo: {},
    populationEntities: [],
    fixedArr: [],
    weizhangPicList: [],
    peopleDensityInfo: {},
    qushituItem:{},
    weizhangRecordList: [],
    noiseRecordList:[],
    recyclegarbageParams: {
        count: 10,
        start: 0
    },
    recycleCount:{},
    garbageRankparams:{
        limit:5,
        period:3,
        type:1
    },
    garbagePlatelist:{
        results:[],
        pagination:{}
    },
    garbagePlateParams:{
        community:'',
        count:4,
        start:0
    },
    garbageplateCount:{result:{}},
    garbageRecycleList:{results:[],pagination:{}},
    noiseTimeSelect:2,
    garbageStatic:{
        results:[
            {name:'',value:''},
            {name:'',value:''},
            {name:'',value:''},
            {name:'',value:''},
        ]
    },
    peopleMiduPopVisible: false,
    peopleMiduData: {},
    peopleLineData: {
        series: [],
        xAxis: []
    },
    baseData:{
        building: 590,
        community: 170,
        culture: "1+6+70",
        externalPeople: 139822,
        family: 121393,
        fitness: 182,
        healthCenter: 3,
        intangibleProject: 12,
        localPeople: 164899,
        nursingHome: 5,
        party: 2051,
        partyOrganization: 163,
        people: 369690,
        school: 48,
        totalArea: 34.19
    },
    getnNoStandart:{},
    nostandardType:"",
    noiseLists:[]
}
const actions = {
    getPeopleLineDataV3({commit}, params) {
        let p = getPeopleLineData(params)
        p.then(res=>{
            commit('GAT_PEOPLE_DATALIST',res)
        })
    },
    garbageStatic({ commit }, params){
        commit;params;
        let p = garbageStatic(params)
        p.then(res => {
            commit('GET_STATIC_CONFIG_LIST', res)
        })
    },
    garbageRecycledList({ commit }, params){
        commit;params;
        let p = garbageRecycledList(params)
        p.then(res => {
            commit('GET_GARBAGERECYCLE_LIST', res)
        })
    },
    garbagePlateCount({ commit }, params){
        commit;params;
        let p = garbagePlateCount(params)
        p.then(res => {
            commit('GET_GARBAGEPLATE_COUNT', res)
        })
    },
    garbageLPlateList({ commit }, params){
        commit;params;
        let p = garbageLPlateList(params)
        p.then(res => {
            commit('GET_GARBAGEPLATE_LIST', res)
        })
    },
    getline({ commit }, params) {
        let p = getline(params);
        return p;
    },
    getPointCount({commit},params){
        let p = getPointCount(params)
        p.then(res => {
            commit('SET_WEIZAHNG_POINTLIST', res)
        })
    },
    getWeizhangPic({commit}, params) {
        let p = getWeizhangPic(params)
        p.then(res => {
            commit('SET_WEIXZHANG_PICLIST', res)
        })
    },
    getWeizhangRecord({commit},params){
        let p = getWeizhangRecord(params)
        p.then(res => {
            commit('SET_WEIXZHANG_RECORD', res)
        })
    },
    getLeftMiddleInfoV3({ commit }, params) {
        let p = getLeftMiddleInfoV3(params);
        p.then(res => {
            commit("GETLEFTMIDDLEINFOV3", res);
        });
    },
    getRenqunMiduListV3({ commit }, params) {
        let p = getRenqunMiduList(params);
        p.then(res => {
            commit("GETRENQUNMIDULIST", res);
            commit("ZAOYINTABLEVISIBLE", false);
            commit("WEIKAKOUTABLEVISIBLE", false);
            commit("RENKOUMIDUTABLEVISIBLE", true);
        });
    },
    getgarbagePie({commit},params){
        commit;
        let p = getgarbagePie(params);
        return p;
    },
    getBar({ commit }, params) {
        let p = getBar(params);
        return p;
    },
    getrecycledList({ commit }, params) {
        commit;
        let p = getrecycledList(params);
        p.then(res => {
            res;
            // console.log(res)
        })
    },
    getrecycledCount({ commit }, params){
        commit;
        let p = getrecycledCount(params);
        p.then(res => {
            res;
            commit('GET_RECYCLE_COUNT',res.result)
            // console.log(res)
        })
    },
    getgarbageRank({ commit }, params){
        commit;
        let p = getgarbageRank(params);
        return p;
        // p.then(res => {
        //     res;
        //     // commit('GET_RECYCLE_COUNT',res.result)
        // })
    },
    qushiTu({commit},params){
        let p = qushiTu(params);
        return p;
    },
    noiseList({commit},params){
        let p = noiseList(params);
        return p;
    },
    getnoiseList({commit},params){
        let p = noiseList(params);
        p.then(res=>{
            console.log(res)
            commit('GET_NOISE_LIST',res)
        })
    },
    noiseRecordList({commit},params){
        let p = noiseRecordList(params);
        p.then(res =>{
            commit("NOISE_RECORD_LIST",res);
        })
    },
    nosieLine({commit},params){
        let p = nosieLine(params);
        return p;
        // p.then(res =>{
        //     commit("NOISE_RECORD_LIST",res);
        // })
    },
    gongdanfenxiPieChart({commit},params){    //工单分析板块饼图
        let p = gongdanfenxiPieChart(params);
        return p;
        // p.then(res =>{
        //     commit("NOISE_RECORD_LIST",res);
        // })
    },
    baseData({commit},params){
        let p = baseData(params);
        return p;
    },
    getnNoStandart({commit},params){
        let p = getnNoStandart(params);
        p.then(res=>{
            commit("NO_STANDART_LIST",res);
        })
        return p;
    }
}

const mutations = {
    GET_NOISE_LIST(state,res){
        state.noiseLists = res.results
    },
    NO_STANDART_LIST(state,res){
        state.getnNoStandart = res;
    },
    GAT_PEOPLE_DATALIST (state, res) {
        state.peopleLineData = res.result
    },
    setPeopleMiduPopVisibleV3(state, peopleData) {
        state.peopleMiduPopVisible = peopleData.isShow
        state.peopleMiduData = peopleData.data
    },
    GET_RECYCLE_COUNT(state,res){
        state.recycleCount = res
    },
    GETLEFTMIDDLEINFOV3(state,res){
        state.leftMiddleInfo = res.result
        state.fixedArr = res.result.fixed
        state.populationEntities = res.result.populationEntities
    },
    RENKOUMIDUTABLEVISIBLEV3(state, res) {
        state.renkoumiduTableVisible = res;
    },
    SET_WEIZAHNG_POINTLIST(state, res){
        state.weizhangPointList = res.results
    },
    CHANGEPOPvISIBLEV3(state, res) {
        state.popContainerVisible = res;
    },
    GETCARDETAILV3(state, res) {
        state.carDetail = res;
    },
    SET_WEIXZHANG_PICLIST(state, res) {
        state.weizhangPicList = res.result.slice(0,2)
    },
    SET_WEIXZHANG_RECORD(state, res) {
        state.weizhangRecordList = res.results
    },
    // SET_PEOPLEDENSITY_LIST(state,res) {
    //     state.peopleDensityInfo = res.result
    // },
    QUSHITU_ITEM(state,res){
        state.qushituItem = res;
    },
    GET_GARBAGEPLATE_LIST(state,res){
        state.garbagePlatelist = res
    },
    GET_GARBAGEPLATE_COUNT(state,res){
        state.garbageplateCount = res
    },
    GET_GARBAGERECYCLE_LIST(state,res){
        state.garbageRecycleList = res
    },
    NOISE_RECORD_LIST(state,res){
        state.noiseRecordList = res.results;
    },
    NOISE_TIME_SELECT(state,res){
        state.noiseTimeSelect = res;
    },
    GET_STATIC_CONFIG_LIST(state,res){
        state.garbageStatic = res
    },
    BASE_DATA(state,res){
        state.baseData = res.result;
    },
    NO_STANDARD(state,res){
        state.nostandardVisible = res;
    },
    SRAND_TYPE(state,res){
        state.nostandardType = res;
    }
}

export default {
    state, actions, mutations
}

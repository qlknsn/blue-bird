import Vue from "vue";
import store from "@/store/index";
import { selectdeviceIcon } from "@/utils/deviceIcon";
const state = {
  marker: null, //撒点实例
  cluster: null, //聚合点实例
  markerList: [], //撒点组合
  deviceInfoVisible: false, //景点详情显示属性
  warningtaskVisible: false, //告警列表显示属性
  warningDetail: {
    task: {
      alarmTimeFmt: ""
    }
  }, //告警详情
  warningDetailVisible: false, //告警详情显示属性
  cameraboxVisible: false, //视频框显示属性
  cameraSingle: { hlsUrl: "" }, //视频框显示属性
  warningtableTitle: ""
};
const actions = {};
const mutations = {
  CHANGE_CAMERABOX_VISIBLE(state, res) {
    state.cameraboxVisible = res;
  },
  CHANGE_WARNINGDETAIL_VISIBLE(state, res) {
    state.warningDetailVisible = res;
  },
  GET_WARNING_DETAIL(state, res) {
    state.warningDetail = res;
  },
  CHANGE_WARNINGTASK_VISIBLE(state, res) {
    state.warningtaskVisible = res;
  },
  CHANGE_DEVICEINFO(state, res) {
    state.deviceInfoVisible = res;
  },
  CLEAR_OLDSTREET_MARKER(state) {
    console.log("清除撒点");
    if (state.marker) {
      Vue.prototype.globalAMap.remove(state.marker);
      state.marker = null;
    }
    if (state.markerList.length > 0) {
      console.log(state.markerList);
      state.cluster.clearMarkers();
      Vue.prototype.globalAMap.remove(state.markerList);
      state.markerList = [];
    }
  },
  ADD_OLDSTREET_MARKER(state, res) {
    // 清除撒點
    store.commit("CLEAR_OLDSTREET_MARKER");
    if (res.length > 0) {
      Vue.prototype.globalAMap.setZoomAndCenter(14, [
        res[0].longitude,
        res[0].latitude
      ]);
      res.forEach(item => {
        if (item.longitude && item.latitude) {
          let marker = new AMap.Marker({
            position: new AMap.LngLat(item.longitude, item.latitude),
            snippet: item,
            icon: selectdeviceIcon(item.popType),
            itemData: item
          });
          marker.on("click", e => {
            switch (e.target.w.snippet.type) {
              case "DEVICE":
                if (
                  e.target.w.snippet.popType == "SHUAN_POINT" ||
                  e.target.w.snippet.popType == "ZHAN_POINT"
                ) {
                  //判断若是微型消防站或者消防栓,则地图点位不可点击,无需弹框
                  e;
                } else {
                  store.commit("CHANGE_DEVICEINFO", true);
                  store.dispatch("getOtherScenicOne", {
                    id: e.target.w.snippet.id,
                    type: e.target.w.snippet.ponitType
                  });
                }

                break;
              case "WARNING":
                store.commit("CHANGE_WARNINGTASK_VISIBLE", false);

                setTimeout(() => {
                  console.log(e.target.w.snippet);
                  state.warningtableTitle = e.target.w.snippet.list[0].typeStr;
                  state.warningtaskList = e.target.w.snippet.list;
                  store.commit("CHANGE_WARNINGTASK_VISIBLE", true);
                }, 500);

                break;
              case "CAMERA": {
                let d = e.target.w.snippet;
                store.dispatch("connectStream", { id: d.id });
                state.cameraSingle = d;
                store.commit("CHANGE_CAMERABOX_VISIBLE", true);
                break;
              }
            }

            //   store.commit(e.target.w.snippet.popType,true)
          });
          state.markerList.push(marker);
          Vue.prototype.globalAMap.add(state.markerList);
        }
      });
      Vue.prototype.globalAMap.plugin(["AMap.MarkerClusterer"], function() {
        state.cluster = new AMap.MarkerClusterer(
          Vue.prototype.globalAMap,
          state.markerList,
          {
            gridSize: 80,
            zoomOnClick: false
          }
        );
        // 点击聚合点的回调
        state.cluster.on("click", function(e) {
          //             if (Vue.prototype.globalAMap.getZoom() == 18) {
          let lnglatLocalList = e.markers;
          console.log(e.markers);
          let obj = {
            position: lnglatLocalList[0].Ce.position,
            markers: lnglatLocalList
          };
          console.log(obj);
          store.commit("SHOW_OLDSTREET_INFO_WINDOW", obj);
        });
      });
      // store.commit('CLEAR_MAP')
    }
  },
  SHOW_OLDSTREET_INFO_WINDOW(state, data) {
    // let that = this
    const contents = Vue.extend({
      template: `
          <div class='infoBox' style='max-height:300px'>
          <ul>
          <li v-for="item in markerList" @click="handleConnectVideo(item)"  style='cursor:pointer'><img :src="item|getImgUrl"></img><span style='color:white;'>{{item.w.itemData.address}}</span></li>
          </ul>
          </div>
                `,
      data() {
        return {
          markerList: data.markers
        };
      },
      mounted() {},
      filters: {
        getImgUrl: function(val) {
          let d = val.w.itemData;
          console.log(d);
          return selectdeviceIcon(d.popType);
        }
      },
      methods: {
        handleConnectVideo(e) {
          console.log(e);
          console.log(e.w.itemData);
          switch (e.w.itemData.type) {
            case "DEVICE":
              if (
                e.w.itemData.popType == "SHUAN_POINT" ||
                e.w.itemData.popType == "ZHAN_POINT"
              ) {
                //判断若是微型消防站或者消防栓,则地图点位不可点击,无需弹框
                e;
              } else {
                store.commit("CHANGE_DEVICEINFO", true);
                store.dispatch("getOtherScenicOne", {
                  id: e.w.itemData.id,
                  type: e.w.itemData.ponitType
                });
              }
              break;
            case "WARNING":
              store.commit("CHANGE_WARNINGTASK_VISIBLE", false);
              setTimeout(() => {
                state.warningtaskList = e.w.itemData.list;
                store.commit("CHANGE_WARNINGTASK_VISIBLE", true);
              }, 500);
              break;
            case "CAMERA": {
              let d = e.w.itemData;
              store.dispatch("connectStream", { id: d.id });
              state.cameraSingle = d;
              store.commit("CHANGE_CAMERABOX_VISIBLE", true);
              break;
            }
          }
        }
      },
      computed: {
        // url: function() {
        //   return "http://baidu.com";
        // }
      }
    });
    const con = new contents().$mount();
    // const contents = `
    //     <div>
    //         ${data.markers.map(item=>`
    //             <div onclick="${openvideo()}">
    //                 <img src="${item.Ce.icon.w.image}">
    //             </div>
    //         `).join(' ')}
    //     </div>
    // `
    var infoWindow = new AMap.InfoWindow({
      // isCustom: true,
      content: con.$el,
      position: data.position,
      offset: new AMap.Pixel(0, -20)
    });
    infoWindow.open(Vue.prototype.globalAMap, data.markers[0].getPosition());
  }
};
export default {
  state,
  actions,
  mutations
};

import {
  getPersonnelCount,
  getOldStreetCount,
  countStatistics,
  lineStatistics,
  getScenicCoordinateInfo,
  getOtherScenicOne,
  getWarncountStatistics,
  getTaskCoordinateInfo,
  getProblemCount,
  getTaskDetailsInfo,
  getDefaultConfig,
  findTree,
  connectStream,
  disConnectStream,
  saveDefaultConfig,
  getallcameralist,
  gettaskInfo,
  getCountDevice,
  getListDevice
} from "../../api/oldstreet/oldstreet";
import store from "@/store/index";
const state = {
  analyseCarPopVisible: false,
  analyseCarPersonVisible: false,
  waringEquimentPopVisible: false,
  // 基础信息
  basiInfoTongji: {},
  // 老街景点统计
  oldstreetTongji: {},
  // 客流统计
  customerTongji: {
    enterCount: "",
    exitCount: ""
  },
  // 客流分析参数
  customerParams: {
    cameraType: 0,
    position: "东入口"
  },
  // 人流、客流折线图
  statisticsLine: [],
  statisticCount: {
    enterLine: [],
    exitLine: [],
    timeArray: []
  },
  // 景点撒点
  ScenicCoordinateInfoList: [],
  ScenicOneInfo: {},
  // 智能告警统计
  WarncountStatistics: [],
  // 智能告警其中一类撒点
  WarncountStatisticssingle: [],
  warnParams: {
    timeRangeType: 3,
    type: "",
    smartType: 1
  },
  // 智能告警撒点
  TaskCoordinateInfo: [],
  // 问题上报数量
  problemCount: {},
  // 智能告警二级统计页面
  taskdetailInfo: {},
  // 监控情况视频列表
  cameralistFour: [
    { hlsUrl: "" },
    { hlsUrl: "" },
    { hlsUrl: "" },
    { hlsUrl: "" }
  ],
  // 监控点位结构树
  cameratreelist: [],
  // 地图撒点
  allcameralist: [],
  // ws消息
  scrollInfo: [],
  getsingletaskInfo: {}
};
const actions = {
  gettaskInfo({ commit }, params) {
    let p = gettaskInfo(params);
    p.then(res => {
      console.log(res);
      commit("GET_WARNING_DETAIL", res.data.result);
      // commit('GET_TASK_INFO',res)
    });
  },
  getallcameralist({ commit }, params) {
    let p = getallcameralist(params);
    p.then(res => {
      commit("GET_ALLCAMERA_LIST", res);
    });
  },
  saveDefaultConfig({ commit }, params) {
    let p = saveDefaultConfig(params);
    p.then(res => {
      commit;
      res;
    });
  },
  disConnectStream({ commit }, params) {
    let p = disConnectStream(params);
    p.then(res => {
      commit;
      res;
    });
  },
  connectStream({ commit }, params) {
    let p = connectStream(params);
    p.then(res => {
      commit;
      res;
    });
  },
  findTree({ commit }, params) {
    let p = findTree(params);
    p.then(res => {
      commit("GET_CAMERA_TREE", res);
    });
  },
  getDefaultConfig({ commit }, params) {
    let p = getDefaultConfig(params);
    p.then(res => {
      commit("GET_CAMERA_LIST", res);
    });
  },
  getTaskDetailsInfo({ commit }, params) {
    let p = getTaskDetailsInfo(params);
    return p;
  },
  getProblemCount({ commit }, params) {
    let p = getProblemCount(params);
    p.then(res => {
      commit("GET_PROBLEM_COUNT", res);
    });
  },
  getTaskCoordinateInfo({ commit }, params) {
    let p = getTaskCoordinateInfo(params);
    p.then(res => {
      commit("GET_TASKCOORDINATE_INFO", res);
    });
  },
  getWarncountStatistics({ commit }, params) {
    let p = getWarncountStatistics(params);
    p.then(res => {
      commit("GET_WARNCOUNT_STATISTICS", res);
    });
  },
  // 获取告警其中一类
  getWarncountStatistics_type({ commit }, params) {
    let p = getWarncountStatistics(params);
    p.then(res => {
      commit("GET_WARNCOUNT_STATISTICS_TYPE", res);
    });
  },
  getScenicCoordinateInfo({ commit }, params) {
    let p = getScenicCoordinateInfo(params);
    p.then(res => {
      commit("GET_SCENICCORD_DINATEINFO", res);
    });
  },
  lineStatistics({ commit }, params) {
    let p = lineStatistics(params);
    p.then(res => {
      commit("LINE_STATISTICS", res);
    });
  },
  getPersonnelCount({ commit }, params) {
    let p = getPersonnelCount(params);
    p.then(res => {
      commit("GET_PERSONNEL_COUNT", res);
    });
  },
  getOldStreetCount({ commit }, params) {
    let p = getOldStreetCount(params);
    p.then(res => {
      commit("GET_OLDSTREET_COUNT", res);
    });
  },
  countStatistics({ commit }, params) {
    let p = countStatistics(params);
    p.then(res => {
      console.log(res);
      commit("COUNT_STATISTICS", res);
    });
  },
  getOtherScenicOne({ commit }, params) {
    let p = getOtherScenicOne(params);
    p.then(res => {
      commit("GET_OTHER_SCENICONE", res);
    });
  },
  getCountDevice({ commit }, params) {
    let p = getCountDevice(params);
    p.then(res => {});
    return p;
  },
  getListDevice({ commit }, params) {
    console.log("走了");
    let p = getListDevice(params);
    p.then(res => {
      commit("GET_WARNING_EQUIMENT_LIST", res);
    });
    return p;
  }
};
const mutations = {
  // GET_TASK_INFO(state,res){
  //     state.getsingletaskInfo = res
  // },
  // ws消息推送
  SET_SCROLL_INFO(state, res) {
    console.log(res);
    state.scrollInfo = res;
  },
  GET_ALLCAMERA_LIST(...arg) {
    let [
      state,
      {
        data: { results }
      }
    ] = arg;
    results.forEach(item => {
      item.popType = "CAMERA_POINT";
      item.type = "CAMERA";
    });
    state.allcameralist = results;
    store.commit("ADD_OLDSTREET_MARKER", results);
  },
  GET_WARNING_EQUIMENT_LIST(...arg) {
    let [
      state,
      {
        data: { results }
      }
    ] = arg;
    results.forEach(item => {
      item.popType = "WENBAO_POINT";
      item.type = "TASK_POINT";
    });
    store.commit("ADD_OLDSTREET_MARKER", results);
  },
  GET_CAMERA_TREE(...arg) {
    let [
      state,
      {
        data: { results }
      }
    ] = arg;
    state.cameratreelist = results;
  },
  GET_CAMERA_LIST(...arg) {
    let [
      state,
      {
        data: { results }
      }
    ] = arg;
    state.cameralistFour = results;
  },
  GET_TASKDETAIL_INFO(...arg) {
    let [
      state,
      {
        data: { result }
      }
    ] = arg;
    state.taskdetailInfo = result;
  },
  GET_PROBLEM_COUNT(...arg) {
    let [
      state,
      {
        data: { result }
      }
    ] = arg;
    state.problemCount = result;
  },
  GET_TASKCOORDINATE_INFO(...arg) {
    let [
      state,
      {
        data: { results }
      }
    ] = arg;
    state.TaskCoordinateInfo = results;
    results.forEach(item => {
      item.type = "WARNING";
      item.popType = "TASK_POINT";
    });
    store.commit("ADD_OLDSTREET_MARKER", results);
  },
  GET_WARNCOUNT_STATISTICS(...arg) {
    let [
      state,
      {
        data: { results }
      }
    ] = arg;
    state.WarncountStatistics = results;
  },
  GET_WARNCOUNT_STATISTICS_TYPE(...arg) {
    let [
      state,
      {
        data: { results }
      }
    ] = arg;
    state.WarncountStatisticssingle = results;
  },
  GET_OTHER_SCENICONE(...arg) {
    let [
      state,
      {
        data: { result }
      }
    ] = arg;
    state.ScenicOneInfo = result;
  },
  GET_SCENICCORD_DINATEINFO(...arg) {
    let [
      state,
      {
        data: { results }
      }
    ] = arg;
    state.ScenicCoordinateInfoList = results;
    results.forEach(item => {
      if (item.ponitType == "1") {
        item.popType = "WENBAO_POINT";
      }
      if (item.ponitType == "2") {
        item.popType = "WENHUA_POINT";
      }
      if (item.ponitType == "6") {
        item.popType = "HUODONG_POINT";
      }
      if (item.ponitType == "7") {
        item.popType = "TIYU_POINT";
      }
      if (item.ponitType == "8") {
        item.popType = "SHUAN_POINT";
      }
      if (item.ponitType == "9") {
        item.popType = "ZHAN_POINT";
      }
      if (item.ponitType == "10") {
        item.popType = "JUWEI_POINT";
      }
      item.type = "DEVICE";
    });
    store.commit("ADD_OLDSTREET_MARKER", results);
  },
  LINE_STATISTICS(...arg) {
    let [
      state,
      {
        data: { results }
      }
    ] = arg;
    let enterLine = [];
    let exitLine = [];
    let timeArray = [];
    results.forEach(res => {
      enterLine.push(res.enterCount);
      exitLine.push(res.exitCount);
      timeArray.push(res.time);
    });
    state.statisticCount = {
      enterLine,
      exitLine,
      timeArray
    };
    // console.log(state.statisticCount)
    state.statisticsLine = results;
  },
  // 修改客流入口
  CHANGE_CUSTOMER(state, res) {
    state.customerParams.position = res;
  },
  // 修改客\人流
  CHANGE_TYPE(state, res) {
    state.customerParams.cameraType = res;
  },
  COUNT_STATISTICS(...arg) {
    let [
      state,
      {
        data: { result }
      }
    ] = arg;
    state.customerTongji = result;
  },
  GET_OLDSTREET_COUNT(...arg) {
    let [
      state,
      {
        data: { result }
      }
    ] = arg;
    state.oldstreetTongji = result;
  },
  GET_PERSONNEL_COUNT(...arg) {
    let [
      state,
      {
        data: { result }
      }
    ] = arg;
    state.basiInfoTongji = result;
  },
  ANALYSE_CAR_POP_VISIBLE(state, res) {
    state.analyseCarPopVisible = res;
  },
  ANALYSE_PERSON_POP_VISIBLE(state, res) {
    state.analyseCarPersonVisible = res;
  },
  WARING_EQUIMENT_POP_VISIBLE(state, res) {
    state.waringEquimentPopVisible = res;
  },
  CHANGE_WARN_PARAMS(state, res) {
    state.warnParams.timeRangeType = res.timeRangeType;
    state.warnParams.smartType = res.smartType;
    store.dispatch("getWarncountStatistics", state.warnParams);
  },
  CHANGE_WARN_PARAMS_TYPE(state, res) {
    state.warnParams[res.name] = res.value;
    store.dispatch("getTaskCoordinateInfo", state.warnParams);
  },
  CHANGE_WARN_EQUIMENT_TYPE(state, res) {
    store.dispatch("getListDevice", { type: res.value });
  }
};
export default {
  state,
  actions,
  mutations
};


import {cityState,findMetroPoint,findMetroPointInfo,findMetroPointList} from '../../../api/lingang/cityState/cityState'
const state = {
    cityState:{
        threeChaos:0,
        illegalConstruction:0,
        moveCarHelp:0,
        exposedGarbage:0,
        noisePollution:0,
        cdcPesticide:0,
        groupRent:0,
        housingSecurity:0,
        epidemicSituation:0,

    },
    manageTaskTablelistPop:[
        {
            taskName:'标题',
            category:1,
            status:3,
            createTime:'2020-02-09 12:23:34'
        }
    ],
    findMetroPoint:{},
    findMetroPointInfo:{},
    findMetroPointList:[{policyName:''}]
}

const actions = {
    cityState({commit},params){
        let p = cityState(params);
        p.then(res =>{
            commit("CITY_STATE",res);
        })
    },
    findMetroPoint({commit},params){
        commit;
        let p = findMetroPoint(params);
        p.then(res =>{
            console.log(res.data.slice(0,99));
            commit("ADD_CITYSTATE_MARKER",res.data);
        })
    },
    findMetroPointInfo({commit},params){
        commit;
        let p = findMetroPointInfo(params);
        return p;
        // p.then(res =>{
        //     console.log(res);
        //     commit("FIND_METROPOINT_INFO",res);
        // })
    },
    findMetroPointList({commit},params){
        commit;
        let p  = findMetroPointList(params);
        p.then(res =>{
            commit("FIND_METROPOINT_LIST",res)
        })
    }
}

const mutations = {
    CITY_STATE(state,res){
        state.cityState = res.data;
    },
    FIND_METROPOINT_INFO(state,res){
        state.cityState = res.data;
    },
    FIND_METROPOINT_LIST(state,res){
        state.findMetroPointList = res.data.currentList;
    }
}
export default{
    state,actions,mutations
}
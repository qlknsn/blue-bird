import {
  commpanyList,
  videoList,
  basicDataPool,
  SynergyPage,
  companyPage,
  watchPage,
  findPageList,
  findSensorDeviceList,
  lingangDevices
} from "@/store/api/lingang/basicInfo/basicInfo.js";
import { getLingangUrl } from "@/store/api/lingang/liveCast/live";

import Vue from "vue";
import store from "@/store/index";
import { selectdeviceIcon } from "@/utils/deviceIcon";
const state = {
  basicInfoTitle: "基础信息",
  qiyeList: [],
  cluster: null, //聚合点
  qiyeInfo: {},
  marker: null,
  sanhuiInfo: {},
  hotLineInfo: {},
  videoList: [],
  currentUrn: "",
  basicdatapool: {},
  synergyList: [],
  synergyParams: {
    page: 1,
    type: 0,
    districtId: "",
    pagesize: 10
  },
  synergyTotal: 0,
  companyList: [],
  watchList: [],
  fangyiList: [],
  sensorDeviceList: [],
  location: ""
};
const actions = {
  commpanyList({ commit }, params) {
    let p = commpanyList(params);
    p.then(res => {
      commit("GET_QIYE_LIST", res);
    });
  },
  lingangvideoList({ commit }, params) {
    commit;
    let p = videoList(params);
    return p;
  },
  basicDataPool({ commit }, params) {
    commit;
    let p = basicDataPool(params);
    p.then(res => {
      commit("BASIC_DATA_POOL", res);
    });
  },
  SynergyPage({ commit }, params) {
    commit;
    let p = SynergyPage(params);
    p.then(res => {
      commit("SYNERGY_PAGE", res);
    });
  },
  companyPage({ commit }, params) {
    commit;
    let p = companyPage(params);
    p.then(res => {
      commit("COMPANY_PAGE", res);
    });
  },
  watchPage({ commit }, params) {
    commit;
    let p = watchPage(params);
    p.then(res => {
      commit("WATCH_PAGE", res);
    });
  },
  findPageList({ commit }, params) {
    commit;
    let p = findPageList(params);
    p.then(res => {
      commit("FIND_PAGE_LIST", res);
    });
  },
  findSensorDeviceList({ commit }, params) {
    commit;
    let p = findSensorDeviceList(params);
    p.then(res => {
      res;
      commit("FIND_SENSOR_DEVICE_LIST", res);
    });
  },
  lingangDevices({ commit }, params) {
    commit;
    let p = lingangDevices(params);
    p.then(res => {
      commit("GET_DEVICE_LIST", res);
    });
  },
  getLingangUrl({ commit }, params) {
    let p = getLingangUrl(params);
    p.then(res => {
      console.log(params);
      commit("CONNECT_LINGANG_VIDEO", res);
    });
  }
};

const mutations = {
  SET_BASICINFO_TITLE(state, res) {
    state.basicInfoTitle = res;
  },
  GET_QIYE_LIST(state, res) {
    state.qiyeList = res.data;
  },
  CLEAR_MAP() {
    Vue.prototype.globalAMap.clearMap();
    store.commit("POLY_GON");
  },
  CLEAR_LINGANG_MARKER(state) {
    console.log("清除撒点");
    if (state.marker) {
      Vue.prototype.globalAMap.remove(state.marker);
      state.marker = null;
    }
    if (state.videoList.length > 0) {
      state.cluster.clearMarkers();
      state.videoList = [];
    }
  },
  ADD_MARKER(state, res) {
    store.commit("CLEAR_MARKERS");
    if (state.marker) {
      Vue.prototype.globalAMap.remove(state.marker);
      state.marker = null;
    }
    if (state.videoList.length > 0) {
      state.cluster.clearMarkers();
      state.videoList = [];
    }

    // store.commit('CLEAR_MAP')

    state.marker = new AMap.Marker({
      position: new AMap.LngLat(res.lng, res.lat),
      snippet: res,
      icon: selectdeviceIcon(res.popType)
    });
    state.marker.on("click", e => {
      switch (e.target.w.snippet.popType) {
        case "SET_QIYEINFOPOP_VISIBLE":
          state.qiyeInfo = e.target.w.snippet;
          break;
        case "SET_SANHUI_VISIBLE":
          state.sanhuiInfo = e.target.w.snippet;
          break;
        case "SET_HOTLINE_VISIBLE":
          state.hotLineInfo = e.target.w.snippet;
          break;
        case "UN_SET_HOTLINE_VISIBLE":
          state.hotLineInfo = e.target.w.snippet;
          break;
        case "SET_VIDEOPOP_VISIBLE":
          state.videoInfo = e.target.w.snippet;
          break;
      }
      store.commit(e.target.w.snippet.popType, true);
    });
    Vue.prototype.globalAMap.add(state.marker);
  },

  ADD_VIDEO_MARKER(state, res) {
    store.commit("CLEAR_MARKERS");
    if (state.marker) {
      Vue.prototype.globalAMap.remove(state.marker);
      state.marker = null;
    }
    if (state.videoList.length > 0) {
      state.cluster.clearMarkers();
      state.videoList = [];
    }
    // store.commit('CLEAR_MAP')
    res.forEach(e => {
      e.properties = { _sys_detectionType: "CAMERAGROUP" };
      // e.urn={organization:e.organization,group:e.videoGroup,id:e.cameraId}
      let marker = new AMap.Marker({
        position: new AMap.LngLat(e.Longitude, e.Latitude),
        itemData: e,
        icon: selectdeviceIcon("CAMERAGROUP")
      });
      marker.on("click", e => {
        let d = e.target.w.itemData;
        console.log(d);
        // state.currentUrn = d.cameraId;
        // let videoId = d.cameraId
        // state.videoconcatparams = {
        //   action:'CONNECT',
        //   cameraId:[{streamNodeUrn:videoId}],
        //   protocol:'hls',
        //   streamType:0
        // }
        // store.dispatch('connectRandomVideo',state.videoconcatparams)
        store.commit("CONTROL_DIALOG", true);

        // store.commit('SET_DIALOG_NAME', 'video')
        // store.commit('SET_DIALOG_INFOLIST', [{}])
        // store.commit('SET_DIALOG_TITLE', '监控画面')
      });
      state.videoList.push(marker);
      Vue.prototype.globalAMap.add(state.videoList);
    });
    Vue.prototype.globalAMap.plugin(["AMap.MarkerClusterer"], function() {
      state.cluster = new AMap.MarkerClusterer(
        Vue.prototype.globalAMap,
        state.videoList,
        {
          gridSize: 80,
          zoomOnClick: false
        }
      );
      // 点击聚合点的回调
      state.cluster.on("click", function(e) {
        if (Vue.prototype.globalAMap.getZoom() == 18) {
          let lnglatLocalList = e.markers;
          let obj = {
            position: lnglatLocalList[0].Ce.position,
            markers: lnglatLocalList
          };
          console.log(obj);
          store.commit("SHOW_LINGANG_INFO_WINDOW", obj);
        } else {
          store.commit("ZOOM_IN_GLOBAL");
        }
      });
    });
  },

  ADD_CITYSTATE_MARKER(state, res) {
    store.commit("CLEAR_MARKERS");
    if (state.marker) {
      Vue.prototype.globalAMap.remove(state.marker);
      state.marker = null;
    }
    if (state.videoList.length > 0) {
      state.cluster.clearMarkers();
      state.videoList = [];
    }
    // store.commit('CLEAR_MAP')
    res.forEach(e => {
      console.log(e);
      e.properties = { _sys_detectionType: "SET_HOTLINE_VISIBLE" };
      // e.urn={organization:e.organization,group:e.videoGroup,id:e.cameraId}
      let marker = new AMap.Marker({
        position: new AMap.LngLat(e.lng, e.lat),
        itemData: e,
        size: 25,
        icon:
          e.status == 2
            ? selectdeviceIcon("UN_SET_HOTLINE_VISIBLE")
            : selectdeviceIcon("SET_HOTLINE_VISIBLE")
      });
      marker.on("click", e => {
        // let d = e.target.w.itemData;
        // console.log(d)
        // state.currentUrn = d.cameraId;
        // let videoId = d.cameraId
        // state.videoconcatparams = {
        //   action:'CONNECT',
        //   cameraId:[{streamNodeUrn:videoId}],
        //   protocol:'hls',
        //   streamType:0
        // }
        // store.dispatch('connectRandomVideo',state.videoconcatparams)
        console.log(e);
        if (e.target.w.itemData.allcount > 1) {
          let params = {
            times: 6,
            page: 1,
            pagesize: 1000,
            lng: e.target.w.itemData.lng,
            lat: e.target.w.itemData.lat,
            districtId: Vue.ls.get("districtId"),
            infoatnameWord: e.target.w.itemData.infoatnameWord
          };
          store.dispatch("findMetroPointList", params);
          store.commit("SET_CITY_STATE_TABLE_VISIBLE", true);
        } else {
          let params = {
            id: e.target.w.itemData.id,
            types: e.target.w.itemData.types,
            keyWord: Vue.ls.get("userInfo").keyWord,
            infoatname: e.target.w.itemData.infoatnameWord
          };
          store.dispatch("findMetroPointInfo", params).then(res => {
            state.hotLineInfo = res.data;
            store.commit("SET_HOTLINE_VISIBLE", true);
          });
        }

        // store.commit('SET_DIALOG_NAME', 'video')
        // store.commit('SET_DIALOG_INFOLIST', [{}])
        // store.commit('SET_DIALOG_TITLE', '监控画面')
      });
      state.videoList.push(marker);
      Vue.prototype.globalAMap.add(state.videoList);
    });
    Vue.prototype.globalAMap.plugin(["AMap.MarkerClusterer"], function() {
      state.cluster = new AMap.MarkerClusterer(
        Vue.prototype.globalAMap,
        state.videoList,
        {
          gridSize: 80,
          zoomOnClick: false
        }
      );
      // 点击聚合点的回调
      state.cluster.on("click", function(e) {
        if (Vue.prototype.globalAMap.getZoom() == 18) {
          let lnglatLocalList = e.markers;
          console.log(e.markers);
          let obj = {
            position: lnglatLocalList[0].Ce.position,
            markers: lnglatLocalList
          };
          console.log(obj);
          store.commit("SHOW_LINGANG_INFO_WINDOW", obj);
        } else {
          store.commit("ZOOM_IN_GLOBAL");
        }
      });
    });
  },
  GET_LOCATION(state, d) {
    state.location = d.location;
  },
  SHOW_LINGANG_INFO_WINDOW(state, data) {
    // let that = this
    const contents = Vue.extend({
      template: `
      <div class='infoBox' style='max-height:300px'>
      <ul>
      <li v-for="item in markerList"><img @click="handleConnectVideo(item)" :src="item|getImgUrl"></img><span style='color:white'>{{item.w.itemData.Name}}</span></li>
      </ul>
      </div>
            `,
      data() {
        return {
          markerList: data.markers
        };
      },
      mounted() {},
      filters: {
        getImgUrl: function(val) {
          let d = val.w.itemData;
          console.log(val);
          return selectdeviceIcon(d.properties._sys_detectionType);
        }
      },
      methods: {
        handleConnectVideo(e) {
          let d = e.w.itemData;

          // state.currentUrn = d.cameraId;
          // let videoId = d.cameraId
          // let params = {
          //   action:'CONNECT',
          //   cameraId:[{streamNodeUrn:videoId}],
          //   protocol:'hls',
          //   streamType:0
          // }
          // store.dispatch('connectRandomVideo',params)
          store.commit("CONTROL_DIALOG", true);
          store.commit("SET_DIALOG_NAME", "video");
          store.commit("SET_DIALOG_INFO", { id: 1, url: d.RtspAddress });
          store.commit("SET_DIALOG_TITLE", "监控画面");
        }
      },
      computed: {
        // url: function() {
        //   return "http://baidu.com";
        // }
      }
    });
    const con = new contents().$mount();
    // const contents = `
    //     <div>
    //         ${data.markers.map(item=>`
    //             <div onclick="${openvideo()}">
    //                 <img src="${item.Ce.icon.w.image}">
    //             </div>
    //         `).join(' ')}
    //     </div>
    // `
    var infoWindow = new AMap.InfoWindow({
      // isCustom: true,
      content: con.$el,
      position: data.position,
      offset: new AMap.Pixel(0, -20)
    });
    infoWindow.open(Vue.prototype.globalAMap, data.markers[0].getPosition());
  },
  BASIC_DATA_POOL(state, res) {
    state.basicdatapool = res.data;
  },
  SYNERGY_PAGE(state, res) {
    state.synergyList = res.data.records;
    state.synergyTotal = res.data.total;
  },
  COMPANY_PAGE(state, res) {
    state.companyList = res.data;
  },
  WATCH_PAGE(state, res) {
    state.watchList = res.data;
  },
  FIND_PAGE_LIST(state, res) {
    state.fangyiList = res.data;
  },
  FIND_SENSOR_DEVICE_LIST(state, res) {
    state.sensorDeviceList = res.data;
  },
  HOTLINE_INFO(state, res) {
    state.hotLineInfo = res;
  }
};

export default {
  state,
  actions,
  mutations
};

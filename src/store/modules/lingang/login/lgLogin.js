import {lgLogin} from "../../../api/lingang/login/login";
import router from "../../../../router";
import axios from "axios";
import Vue from "vue";
import { Message } from "element-ui";
const state = {
  userInfo: {}
}
const actions = {
  lgLogin ({commit}, data) {
    let p = lgLogin(data)
    p.then(res => {
      if(res.code==200){
        commit('GET_USER_INFO', res.data)
        commit('POLY_GON')
      }else{
        Message.warning(res.msg)
      }
      
    })
  }
}
const mutations = {
  GET_USER_INFO(state, res){
    state.userInfo = res
    Vue.ls.set("userInfo", JSON.stringify(res));
    Vue.ls.set("districtId", res.districtId);
    Vue.ls.set("keyWord", res.keyWord);
    axios.get('/json/videoAggregation.json').then(res=>{
      router.push({
        path: `/lingang/bigscreen?token=${res.data.token}`,
      })
    })
  }
}

export default {
  state,
  actions,
  mutations
}

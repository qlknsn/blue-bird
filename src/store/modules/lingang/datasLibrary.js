const state = {
  dialogName: 'video',
  dialogInfo: {},
  dialogInfoList: [],
  dialogTitle: ''
};
const actions = {

};

const mutations = {
  SET_DIALOG_NAME(state, res) {
    state.dialogName = res // 弹窗类型分为普通版normal和表格版unnormal和视频版video
  },
  SET_DIALOG_INFO(state, res) {
    state.dialogInfo = res
  },
  SET_DIALOG_INFOLIST (state, res) {
    state.dialogInfoList = res
  },
  SET_DIALOG_TITLE (state, res) {
    state.dialogTitle = res
  }
};

export default {
  state,
  actions,
  mutations
};
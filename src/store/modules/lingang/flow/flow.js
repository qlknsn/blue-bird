import {
  meetingsList,
  eventList,
  meetingDetail,
  orderInfo,
  taskInfo
} from "@/store/api/lingang/flow/flow.js";
const state = {
  meetingsList: [],
  eventList: {
    lastMonthTaskCount: 0,
    totalTaskCount: 0,
    data: {
      list: []
    }
  },
  meetingDetail: {},
  orderInfo: {},
  taskInfo: {}
};
const actions = {
  meetingsList({ commit }, params) {
    commit;
    let p = meetingsList(params);
    p.then(res => {
      commit("MEETING_LIST", res);
    });
  },
  eventList({ commit }, params) {
    commit;
    let p = eventList(params);
    p.then(res => {
      commit("EVENT_LIST", res);
    });
  },
  meetingDetail({ commit }, params) {
    commit;
    let p = meetingDetail(params);
    p.then(res => {
      commit("MEETING_DETAIL", res);
    });
  },
  orderInfo({ commit }, params) {
    let p = orderInfo(params);
    p.then(res => {
      commit("ORDER_INFO", res);
    });
  },
  taskInfo({ commit }, params) {
    let p = taskInfo(params);
    p.then(res => {
      commit("taskInfo", res);
    });
  }
};
const mutations = {
  MEETING_LIST(state, res) {
    state.meetingsList = res.data;
  },
  EVENT_LIST(state, res) {
    if (res.data) {
      state.eventList = res.data;
    }
  },
  MEETING_DETAIL(state, res) {
    state.meetingDetail = res;
  },
  ORDER_INFO(state, res) {
    state.orderInfo = res;
  },
  TASK_INFO(state, res) {
    state.taskInfo = res;
  }
};
export default {
  state,
  actions,
  mutations
};

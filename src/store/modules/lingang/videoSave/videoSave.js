import {
  lgGetVideoList,
  lgGetMessageList,
  lgVideoTimeSet
} from "@/store/api/lingang/videoSave/videoSave";

const state = {
  lgVideoList: [
    { RtspAddress: "1" },
    { RtspAddress: "2" },
    { RtspAddress: "3" },
    { RtspAddress: "4" }
  ],
  lgVideoIndex: 0,
  playChangeTime: 1,
  lgMessageList: [],
  lgMessageListTotal: 0
};
const actions = {
  lgGetVideoList({ commit }, data) {
    let p = lgGetVideoList(data);
    p.then(res => {
      commit("LG_GET_VIDEO_LIST", res);
    });
  },
  lgGetMessageList({ commit }, data) {
    let p = lgGetMessageList(data);
    p.then(res => {
      commit("LG_GET_MESSAGE_LIST", res);
    });
  },
  lgVideoTimeSet({ commit }, data) {
    let p = lgVideoTimeSet(data);
    p.then(res => {
      commit("LG_VIDEO_TIME_SET", res);
    });
  }
};

const mutations = {
  LG_GET_VIDEO_LIST(state, res) {
    state.lgVideoList = res.data;
    state.lgVideoList.push("1");
    state.lgVideoList.splice(4, 1);
    state.lgVideoIndex = res.index;
    state.playChangeTime = res.playChangeTime;
  },
  LG_GET_MESSAGE_LIST(state, res) {
    state.lgMessageList = res.data.records;
    state.lgMessageListTotal = res.data.total;
  },
  LG_VIDEO_TIME_SET(state, res) {
    state;
    res;
  }
};

export default {
  state,
  actions,
  mutations
};

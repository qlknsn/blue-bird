const state = {
    isShowDialog: false,
    basicInfoVisible:true,
    qiyeListVisible:false,
    zhishoulistVisile:false,
    qiyeInfoPopVisible:false,
    dutylistPopVisible:false,
    sensorListPopVisible:false,
    sanhuiPopVisible:false,
    hotlinePopVisible:false,
    cityStateTableVisible:false,
    epidemicPopVisible:false
};
const actions = {
  
};

const mutations = {
  SET_BASICINFO_VISIBLE(state,res){
    state.basicInfoVisible = res
  },
  SET_QIYELIST_VISIBLE(state,res){
    state.qiyeListVisible = res
  },
  SET_ZHISHOULIST_VISIBLE(state,res){
    state.zhishoulistVisile = res
  },
  CONTROL_DIALOG(state, res) {
    state.isShowDialog = res
  },
  SET_QIYEINFOPOP_VISIBLE(state,res){
    state.qiyeInfoPopVisible = res
  },
  SET_DUTYLISTPOP_VISIBLE(state,res){
    state.dutylistPopVisible = res
  },
  SET_SENSORLISTPOP_VISIBLE(state,res){
    state.sensorListPopVisible = res
  },
  SET_SANHUI_VISIBLE(state,res){
    state.sanhuiPopVisible =res;
  },
  UN_SET_HOTLINE_VISIBLE(state,res){
    state.hotlinePopVisible = res;
  },
  SET_HOTLINE_VISIBLE(state,res){
    state.hotlinePopVisible = res;
  },
  SET_CITY_STATE_TABLE_VISIBLE(state,res){
    state.cityStateTableVisible = res;
  },
  SET_EPIDEMICPOP_VISIBLE(state,res){
    state.epidemicPopVisible = res;
  },
};

export default {
  state,
  actions,
  mutations
};
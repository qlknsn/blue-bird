// import { commpanyList,videoList } from "@/store/api/lingang/basicInfo/basicInfo.js";
import Vue from 'vue';
// import store from "@/store/index";
// import { selectdeviceIcon } from "@/utils/deviceIcon";
import * as esriloader from 'esri-loader'
import mapconfig from '@/config/config_map.js'
import jsapiUtil from '@/utils/jsapi'
const state = { 

};
const actions = {

};

const mutations = {
    async INIT_LINGANG_MAP(){
        esriloader.loadCss(mapconfig.cssUrl) //添加样式
        const [
            // esriConfig,
            Map,
            // Basemap,
            SceneView,
            // MapView,
            SpatialReference,
            // IdentityManager,
            // Camera,
            // LayerList,
            // Expand,
            Fullscreen,
            // Legend,
            GroupLayer,
            TileLayer,
            // SceneLayer,
            // FeatureLayer,
            // DirectLineMeasurement3D,
            // AreaMeasurement3D,
            // Graphic,
            // watchUtils,
          ] = await jsapiUtil.load([
            // 'esri/config',
            'esri/Map',
            // 'esri/Basemap',
            'esri/views/SceneView',
            // 'esri/views/MapView',
            'esri/geometry/SpatialReference',
            // 'esri/identity/IdentityManager',
            // 'esri/Camera',
            // 'esri/widgets/LayerList',
            // 'esri/widgets/Expand',
            'esri/widgets/Fullscreen',
            // 'esri/widgets/Legend',
            'esri/layers/GroupLayer',
            'esri/layers/TileLayer',
            // 'esri/layers/SceneLayer',
            // 'esri/layers/FeatureLayer',
            // 'esri/widgets/DirectLineMeasurement3D',
            // 'esri/widgets/AreaMeasurement3D',
            // 'esri/Graphic',
            // 'esri/core/watchUtils',
        ])
        Vue.prototype.lingangGlobal_map = new Map({
            ground: {
                surfaceColor: '#021425',
            },
        })
        const initCamera = {
            heading: 0,
            tilt: 0.49999999999913347,
            fov: 55,
            position: {
              x: 31677.802827289288,
              y: -33415.02681828593,
              z: 95587.70223144352,
              spatialReference: SpatialReference.WebMercator,
            },
          }
        Vue.prototype.lingangGlobal_sceneView = new SceneView({
            container: 'mapLingangcontainer',
            viewingMode: 'local',
            map: Vue.prototype.lingangGlobal_map,
            camera: initCamera,
            popup: {
              defaultPopupTemplateEnabled: true,
            },
        })
        const fullScreen = new Fullscreen({
            view: Vue.prototype.lingangGlobal_sceneView,
        })
        Vue.prototype.lingangGlobal_sceneView.ui.add(
            [
              fullScreen,
            ],
            'top-left'
        )
        const baseLayer = await new TileLayer(
            'http://10.89.5.14/arcgis/rest/services/basemap/base_kp_3857/MapServer',
            {
              id: 'zhengwuBasemap',
              visible: true,
              opacity: 1,
              title: '政务底图',
            }
          )
        const baseMapGroupLayer = new GroupLayer({
            title: '基础底图',
            visible: true,
            visibilityMode: 'exclusive',
            layers: [baseLayer],
            opacity: 1,
          })
        Vue.prototype.lingangGlobal_map.layers.add(baseMapGroupLayer)
    }
};

export default {
  state,
  actions,
  mutations
};
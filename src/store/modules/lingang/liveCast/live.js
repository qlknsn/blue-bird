import { getLiveUrl,getLingangUrl } from "@/store/api/lingang/liveCast/live";
import Vue from "vue";
// import router from '@/router'
// import router from "@/router/index";
import { GET_LIVE_URL } from "@/store/mutation-types.js";

const state = { lingangNode: { metadata: { url: "" } } };
const actions = {
  getLiveUrl({ commit }, data) {
    let p = getLiveUrl(data);
    p.then(res => {
      commit(GET_LIVE_URL, res);
    });
  }
};

const mutations = {
  [GET_LIVE_URL](...arg) {
    let [state, { result }] = [...arg];
    state.lingangNode = result;
    console.log(result);
  }
};

export default {
  state,
  actions,
  mutations
};

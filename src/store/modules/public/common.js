import { markCameraDamaged } from "@/store/api/public/common";
import { MARK_CAMERA_DAMAGED } from "@/store/mutation-types.js";
import Vue from "vue";
const state = { testData: 123 };
const actions = {
  markCameraDamaged({ commit }, data) {
    let p = markCameraDamaged(data);
    p.then(res => {
      commit("MARK_CAMERA_DAMAGED", res);
    });
  }
};
const mutations = {
  [MARK_CAMERA_DAMAGED](...arg) {
    let [, { status }] = [...arg];
    if (status == 1) {
      Vue.prototype.$message({ type: "success", message: "上报成功" });
    } else {
      Vue.prototype.$message({ type: "error", message: "发生错误" });
    }
  }
};

export default {
  state,
  actions,
  mutations
};

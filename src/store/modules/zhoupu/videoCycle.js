
import {
    closeStream,
    changeStream,
    fetchStream,
    markStream,
    markVideos
} from "@/store/api/zhoupu/videoCycle";
import store from "../..";
import { Message } from "element-ui";
const state = {
    leftStreamLists:[{id:''},{id:''},{id:''},{id:''},{id:''},{id:''}],
    leftStreamList: [{unionCode:''},{unionCode:''},{unionCode:''},{unionCode:''},{unionCode:''},{unionCode:''}],
    rightStreamList: [],
    leftStart: 0,
    allNum: 0,
    serport: 9001,
    markvideolist:[]
};
const actions = {
    markVideos({ commit }, params) {
        let p = markVideos(params)
        p.then(res => {
            console.log(res)
            commit('GET_MARK_VIEOS',res.data)
        })
    },
    closeStream({ commit }, params) {
        let p = closeStream(params)
        p.then(res => {
            commit;
            res;
        })
    },
    changeStream({ commit }, params) {
        let p = changeStream({ camId: params.camId })
        p.then(res => {
            console.log(res)
            res.index = params.index
            commit("CHANGE_STREAM", res)
        })
    },
    markStream({ commit }, params) {
        let p = markStream(params)
        p.then(res => {
            if(res){
                Message({
                    type:'success',
                    message:'标记成功'
                })
            }
        })
    },
    getfetchStream({ commit }, params) {
        let p = fetchStream(params)
        p.then(res => {
            console.log(res)
            commit("GET_STREAM", res)
        })
    },
};

const mutations = {
    GET_MARK_VIEOS(state,res){
        let port = 9001
        res.forEach(item=>{
            item.id = String(item.id)
            item.port=port++
            item.server = location.hostname
        })
        state.markvideolist = res
    },
    MARK_STREAM(state, res) {
        state;
        res
    },
    GET_STREAM(state, res) {
        state.leftStreamList = res
    },
    CHANGE_NUM(state, res) {
        state.allNum = res
    },
    SET_VIDEO_LEFT(state, res) {
        // if(state.leftStreamList.length>=6){
        //     state.leftStreamList = []
        // }
        // setTimeout(() => {
        //     state.leftStreamList.push(res)
        // }, 50);
        state.leftStreamList = res
    },
    SET_PORT(state,res){
        state.serport = 9001
    },
    CLOSE_LEFT_VIDEO() {
        state.leftStreamList.forEach(item => {
            store.dispatch('closeStream', { camId: item.camId })
        })
    },
    CHANGE_STREAM(state, res) {
        if (res.camId) {
            state.leftStreamList.splice(res.index, 1, { camId: res.camId, url: res.url })
        }
    }
};

export default {
    state,
    actions,
    mutations
};
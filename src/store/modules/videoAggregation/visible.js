const state = {
  videoRankvisible : true,
  mapMp4Visible:false,
  usablePointVisible:false,
  villageAnalyseListVisible:false
};
const actions = {
  
};

const mutations = {
   SET_VIDEO_RANK_VISIBLE(state,res){
    //  console.log(222)
    state.videoRankvisible = res
   },
   SET_VIDEO_MP4_VISIBLE(state,res){
    //  console.log(222)
    state.mapMp4Visible = res
   },
   SET_USABLE_POINT_VISIBLE(state,res){
     state.usablePointVisible = res;
   },
   SET_VILLAGE_ANALYSE_VISIBLE(state,res){
     state.villageAnalyseListVisible = res;
   }
};

export default {
  state,
  actions,
  mutations
};
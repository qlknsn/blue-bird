import {
    camerasSummary,
    devicesCamera,
    leadboardBySource,
    realtimeByDistrict,
    searchByCamera,
    usageSummary,
    searchByTag,
    leadboardByCamera,
    getAllTitle,
    cameraPoint,
    available,
    villageAnalyseList
} from "@/store/api/videoAggregation/videoAggregation";
// import router from "@/router/index";
const state = {
    camerasummary:{
        cameraTotal:0,
        nvrTotal:0,
        realCall:0,
        normal:0,
        abnormal:0,
        gbReg:0,
        url:0,
        onvif:0,
        other:0
    },
    leadboardBySourceList:[],
    callTimeParams:{//
        sort:'HITS',
        timeUnit:'Y',
        topN:6
    },
    leadboardBySourcetimeList:[],
    callTimelongParams:{//
        sort:'DURATIONS',
        timeUnit:'Y',
        topN:10
    },
    realTimeByDistrict:[],
    realTimeByDistrictcun:[],
    byCameraParams:{
        // count:9,
        // start:0,
        timeUnit:'Y',
        topN:10
    },
    // byCameraTotal:0
    byCameralistParams:{
        count:7,
        start:0,
        timeUnit:'Y',
        urns:[]
    },
    byCameraList:[],
    byCameraListAll:[],
    byCameraListTotalPage:0,
    invokingAnalyseParams:{
        timeUnit:'Y',
        type:0
    },
    invokingAnalyseList:[],
    invokingAnalyse2Params:{
        timeUnit:'Y',
        type:1
    },
    invokingAnalyse2List:[],
    searchBytaglist:[],
    searchbyParams:{
        timeUnit:'Y',
        urns:[]
    },
    arrPrex2:{
        prex:[],
        prey:[],
        prepoint:[]
    },
    arrPrex:{
        prex:[],
        prey:[],
        prepoint:[]
    } ,
    totalfenxi:[],
    allTitle: '',
    weather:{},
    metadatas:[],
    available:{
        pagination:{
            total:0
        },
        results:[]
    },
    villageAnalyseList:{},
    usablePointParams:{}
};

const actions = {
    getCamerasSummary({commit},params){
        let p = camerasSummary(params)
        p.then(res=>{
            commit('GET_CAMERA_SUMMARY',res.result.current.details)
            // console.log(res)
        })
    },
    devicesCamera({commit},params){
        let p = devicesCamera(params)
        p.then(res=>{
            console.log(res)
            commit('GET_DEVICE_LIST', res);
        })
    },
    leadboardBySource({commit},params){
        let p = leadboardBySource(params)
        p.then(res=>{
            commit('GET_LEADBOARDBY_SOURCE', res);
        })
    },
    leadboardBySourceTimeList({commit},params){
        let p = leadboardBySource(params)
        p.then(res=>{
            commit('GET_LEADBOARDBY_SOURCE_TIME', res);
        })
    },
    realtimeByDistrict({commit},params){
        let p = realtimeByDistrict(params)
        p.then(res=>{
            commit('GET_REALTIMEBY_DISTRICT', res);
        })
    },
    searchByCamera({commit},params){
        let p = leadboardByCamera(params)
        p.then(res=>{
            commit('GET_SEARCHBY_CAMERA', res);
        })
    },
    // leadboardByCamera({commit},params){
    //     let p = leadboardByCamera(params)
    //     p.then(res=>{
    //         commit('GET_LEADBOARDBY_CAMERA', res);
    //     })
    // },
    searchByCameraList({commit},params){
        let p = searchByCamera(params)
        p.then(res=>{
            commit('GET_SEARCHBY_CAMERA_LIST', res);
        })
    },
    usageSummary({commit},params){
        let p = usageSummary(params)
        p.then(res=>{
            if(params.type==0){
                commit('GET_USAGE_SUMMARY', res);
            }else{
                commit('GET_USAGE_SUMMARY2', res);
            }

        })
    },
    searchByTag({commit},params){
        state.arrPrex={
            prex:[],
            prey:[],
            prepoint:[]
        }
        state.arrPrex2={
            prex:[],
            prey:[],
            prepoint:[]
        }
        let p = searchByTag(params)
        p.then(res=>{
            let arr = res.result.current.details.slice(0,6)
            let timeMum = 0
            let timeLong = 0
            let sortNum = (a,b)=>{
                return b.hits-a.hits
            }
            let sortTimelong=(a,b)=>{
                return b.durations-a.durations
            }
            arr.forEach(item=>{
                timeMum+=item.hits
                timeLong+=item.durations

            })
            arr.forEach(item=>{
                item.numpercent = `${(item.hits/timeMum*100).toFixed(2)}`
                item.timelongpercent = `${(item.durations/timeLong*100).toFixed(2)}`

            })

            let sortTimeNum = arr.sort(sortNum)
            let sortTimeLong = arr.sort(sortTimelong)

            sortTimeNum.forEach(item=>{
                // Object.assign(state.arrPrex,{
                //     prex:item.category.displayValue,
                //     prey:item.hits,
                //     prepoint:item.numpercent,
                // })
                state.arrPrex.prex.push(item.category.displayValue)
                state.arrPrex.prey.push(item.hits)
                state.arrPrex.prepoint.push(item.numpercent)
            })
            sortTimeLong.forEach(item=>{
                state.arrPrex2.prex.push(item.category.displayValue)
                state.arrPrex2.prey.push(item.durations)
                state.arrPrex2.prepoint.push(item.timelongpercent)
            })
        })
    },
    getAllTitle({commit},params) {
        let p = getAllTitle(params)
        // p.then(res => {
        //     commit('GET_ALL_TITLE', res)
        // })
        return p;
    },
    cameraPoint({commit},params){
        let p = cameraPoint(params)
        p.then(res=>{
            console.log(res)
            commit('GET_DEVICE_LIST', res);
        })
    },
    available({commit},params){
        let p = available(params);
        p.then(res=>{
            commit("AVAILABLE",res);
        })
    },
    villageAnalyseList({commit},params){
        console.log('一调用')
        let p = villageAnalyseList(params);
        p.then(res=>{
            console.log('已返回', res)
            commit("VILLAGE_ANALYSE_LIST",res)
        })
    }
};

const mutations = {
    GET_CAMERA_SUMMARY(state,res){
        state.camerasummary.cameraTotal = res.filter(r=>r.category.id=='CAMERA_TOTAL')[0].total
        state.camerasummary.nvrTotal = res.filter(r=>r.category.id=='NVR_TOTAL')[0].total
        state.camerasummary.realCall = res.filter(r=>r.category.id=='REAL_CALL')[0].total
        state.camerasummary.normal = res.filter(r=>r.category.id=='NORMAL')[0].total
        state.camerasummary.abnormal = res.filter(r=>r.category.id=='ABNORMAL')[0].total
        state.camerasummary.gbReg = res.filter(r=>r.category.id=='GB_REG')[0].total
        state.camerasummary.url = res.filter(r=>r.category.id=='URL')[0].total
        state.camerasummary.onvif = res.filter(r=>r.category.id=='ONVIF')[0].total
        state.camerasummary.other = res.filter(r=>r.category.id=='OTHER')[0].total
        // console.log(state.camerasummary)
    },
    GET_LEADBOARDBY_SOURCE(state,res){
        res.result.current.details.forEach((item,index)=>{
            switch (index) {
                case 0:
                    item.imgUrl = require('@/assets/images/videoAggregation/medal1.png')
                    break;
                case 1:
                    item.imgUrl = require('@/assets/images/videoAggregation/medal2.png')
                    break;
                case 2:
                    item.imgUrl = require('@/assets/images/videoAggregation/medal3.png')
                    break;
            }
        })
        state.leadboardBySourceList = res.result.current.details
    },
    GET_LEADBOARDBY_SOURCE_TIME(state,res){
        res.result.current.details.forEach((item,index)=>{
            switch (index) {
                case 0:
                    item.imgUrl = require('@/assets/images/videoAggregation/1@2x.png')
                    break;
                case 1:
                    item.imgUrl = require('@/assets/images/videoAggregation/2@2x.png')
                    break;
                case 2:
                    item.imgUrl = require('@/assets/images/videoAggregation/3@2x.png')
                    break;
                case 3:
                    item.imgUrl = require('@/assets/images/videoAggregation/4@2x.png')
                    break;
                case 4:
                    item.imgUrl = require('@/assets/images/videoAggregation/5@2x.png')
                    break;
                case 5:
                    item.imgUrl = require('@/assets/images/videoAggregation/6@2x.png')
                    break;
            }
        })
        state.leadboardBySourcetimeList = res.result.current.details
    },
    GET_REALTIMEBY_DISTRICT(state,res){
        // console.log(res)
        let a = []
        a.push(res.result)

        let arr =  [...res.result.children,...a]
        console.log(arr)
        arr.forEach(item=>{
            if(item.current?.details?.length>0){

                state.totalfenxi=[...state.totalfenxi,...item.current.details]
                console.log(state.totalfenxi)
            }
        })
        console.log(state.totalfenxi)
        state.realTimeByDistrict =state.totalfenxi.filter(item=>item.category.id&&item.category.properties.name)
        state.realTimeByDistrictcun = state.totalfenxi.filter(item=>(!item.category.id)&&item.category.properties.name)
    },
    GET_SEARCHBY_CAMERA(state,res){
        state.byCameraList = res.result.current.details
    },
    GET_SEARCHBY_CAMERA_LIST(state,res){
        state.byCameraListAll = res.results[0].current.details
        state.byCameraListTotalPage = res.pagination.total
    },

    GET_USAGE_SUMMARY(state,res){
        let sortNumber = (a,b)=>{
            // console.log(a,b)
            return (b.category.properties.usage / b.category.properties.available).toFixed(4) - (a.category.properties.usage / a.category.properties.available).toFixed(4)
        }
        let c = res.result.current.details.sort(sortNumber)
        state.invokingAnalyseList = c.slice(0,6)
    },
    GET_USAGE_SUMMARY2(state,res){
        let sortNumber = (a,b)=>{
            return (b.category.properties.majority / b.category.properties.concurrent).toFixed(4) - (a.category.properties.majority / a.category.properties.concurrent).toFixed(4)
        }
        let c = res.result.current.details.sort(sortNumber)
        state.invokingAnalyse2List = c.slice(0,6)
    },
    GET_SEARCH_BYTAG(state,res){
        state.searchBytaglist = res
    },
    GET_ALL_TITLE(state, res){
        state.allTitle = res.result.title
        state.weather = res.result.weather
        state.metadatas = res.result.metadatas
    },
    AVAILABLE(state,res){
        state.available = res;
    },
    VILLAGE_ANALYSE_LIST(state, res){
        state.villageAnalyseList = res;
    },
    USABLE_POINT_PARAMS(state,res){
        console.log(res);
        state.usablePointParams = res;
    }
    // CLEAR_REALTIMEDISTRICT(){
    //     state.realTimeByDistrict  =[]
    //     state.realTimeByDistrictcun  =[]
    // }
};

export default {
state,
actions,
mutations
};

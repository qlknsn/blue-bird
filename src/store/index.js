import Vue from "vue";
import Vuex from "vuex";
import login from "@/store/modules/sanlin/login/login";
import tools from "@/store/modules/sanlin/tools/tools"; //引入工具文件
import mapdevice from "@/store/modules/sanlin/map/map";
import commonAPI from "@/store/modules/public/common";
import echarts from "@/store/modules/zhuqiao/echarts/echarts";
import sanlinecharts from "@/store/modules/sanlin/sanlinecharts/sanlinecharts";
import ukeyLogin from "@/store/modules/zhuqiao/login/login";
import live from "@/store/modules/lingang/liveCast/live";
import videoRankVisible from "@/store/modules/videoAggregation/visible.js";
import videomap from "@/store/modules/videoAggregation/map.js";
import videoAggregation from "@/store/modules/videoAggregation/videoAggregation.js";
import lingangVisible from "@/store/modules/lingang/visible.js";
// import sanlinVisible from "@/store/modules/sanlin/visible.js";
import lingangbasicInfo from "@/store/modules/lingang/basicInfo/basicInfo.js";
import datasLibrary from "./modules/lingang/datasLibrary";
import lingangflow from "../store/modules/lingang/flow/flow";
import lingangWarningAnalyse from "../store/modules/lingang/warningAnalyse/warningAnalyse";
import lingangCityState from "../store/modules/lingang/cityState/cityState";
import videoSave from "../store/modules/lingang/videoSave/videoSave";
import lingangMap from "../store/modules/lingang/map/map";
import lgLogin from "./modules/lingang/login/lgLogin";
import sanlinVisible from "@/store/modules/sanlin/visible.js";
import sanlinerqiV3 from "./modules/sanlin/sanlinerqiV3/sanlinerqiV3";
import videoCycle from "./modules/zhoupu/videoCycle";
import liqingchang from "./modules/liqingchang/liqingchang";
import anzhi from "./modules/anzhi/anzhi";
import oldStreet from "@/store/modules/oldStreet/oldStreet.js"
import oldStreetmap from "@/store/modules/oldStreet/oldstreetMap.js"

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    commonAPI,
    tools,
    login,
    mapdevice,
    echarts,
    ukeyLogin,
    sanlinecharts,
    live,
    videoRankVisible,
    videomap,
    videoAggregation,
    lingangVisible,
    lingangbasicInfo,
    datasLibrary,
    lingangflow,
    lingangWarningAnalyse,
    lingangCityState,
    videoSave,
    lingangMap,
    lgLogin,
    sanlinVisible,
    sanlinerqiV3,
    videoCycle,
    liqingchang,
    anzhi,
    oldStreet,
    oldStreetmap
    // zhuqiaomapdevice
  }
});

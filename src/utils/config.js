export const config = {
  identifier: {
    sanlin: {
      token: "urn:sh:district:310115130",
      leftScreenUrl: "",
      rightScreenUrl: ""
    },
    zhoujiadu: {
      token: "urn:sh:district:310115007",
      leftScreenUrl: "",
      rightScreenUrl: ""
    },
    lxz: {
      token: "urn:sh:district:310115000",
      leftScreenUrl: "",
      rightScreenUrl: ""
    },
    zhuqiao: {
      token: "urn:sh:district:310115139",
      leftScreenUrl: "",
      rightScreenUrl: ""
    },
    lingang: {
      token: "urn:sh:district:310115000",
      leftScreenUrl: "",
      rightScreenUrl: ""
    },
  }
};

import Vue from 'vue'
import store from '@/store/index'

let socket = null
// const WS_BASE_URL = 'ws://192.168.63.162:8087/rockfish'
// const WS_BASE_URL = 'ws://192.168.63.161:8087/rockfish'
const WS_BASE_URL = 'ws://192.168.123.232:8087/rockfish'

export function WSconnect(num) {
    // // 建立连接对象
    socket = new WebSocket(WS_BASE_URL)
    socket.onopen = () => {
        let json = {
            type: 'register',
            districtId: "right"
        }
        socket.send(JSON.stringify(json))
        // let jsons = {
        //     type: 'accept',
        //     districtId: "6",
        //     screen:'right',
        //     count:num
        // }
        // socket.send(JSON.stringify(jsons))
    }
    socket.onerror = () => {
        // reconnect()
    }
    socket.onmessage = (evt) => {
        let wsdata = JSON.parse(evt.data)
        // console.log(wsdata)
        if(wsdata.data){
            store.commit('SET_VIDEO_LEFT',wsdata)
        }
        Vue;
    };
    socket.onclose = () => {
        WSconnect()
    };
}
// import SockJS from 'sockjs-client'
// import Stomp from 'stompjs'
// const BASE_URL = 'ws://192.168.25.100:8787/ws/butterflyfish' //猎熊座测试环境

// const BASE_URL = 'ws://192.168.250.97:19006/starfish'
var BASE_URL = "";
import store from "@/store/index";
import router from "@/router/index";
// import Vue from "vue";
export function WSconnect() {
  let timeout = null;
  // 建立连接对象
  BASE_URL = "ws://10.9.0.61:8087/rockfish";
  let socket = new WebSocket(BASE_URL);
  socket.onopen = () => {
    let json = {
      type: "first",
      districtId: "123"
    };

    socket.send(JSON.stringify(json));
  };
  socket.onerror = () => {
    reconnect();
  };
  socket.onmessage = evt => {
    let d = JSON.parse(evt.data);
    store.commit("SET_ANZHI_VIDEO_LIST", d);
    // console.log(d);
  };
  socket.onclose = () => {
    reconnect();
  };

  function reconnect() {
    timeout && clearTimeout(timeout);
    timeout = setTimeout(() => {
      WSconnect();
    }, 4000);
  }
}

// var ws = new WebSocket('ws://localhost:8088/api/ws/warning');

// import SockJS from 'sockjs-client'
// import Stomp from 'stompjs'
import Vue from "vue";

// const BASE_URL = 'ws://47.100.65.32:17902/webSocket/standard' // 生产环境
import store from "../store/index";
export function WSconnect(BASE_URL) {
  let timeout = null;
  // 建立连接对象
  let socket = new WebSocket(BASE_URL);
  socket.onopen = () => {
    let json = {
      type: "REGISTER",
      properties: {
        channelGroup: "SeaweedChannelGroup",
        channel: "SeaweedChannel"
      }
    };
    socket.send(JSON.stringify(json));
  };
  socket.onerror = () => {
    reconnect();
  };
  socket.onmessage = evt => {
    if (
      JSON.parse(evt.data).className ===
      "cn.bearhunting.seaweed.webservice.entity.NoiseDeviceEntity"
    ) {
      // 噪音
      const detail = {};
      detail.result = JSON.parse(evt.data).info;
      store.commit("DEVICEPROBINGVISIBLE", false); // todo
      store.commit("GETVOICEDETAIL", detail);
    }
    if (
      JSON.parse(evt.data).className ===
      "cn.bearhunting.seaweed.common.datamodel.CarViolationRecord"
    ) {
      // 违章停车
      store.commit("CHANGEPOPvISIBLE", false); // todo
      store.commit("GETCARDETAIL", JSON.parse(evt.data).info);
    }
  };
  socket.onclose = () => {
    reconnect();
  };
  function reconnect() {
    timeout && clearTimeout(timeout);
    timeout = setTimeout(() => {
      let url = localStorage.websocketUrl
      WSconnect(url)
    }, 4000);
  }
}

// var ws = new WebSocket('ws://localhost:8088/api/ws/warning');

// import SockJS from 'sockjs-client'
// import Stomp from 'stompjs'
import Vue from "vue";

// const BASE_URL = 'ws://192.168.25.54:8788/ws/butterflyfish' // 生产环境
import store from "../store/index";
export function WSconnect(BASE_URL) {
  let timeout = null;
  // 建立连接对象
  let socket = new WebSocket(BASE_URL);
  socket.onopen = () => {
    let json = {
      type: "REGISTER",
      properties: {
        channelGroup: "SeaweedChannelGroup",
        channel: "SeaweedChannel"
      }
    };
    socket.send(JSON.stringify(json));
  };
  socket.onerror = () => {
    reconnect();
  };
  socket.onmessage = evt => {
    if (
      JSON.parse(evt.data).className ===
      "cn.bearhunting.seaweed.webservice.entity.NoiseDeviceEntity"
    ) {
      // 噪音
      const detail = {};
      detail.result = JSON.parse(evt.data).info;
      store.commit("DEVICEPROBINGVISIBLE", false); // todo
      store.commit("GETVOICEDETAIL", detail);
    }
    if (
      JSON.parse(evt.data).className ===
      "cn.bearhunting.seaweed.common.datamodel.CarViolationRecord"
    ) {
      // 违章停车
      store.commit("CHANGEPOPvISIBLE", false); // todo
      store.commit("GETCARDETAIL", JSON.parse(evt.data).info);
    }
  };
  socket.onclose = () => {
    reconnect();
  };
  function reconnect() {
    timeout && clearTimeout(timeout);
    timeout = setTimeout(() => {
      let url = localStorage.websocketUrl
      WSconnect(url)
    }, 4000);
  }
}

/*
    socket.onmessage = (evt) => {
        console.warn("WebSocket: " + JSON.parse(evt.data).msg)
        if (JSON.parse(evt.data).type == 'MANAGER_TASK') {
            // 管理任务
            store.commit('GET_MANAGETASK_LIST', JSON.parse(evt.data).data)
        }
        if (JSON.parse(evt.data).type == 'SIGN') {
            // 治理体征
            store.commit('GET_SIGNS_LIST', JSON.parse(evt.data).data)
        }
        if (JSON.parse(evt.data).type == 'DYNAMIC_PROCESS') {
            // 动态流程

            store.commit('GET_FLOW_LIST', JSON.parse(evt.data).data)

        }
    };*/

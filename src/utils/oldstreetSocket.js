import Vue from 'vue'
import store from '@/store/index'

let socket = null
// const WS_BASE_URL = 'ws://192.168.63.162:8087/rockfish'
// const WS_BASE_URL = 'ws://192.168.63.161:8087/rockfish'
let a = Math.random() * 100000000
const WS_BASE_URL = `ws://47.100.65.32:33591/webSocket/task/s${a}`

export function WSconnect(num) {
    // // 建立连接对象
    socket = new WebSocket(WS_BASE_URL)
    socket.onopen = () => {
        console.log(`open socket`)
    }
    socket.onerror = () => {
        // reconnect()
    }
    socket.onmessage = (evt) => {
        console.log(evt.data)
        console.log(evt.data.indexOf('date')!==-1)
        if (evt.data.indexOf('date')!==-1) {
            let wsdata = JSON.parse(evt.data)
            console.log(wsdata)
            if (wsdata.date) {
                store.commit('SET_SCROLL_INFO', wsdata.date)
            }
        }
    };
    socket.onclose = () => {
        WSconnect()
    };
}
import Vue from "vue";
import axios from "axios";
import router from "@/router/index";
import { config as configuration } from "@/utils/config.js";
import { VueAxios } from "./axios";
// 创建 axios 实例
const service = axios.create({
  baseURL: process.env.VUE_APP_API_BASE_URL, // api base_url
  timeout: 60000, // 请求超时时间
  onUploadProgress: function(progressEvent) {
    progressEvent;
    // Do whatever you want with the native progress event
    // alert('total: ' + progressEvent.total + ',loaded: ' + progressEvent.loaded)
  }
});

const err = error => {
  if (error.response) {
    const data = error.response.data;
    // const token = Vue.ls.get('token')
    // const token = '7486bab3-3b3b-49fd-bca8-3c64b3aeb965'
    // todo: 需要对503状态吗进行处理
    if (
      error.response.status === 401 &&
      !(data.result && data.result.isLogin)
    ) {
      // notification.error({
      //     message: 'Unauthorized',
      //     description: 'Authorization verification failed'
      // })
    }
    if (error.response.status == 404) {
      console.log('...');
    }
    if (error.response.status === 400) {
      alert("接口请求出错");
    }
    if (error.response.status === 403) {
      // notification.error({
      //     message: 'Forbidden',
      //     description: data.message
      // })
      alert("forbidden");
    }
    if (error.response.status === 503) {
      Vue.prototype.$message({
        type: "error",
        message: "网关错误",
        duration: 10000,
        offset: "500px"
      });
    }
    if (error.response.status === 500) {
      // this.$message({
      //     message: '警告哦，这是一条警告消息',
      //     type: 'warning'
      // });
      Vue.prototype.$message({
        type: "error",
        message: "服务器错误",
        duration: 10000,
        offset: "500px"
      });
    }
  }
  return Promise.reject(error);
};

// request interceptor
service.interceptors.request.use(config => {
  let route = router.currentRoute;
  let path = route.fullPath;
  if (path.indexOf("sanlin") > -1) {
    config.headers["identifier"] = configuration.identifier.sanlin.token;
    config.headers["tokenv2"] = Vue.ls.get('sanlinlogintoken');
  } else if (path.indexOf("zhoujiadu") > -1) {
    config.headers["identifier"] = configuration.identifier.zhoujiadu.token;
  } else if (path.indexOf("liexiongzuo") > -1) {
    config.headers["identifier"] = configuration.identifier.lxz.token;
  } else if (path.indexOf("videoAggregation") > -1) {
    config.headers["token"] = route.query.token;
    config.headers["identifier"] = route.query.token;
  }else if (path.indexOf("lingang") > -1) {
    config.headers["token"] = route.query.token;
    config.headers["identifier"] = configuration.identifier.lingang.token;
    // config.headers["Content-Type"] = "application/x-www-form-urlencoded";

  }
  else if (path.indexOf("zhuqiao") > -1) {
    config.headers["identifier"] = configuration.identifier.zhuqiao.token;
  }
  config.headers["screen"] = sessionStorage.getItem("screen");
  // config.headers["screen"] = Vue.ls.get("screen");
  config.headers["Access-Control-Allow-Origin"] = "*";
  // config.headers["Content-Type"] = "application/json;charset=UTF-8";
  config.headers["Authorization"] = "Basic YWRtaW46YWRtaW4=";

  return config;
}, err);

// response interceptor
service.interceptors.response.use(response => {
  return response.data;
}, err);

const installer = {
  vm: {},
  install(Vue) {
    Vue.use(VueAxios, service);
  }
};

export { installer as VueAxios, service as axios };

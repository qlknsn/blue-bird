const deviceIcons = {
  GENERAL: require("../assets/images/screen/office.png"),
  VEHICLE: require("../assets/images/screen/road.png"),
  VIOLATION: require("../assets/images/screen/car.png"),
  FACIAL: require("../assets/images/screen/face.png"),
  DAMAGED: require("../assets/images/screen/errIcon.png"),
  CAMERAGROUP: require("../assets/images/lingang/shexiangji.png"),
  SET_QIYEINFOPOP_VISIBLE: require("../assets/images/lingang/qiye.png"),
  SET_SANHUI_VISIBLE: require("../assets/images/lingang/sanhui.png"),
  SET_HOTLINE_VISIBLE: require("../assets/images/lingang/gongdan.png"),
  // SET_UN_HOTLINE_VISIBLE: require("../assets/images/lingang/gongdan@2x.png"),
  UN_SET_HOTLINE_VISIBLE: require("../assets/images/lingang/gongdan@2x.png"),
  SET_SENSOR: require("../assets/images/lingang/传感器-1.png"),

  // 三林老街
  WENBAO_POINT:require("../assets/images/oldstreet/wenbaodian.png"),
  WENHUA_POINT:require("../assets/images/oldstreet/wenhuachanggaun.png"),
  HUODONG_POINT:require("../assets/images/oldstreet/wenhuahuodongdian.png"),
  TIYU_POINT:require("../assets/images/oldstreet/jianshendian.png"),
  JUWEI_POINT:require("../assets/images/oldstreet/juwei.png"),
  ZHAN_POINT:require("../assets/images/oldstreet/xiaofangzhan.png"),
  SHUAN_POINT:require("../assets/images/oldstreet/xiaofangshuan.png"),
  CAMERA_POINT:require("../assets/images/oldstreet/shexiangji.png"),
  TASK_POINT:require("../assets/images/oldstreet/xietong_red.png"),
};

export function selectdeviceIcon(statu) {
  return deviceIcons[statu];
}
export function getIcons() {
  return deviceIcons;
}

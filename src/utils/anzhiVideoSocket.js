import Vue from 'vue'
import store from '@/store/index'

console.log(location.hostname)
let socket = null
let wstype = ''
// const WS_BASE_URL = 'ws://192.168.63.162:8087/rockfish'
// const WS_BASE_URL = 'ws://192.168.123.232:7087/rockfish'
// const WS_BASE_URL = `ws://10.242.155.3:7086/live?type=cams&camId=`
const WS_BASE_URL = `ws://192.168.123.232:7086/live?type=anzhi&times=5&camId=`
// const WS_BASE_URL = `ws://10.242.155.3:7086/live?type=anzhi&times=60&camId=`

export function WSconnect(type) {
    
    // // 建立连接对象
    socket = new WebSocket(`${WS_BASE_URL}${type}`)
    wstype = type
    socket.onopen = () => {
        
    }
    socket.onerror = () => {
        // reconnect()
    }
    socket.onmessage = (evt) => {
        let wsdata = JSON.parse(evt.data)
        if(wsdata.data){
            store.commit('SET_VIDEO_LEFT',wsdata.data)
        }
        Vue;
    };
    socket.onclose = () => {
        WSconnect(wstype)
    };
}
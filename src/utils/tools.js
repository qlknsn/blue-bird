export const TODAY_TASK = 1 // 今日工作
export const TEAM_WORK = 1 << 1 // 协同队伍
export const SELF_WORK = 1 << 2 // 自治队伍
export const VIDEO_MASK = 1 << 3 // 视频监控

export const GUAN_AI = 1 << 4 //关爱对象
export const DANG_YUAN = 1 << 5 //党员
export const CHU_ZU = 1 << 6 //出租
export const QI_YE = 1 << 7 //企业
export const MIN_SU = 1 << 8 //民宿
export const SHANG_PU = 1 << 9 //商铺
export const CUNMIN_ZU = 1 << 10 //村民组各组
// export const CUNMIN_ZU = 1 << 11 //村民组单组

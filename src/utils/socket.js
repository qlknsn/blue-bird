// import SockJS from 'sockjs-client'
// import Stomp from 'stompjs'
// const BASE_URL = 'ws://192.168.25.100:8787/ws/butterflyfish' //猎熊座测试环境

// const BASE_URL = 'ws://192.168.250.97:19006/starfish'
var BASE_URL = "";
import store from "@/store/index";
import router from "@/router/index";
// import Vue from "vue";
export function WSconnect() {
  let timeout = null;
  // 建立连接对象
  let currentPath = router.currentRoute.fullPath;
  if (currentPath.indexOf("zhoujiadu") > -1) {
    BASE_URL = "ws://10.3.200.10:8787/ws/butterflyfish"; //周家渡测试环境
  }
  if (currentPath.indexOf("liexiongzuo") > -1) {
    BASE_URL = "ws://192.168.25.54:8787/ws/butterflyfish"; //猎熊座测试环境
  }
  if (currentPath.indexOf("sanlin") > -1) {
    BASE_URL = "ws://10.5.200.254:8787/ws/butterflyfish"; //三林正式环境
  }
  if (currentPath.indexOf("zhuqiao") > -1) {
    BASE_URL = "ws://10.2.0.14:8787/ws/butterflyfish";
  }
  if (currentPath.indexOf("lqc") > -1) {
    BASE_URL =
      "ws://47.100.65.32:17902/webSocket/standard/" +
      router.history.current.query.sid;
  }
  let socket = new WebSocket(BASE_URL);
  // 获取stomp子协议的客户端对象
  // let stompClient = Stomp.over(socket)
  // // 向服务器发起websocket连接并发送connect帧
  // stompClient.connect(
  //     {},
  //     function connectCallback(res){
  //         // stompClient.subscribe(topic_url,res=>{
  //         //     console.log(res)
  //         // })
  //         res;
  //         let json = {
  //             type:'register',
  //             districtId:'190220'
  //         }
  //         socket.send(json)
  //     },
  //     function errorCallback() {
  //         // 连接失败时再次进行调用函数
  //         clearTimeout(timeout)
  //         timeout = setTimeout(()=>{
  //           connect()
  //         },4000)
  //     }
  // )
  socket.onopen = () => {
    let currentPath = router.currentRoute.fullPath;
    var json = "";
    if (currentPath.indexOf("zhoujiadu") > -1) {
      json = {
        type: "REGISTER",
        properties: {
          // 猎熊座测试环境
          // channelGroup: 'urn:sh:district:310115000',
          // channel: 'urn:bh:screen:310115000000A'
          // 周家渡测试环境
          channelGroup: "urn:sh:district:310115007",
          channel: sessionStorage.getItem("screen")
        }
      };
    }
    if (currentPath.indexOf("liexiongzuo") > -1) {
      json = {
        type: "REGISTER",
        properties: {
          // 猎熊座测试环境
          channelGroup: "urn:sh:district:310115000",
          channel: sessionStorage.getItem("screen")
        }
      };
    }
    if (currentPath.indexOf("sanlin") > -1) {
      json = {
        type: "REGISTER",
        properties: {
          // 三林正式环境
          channelGroup: "urn:sh:district:310115130",
          channel: sessionStorage.getItem("screen")
        }
      };
    }
    if (currentPath.indexOf("zhuqiao") > -1) {
      json = {
        type: "REGISTER",
        properties: {
          // 三林正式环境
          channelGroup: "urn:sh:district:310115139",
          channel: sessionStorage.getItem("screen")
        }
      };
    }

    socket.send(JSON.stringify(json));
  };
  socket.onerror = () => {
    reconnect();
  };
  socket.onmessage = evt => {
    let currentPath = router.currentRoute.fullPath;
    if (currentPath.indexOf("lqc") > -1) {
      let evtData = JSON.parse(evt.data);
      let { date, messageType } = evtData;
      switch (messageType) {
        case "ALARMASPHALT":
          store.commit("GET_LIQINGCHANG_LIST", date);
          break;
        case "ALARMSTONE":
          store.commit("GET_SHILIAO_LIST", date);
          break;
        case "PRODUCED_SUM_ALL":
          store.commit("GET_PRODUTION_INFO", date);
          break;
        case "PROJECT":
          // store.commit("GET_PRODUTION_PROGRESS", date)
          break;
        case "PLAN":
          store.commit("GET_TRUE_PRODUCT", date);
          break;
        case "NATURAL_GAS":
          store.commit("GET_NATURAL_GAS", date);
          break;
        case "ELECTRICITY":
          store.commit("GET_ELECTRICITY", date);
          break;
        default:
          break;
      }
    } else {
      let evtData = JSON.parse(evt.data);
      if (evtData?.playable) {
        let obj = {
          metadata: JSON.parse(evt.data).playable.metadata,
          screenSpot: JSON.parse(evt.data).playable.screenSpot,
          streamNode: JSON.parse(evt.data).playable.streamNode
        };
        if (evtData.playable.screenSpot) {
          if (evtData.action == "STOP") {
            obj.metadata.url = "";
          } else {
            let str = `%c 1.【websocket流到达】: segment:${obj.screenSpot.segment},<${obj.screenSpot.coordinate.x},${obj.screenSpot.coordinate.y}> ${obj.metadata.url}`;
            console.log(str, "color:gold");
            store.commit("SET_CURRENT_VIDEO_LIST", obj);
          }
        }
      } else {
        console.log(evtData);
      }
    }
  };
  socket.onclose = () => {
    reconnect();
  };

  function reconnect() {
    timeout && clearTimeout(timeout);
    timeout = setTimeout(() => {
      WSconnect();
    }, 4000);
  }
}

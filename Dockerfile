FROM nginx
ADD ./blue-bird /export/files/blue-bird
ADD conf.d/blue-bird.conf /etc/nginx/conf.d/
LABEL version="0.0.1"
LABEL maintainer="Bearhunting Dev Group <fenghelong@bearhunting.cn>"
EXPOSE 58101
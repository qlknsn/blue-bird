[![pipeline status](http://gitlab.bearhunting.cn/frontend/fockfish/badges/master/pipeline.svg)](http://gitlab.bearhunting.cn/frontend/fockfish/commits/master)
[![coverage report](http://gitlab.bearhunting.cn/frontend/fockfish/badges/master/coverage.svg)](http://gitlab.bearhunting.cn/frontend/fockfish/commits/master)

## 石斑鱼项目重构
静态资源文件全部上传到阿里云`rockfish-bigscree`目录下面。
不同模块的图片上传到不同的子目录，例如：
public文件夹 代表公共部分的图片，例如首次加载大屏地图上撒点的图标。
renqunmidu文件夹 代表“人群密度“模块。

### 新增一个街镇需要注意的地方
- `requests.js`文件，需要新增一个header
- `views`文件需要新增对应的文件夹
- `login.js`在mutations中登陆成功后要新增根据是哪个街镇的然后跳转到不同的dashboard

### SRS命令
` docker run -p 1935:1935 -p 8080:8080 -p 1985:1985 -p 8000:8000/udp \
    --env CANDIDATE=192.168.63.78 \
    registry.cn-hangzhou.aliyuncs.com/ossrs/srs:v4.0.23 objs/srs -c conf/rtc.conf`

### Roadmap
请查看[此链接](http://gitlab.bearhunting.cn/frontend/fockfish/blob/master/CHANGELOG.md)

### Docker部署项目
- 在本地运行`npm run build`
- 修改`package.json`文件中`builddocker`,`pushdocker`命令中的镜像标签
- 运行`npm run builddocker`,`npm run pushdocker`


### 沥青厂大屏部署
远程向日葵
![alt 远程向日葵](https://bh-frontend.oss-cn-shanghai.aliyuncs.com/liqingchang/3f73v6wcndius7pmzxubajjh5_0.jpg)


### 文件中包含的大屏项目
- 临港大屏
    `主界面vue文件：/views/lingang`
    `组件vue文件：/components/lingang`
    `部分图片资源：/assets/images/lingang  备注：现在所有的图片资源已经迁移到阿里云`(https://oss.console.aliyun.com/bucket/oss-cn-shanghai/bh-frontend/object?path=lingGang%2F)
    `less文件:/assets/less/lingang`

- 沥青厂大屏
    `主界面vue文件：/views/liqingchang`
    `部分图片资源：所有的图片资源阿里云地址`(https://oss.console.aliyun.com/bucket/oss-cn-shanghai/bh-frontend/object?path=liqingchang%2F)

- 石斑鱼大屏
    `石斑鱼大屏包括：三林石斑鱼、祝桥石斑鱼、周家渡石斑鱼`
    `主界面vue文件分别是：/views/sanlin、 /views/zhuqiao、 /views/zhoujiadu`
    `组件vue文件：/components/sanlin、 /components/zhuqiao、 /components/zhoujiadu`
    `图片资源：/assets/images/sanlin`
    `less文件:/assets/less/sanlin`

- 视频中间件大屏
    `主界面vue文件：/views/videoAggregation`
    `组件vue文件：/components/videoAggregation`
    `部分图片资源：/assets/images/videoAggregation  备注：所有的图片资源阿里云地址`(https://oss.console.aliyun.com/bucket/oss-cn-shanghai/bh-frontend/object?path=videoaggregation%2F)
    `less文件:/assets/less/videoAggregation`

- 三林老街大屏
    `主界面vue文件：/views/sanlinOldStreet`
    `组件vue文件：/components/sanlinOldStreet`
    `部分图片资源：所有的图片资源阿里云地址`(https://oss.console.aliyun.com/bucket/oss-cn-shanghai/bh-frontend/object?path=oldstreet%2F)
    `less文件:/assets/less/sanlin-old`

#### 数据获取、更新、加载

以上所有项目所有数据采用观察者模式:所有的数据获取和更新存储在vuex文件`/store`
接口请求地址文件位置，例如：三林石斑鱼   /store/api/sanlin/\*Url.js
接口请求方法文件位置，例如：三林石斑鱼   /store/api/sanlin/\*.js
接口数据处理文件位置，例如：三林石斑鱼   /store/modules/sanlin/\*.js

#### 路由文件
所有路由文件按模块加载：/router/*.js

### 部署项目
<!-- - 在本地运行`npm run build`
- 修改`package.json`文件中`builddocker`,`pushdocker`命令中的镜像标签
    例：
    `docker build -t registry.cn-shanghai.aliyuncs.com/bearhunting-fe/blue-bird:`<u>qqlss</u>`.`
    `下划线部分是要修改的部分，备注： 最后这个点'.'一定要加，表示的是当前文件夹`
- 运行`npm run builddocker`,`npm run pushdocker`

- 在服务器运行 `docker rmi 老的镜像`
- 测试服务器运行 `docker run -p 58103:` -->
 - 在本地运行`npm run build`、`tar zcvf blue-bird.tar.gz blue-bird/`
 - 测试服务器rz tar包到 `/home/frontend/apps/html`  执行`tar zxvf blue-bird.tar`(目前已经自动化部署,merge到代码仓库会自动部署到服务器)
 - 配置nginx文件详见：/conf.d/blue-bird.conf
 

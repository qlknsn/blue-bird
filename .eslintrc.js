module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: ["plugin:vue/essential", "eslint:recommended"],
  parserOptions: {
    parser: "babel-eslint"
  },
  rules: {
    "no-console": process.env.NODE_ENV === "production" ? "off" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off",
    "no-prototype-builtins": "off",
    "no-undef": "off",
    "vue/no-dupe-keys": "off",
    "no-irregular-whitespace": "off",
    "no-extra-semi": "off",
    "no-unused-vars": "off",
    "vue/no-unused-components": "off"
  }
};

const webpack = require("webpack");
module.exports = {
  runtimeCompiler: true,
  outputDir: `blue-bird`,
  configureWebpack: {
    externals: {
      AMap: "AMap",
      AMapUI: "AMapUI",
      cyberplayer: "baiducyberplayer"
    },
    module: {
      rules: [
        {
          test: /\.(swf|ttf|eot|svg|woff(2))(\?[a-z0-9]+)?$/,
          loader: "file-loader"
        }
      ]
    },
    plugins: [
      new webpack.ProvidePlugin({
        $: "jquery",

        jQuery: "jquery",

        "windows.jQuery": "jquery"
      })
    ]
  },
  // configureWebpack: (config) => {
  //     config.module.rules.push({
  //         test: /\.(swf|ttf|eot|svg|woff(2))(\?[a-z0-9]+)?$/,
  //         loader: 'file-loader',
  //     })
  //     config.module.rules.push({
  //         externals: {
  //             AMap: "AMap"
  //         }
  //     })
  // },
  devServer: {
    proxy: {
      // // 临港直播代理
      "/idms": {
        target: "http://10.242.212.152:8081/public/api/idms",
        changeOrigin: true,
        pathRewrite: { "^/idms": "" }
      },
      "/platform": {
        target: "http://10.242.32.160:8081/public/api/idms/v1/platform",
        changeOrigin: true,
        pathRewrite: { "^/platform": "" }
      },
      // 中间件
      "/viis": {
        // target: "http://10.242.212.152:8081/public/api/viis",
        target: "http://10.5.200.252:9001/public/api/viis",
        // target: "http://10.3.200.31:9001/public/api/viis",
        // target: "http://192.168.25.39:9001/public/api/viis",
        // target: "http://10.242.182.96:9001/public/api/viis",
        changeOrigin: true,
        pathRewrite: { "^/viis": "" }
      },
      // 石斑鱼代理
      "/vis": {
        // target: "http://192.168.25.54:8081/public/api/vis", //   猎熊座视频轮巡后台地址、
        // target: "http://10.2.0.14:8081", //   祝桥视频轮巡后台地址、
        // target: "http://10.3.200.10:8081", //   周家渡视频轮巡后台地址、
        target: "http://10.9.0.61:8081/public/api/vis", //   三林视频轮巡后台地址、
        // target: "http://192.168.25.31:8081/public/api/vis", //   三林视频轮巡后台地址、
        // target:'http://192.168.25.110:28076',
        changeOrigin: true,
        pathRewrite: { "^/vis": "" }
      },
      "/vcs": {
        // target: "http://192.168.25.54:8081/public/api/vis", //   猎熊座视频轮巡后台地址、
        // target: "http://10.2.0.14:8081", //   祝桥视频轮巡后台地址、
        // target: "http://10.3.200.10:8081", //   周家渡视频轮巡后台地址、
        target: "http://10.9.0.61:8086/public/api/vis", //   三林视频轮巡后台地址、
        // target: "http://192.168.25.31:8081/public/api/vis", //   三林视频轮巡后台地址、
        // target:'http://192.168.25.110:28076',
        changeOrigin: true,
        pathRewrite: { "^/vcs": "" }
      },
      "/starfish-admin": {
        target: "http://139.224.69.150:28600",
        // target:'http://192.168.25.110:28076',
        changeOrigin: true,
        pathRewrite: {
          "^/starfish-admin": ""
        }
      },
      "/url": {
        // target:'http://192.168.63.9:19001',
        target: "https://starfish-backend-test.bearhunting.cn/url",
        // target:'http://192.168.63.188:39001',
        changeOrigin: true,
        pathRewrite: {
          "^/url": "/"
        }
      },
      "/icon": {
        // target: 'https://bearhunting-api.utools.club',
        target: "https://starfish-backend-test.bearhunting.cn/assert/icon",
        // target:'https://starfish-backend.bearhunting.cn/assert/icon',
        // target:'http://192.168.63.100:19001',
        changeOrigin: true,
        pathRewrite: {
          "^/icon": ""
        }
      },
      "/upload": {
        // target: 'https://bearhunting-api.utools.club',
        target: "https://starfish-backend-test.bearhunting.cn/assert/upload",
        // target:'https://starfish-backend.bearhunting.cn/assert/upload',
        // target:'http://192.168.63.100:19001',
        changeOrigin: true,
        pathRewrite: {
          "^/upload": ""
        }
      },
      "/lxz": {
        target: "https://starfish-backend-test.bearhunting.cn",
        changeOrigin: true,
        pathRewrite: {
          "^/lxz": ""
        }
      },
      "/zq": {
        // target: 'http://10.2.0.11:8089',
        target: "http://192.168.25.54:18888",
        changeOrigin: true,
        pathRewrite: {
          "^/zq": ""
        }
      },

      "/picture": {
        target: "http://10.2.0.11:8089",
        // target: "http://starfish-backend-test.bearhunting.cn:9001",
        changeOrigin: true,
        pathRewrite: {
          "^/picture": "/picture"
        }
      },
      // "/seaweed": {
      //   target: "http://10.9.0.61:9001/public/api/seaweed",
      //   // 微卡口模块统计信息接口
      //   // target: "http://192.168.63.14:9001/public/api/seaweed", // http://192.168.63.14:9001/
      //   changeOrigin: true,
      //   pathRewrite: {
      //     "^/seaweed": ""
      //   }
      // },
      "micro-bayonet-statistics": {
        target:
          "http://192.168.111.12:28097/public/api/micro-bayonet-statistics", // http://192.168.63.14:9001/
        changeOrigin: true,
        pathRewrite: {
          "^/micro-bayonet-statistics": ""
        }
      },
      //  临港大屏

      "/lingangresultful": {
        // target: "http://192.168.25.54:8087/public/api/lingangresultful/",
        // target: "http://192.168.63.116:39001/",
        // target:'http://192.168.25.54:39001',
        target: "http://jenkins.bearhunting.cn:37001",
        // target:'http://192.168.25.54:37001',
        changeOrigin: true,
        pathRewrite: {
          "^/lingangresultful": "/url"
        }
      },
      "/getCamera": {
        // target: "http://192.168.25.54:8087/public/api/lingangresultful/",
        // target: "http://192.168.63.116:39001/",
        // target:'http://192.168.63.188:39001',
        target: "http://10.242.212.152:8084/public/api",
        changeOrigin: true,
        pathRewrite: {
          "^/getCamera": "/vis"
        }
      },
      "/sanlinerqi": {
        // target: "http://192.168.63.162:9001/public/api",
        target: "http://10.9.0.61:9001/public/api",
        // target: "http://192.168.63.195:9001/public/api",
        changeOrigin: true,
        pathRewrite: {
          "^/sanlinerqi": "/seaweed"
        }
      },
      "/videoLoopTask": {
        // target: "http://192.168.63.162:9001/public/api",
        // target: "http://192.168.63.162:8088/api/videoLoopTask",
        target: "http://192.168.123.232:8086/api/videoLoopTask",
        // target: "http://192.168.63.161:8088/api/videoLoopTask",
        changeOrigin: true,
        pathRewrite: {
          "^/videoLoopTask": ""
        }
      },   
      "/markVideo": {
        // target: "http://192.168.63.162:9001/public/api",
        // target: "http://192.168.63.162:8088/api/videoLoopTask",
        target: "http://192.168.123.232:7086/public/api/vis/v1",
        // target: "http://192.168.63.161:8088/api/videoLoopTask",
        changeOrigin: true,
        pathRewrite: {
          "^/markVideo": ""
        }
      }, 
      "/asp": {
        target: "http://47.100.65.32:17902/public/api/asp",
        // target: "http://58.34.211.146:17904/public/api/asp", // zs
        // target: "http://192.168.63.116:17902/public/api/asp",
        changeOrigin: true,
        pathRewrite: {
          "^/asp": ""
        }
      },
      // "/sanlinlaojie": {
      //   // target: "http://47.100.65.32:17902/public/api/asp",
      //   target: "http://47.100.65.32:33591",
      //   // target: "http://192.168.63.116:17902/public/api/asp",
      //   changeOrigin: true,
      //   pathRewrite: {
      //     "^/sanlinlaojie": ""
      //   }
      // },
      "/getstreetvideo": {
        // target: "http://47.100.65.32:33591/public/api",
        target: "https://sanlinsmart.bearhunting.cn/public/api",
        // target: "http://192.168.63.116:17902/public/api/asp",
        changeOrigin: true,
        pathRewrite: {
          "^/getstreetvideo": ""
        }
      }
    }
    // before: app => {}
  }
};
